-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 13, 2021 at 07:44 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `churchcrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `calendars`
--

CREATE TABLE `calendars` (
  `calendar_id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `accesstoken` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foregroundColor` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `backgroundColor` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `calendars`
--

INSERT INTO `calendars` (`calendar_id`, `name`, `accesstoken`, `foregroundColor`, `backgroundColor`) VALUES
(1, 'Public Calendar', NULL, 'FFFFFF', '00AA00'),
(2, 'Private Calendar', NULL, 'FFFFFF', '0000AA');

-- --------------------------------------------------------

--
-- Table structure for table `calendar_events`
--

CREATE TABLE `calendar_events` (
  `calendar_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `canvassdata_can`
--

CREATE TABLE `canvassdata_can` (
  `can_ID` mediumint(9) UNSIGNED NOT NULL,
  `can_famID` mediumint(9) NOT NULL DEFAULT 0,
  `can_Canvasser` mediumint(9) NOT NULL DEFAULT 0,
  `can_FYID` mediumint(9) DEFAULT NULL,
  `can_date` date DEFAULT NULL,
  `can_Positive` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `can_Critical` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `can_Insightful` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `can_Financial` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `can_Suggestion` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `can_NotInterested` tinyint(1) NOT NULL DEFAULT 0,
  `can_WhyNotInterested` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `church_location_person`
--

CREATE TABLE `church_location_person` (
  `location_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `person_location_role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `church_location_role`
--

CREATE TABLE `church_location_role` (
  `location_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `role_order` int(11) NOT NULL,
  `role_title` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `config_cfg`
--

CREATE TABLE `config_cfg` (
  `cfg_id` int(11) NOT NULL DEFAULT 0,
  `cfg_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `config_cfg`
--

INSERT INTO `config_cfg` (`cfg_id`, `cfg_name`, `cfg_value`) VALUES
(21, 'sDefaultCity', 'Muntinlupa'),
(23, 'sDefaultCountry', 'Philippines'),
(28, 'bSMTPAuth', ''),
(30, 'sSMTPPass', 'admin@123'),
(47, 'bHidePersonAddress', ''),
(48, 'bHideFriendDate', ''),
(49, 'bHideFamilyNewsletter', ''),
(50, 'bHideWeddingDate', ''),
(51, 'bHideLatLon', ''),
(52, 'bUseDonationEnvelopes', ''),
(58, 'bUseScannedChecks', ''),
(65, 'sTimeZone', 'Asia/Brunei'),
(67, 'bForceUppercaseZip', ''),
(72, 'bEnableNonDeductible', ''),
(80, 'bEnableSelfRegistration', '1'),
(999, 'bRegistered', ''),
(1003, 'sChurchName', 'Lighthouse Apostolic Ministry of Pentecost'),
(1004, 'sChurchAddress', 'Midland Alley, Parkhomes Subdivision, Tunasan'),
(1005, 'sChurchCity', 'Muntinlupa'),
(1006, 'sChurchState', 'NCR'),
(1027, 'sPledgeSummary2', 'as of'),
(1028, 'sDirectoryDisclaimer1', 'Every effort was made to ensure the accuracy of this directory.  If there are any errors or omissions, please contact the church office.This directory is for the use of the people of'),
(1035, 'bEnableGravatarPhotos', ''),
(1036, 'bEnableExternalBackupTarget', ''),
(1037, 'sExternalBackupType', 'WebDAV'),
(1046, 'sLastIntegrityCheckTimeStamp', '20211201-211658'),
(1047, 'sChurchCountry', 'Philippines'),
(2010, 'bAllowEmptyLastName', ''),
(2011, 'sKioskVisibilityTimestamp', '2021-12-01 22:16:03'),
(2015, 'sChurchTwitter', 'admin'),
(2017, 'bEnableExternalCalendarAPI', ''),
(2045, 'bPHPMailerAutoTLS', ''),
(2046, 'sPHPMailerSMTPSecure', ''),
(2050, 'bEnabledMenuLinks', ''),
(2052, 'bEnabledFinance', ''),
(2055, 'bEnabledFundraiser', ''),
(2056, 'bEnabledEmail', ''),
(2060, 'IncludeDataInNewPersonNotifications', ''),
(2061, 'bSearchIncludeFamilyCustomProperties', ''),
(2062, 'bBackupExtraneousImages', ''),
(2064, 'sLastSoftwareUpdateCheckTimeStamp', '20211201-211659'),
(2065, 'bAllowPrereleaseUpgrade', ''),
(2069, 'bRequire2FA', ''),
(2071, 'bSendUserDeletedEmail', ''),
(20142, 'bHSTSEnable', '');

-- --------------------------------------------------------

--
-- Table structure for table `deposit_dep`
--

CREATE TABLE `deposit_dep` (
  `dep_ID` mediumint(9) UNSIGNED NOT NULL,
  `dep_Date` date DEFAULT NULL,
  `dep_Comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `dep_EnteredBy` mediumint(9) UNSIGNED DEFAULT NULL,
  `dep_Closed` tinyint(1) NOT NULL DEFAULT 0,
  `dep_Type` enum('Bank','CreditCard','BankDraft','eGive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Bank'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0;

-- --------------------------------------------------------

--
-- Table structure for table `donateditem_di`
--

CREATE TABLE `donateditem_di` (
  `di_ID` mediumint(9) UNSIGNED NOT NULL,
  `di_item` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `di_FR_ID` mediumint(9) UNSIGNED NOT NULL,
  `di_donor_ID` mediumint(9) NOT NULL DEFAULT 0,
  `di_buyer_ID` mediumint(9) NOT NULL DEFAULT 0,
  `di_multibuy` smallint(1) NOT NULL DEFAULT 0,
  `di_title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `di_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `di_sellprice` decimal(8,2) DEFAULT NULL,
  `di_estprice` decimal(8,2) DEFAULT NULL,
  `di_minimum` decimal(8,2) DEFAULT NULL,
  `di_materialvalue` decimal(8,2) DEFAULT NULL,
  `di_EnteredBy` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `di_EnteredDate` date NOT NULL,
  `di_picture` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `donationfund_fun`
--

CREATE TABLE `donationfund_fun` (
  `fun_ID` tinyint(3) NOT NULL,
  `fun_Active` enum('true','false') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `fun_Name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fun_Description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `donationfund_fun`
--

INSERT INTO `donationfund_fun` (`fun_ID`, `fun_Active`, `fun_Name`, `fun_Description`) VALUES
(1, 'true', 'Pledges', 'Pledge income for the operating budget');

-- --------------------------------------------------------

--
-- Table structure for table `egive_egv`
--

CREATE TABLE `egive_egv` (
  `egv_egiveID` varchar(16) CHARACTER SET utf8 NOT NULL,
  `egv_famID` int(11) NOT NULL,
  `egv_DateEntered` datetime NOT NULL,
  `egv_DateLastEdited` datetime NOT NULL,
  `egv_EnteredBy` smallint(6) NOT NULL DEFAULT 0,
  `egv_EditedBy` smallint(6) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `email_count`
-- (See below for the actual view)
--
CREATE TABLE `email_count` (
`email` varchar(100)
,`total` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `email_list`
-- (See below for the actual view)
--
CREATE TABLE `email_list` (
`email` varchar(100)
,`type` varchar(11)
,`id` mediumint(9) unsigned
);

-- --------------------------------------------------------

--
-- Table structure for table `email_message_pending_emp`
--

CREATE TABLE `email_message_pending_emp` (
  `emp_usr_id` mediumint(9) UNSIGNED NOT NULL DEFAULT 0,
  `emp_to_send` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `emp_subject` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `emp_message` text COLLATE utf8_unicode_ci NOT NULL,
  `emp_attach_name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `emp_attach` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_recipient_pending_erp`
--

CREATE TABLE `email_recipient_pending_erp` (
  `erp_id` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `erp_usr_id` mediumint(9) UNSIGNED NOT NULL DEFAULT 0,
  `erp_num_attempt` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `erp_failed_time` datetime DEFAULT NULL,
  `erp_email_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `eventcountnames_evctnm`
--

CREATE TABLE `eventcountnames_evctnm` (
  `evctnm_countid` int(5) NOT NULL,
  `evctnm_eventtypeid` smallint(5) NOT NULL DEFAULT 0,
  `evctnm_countname` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `evctnm_notes` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `eventcountnames_evctnm`
--

INSERT INTO `eventcountnames_evctnm` (`evctnm_countid`, `evctnm_eventtypeid`, `evctnm_countname`, `evctnm_notes`) VALUES
(1, 1, 'Total', ''),
(2, 1, 'Members', ''),
(3, 1, 'Visitors', ''),
(4, 2, 'Total', ''),
(5, 2, 'Members', ''),
(6, 2, 'Visitors', '');

-- --------------------------------------------------------

--
-- Table structure for table `eventcounts_evtcnt`
--

CREATE TABLE `eventcounts_evtcnt` (
  `evtcnt_eventid` int(5) NOT NULL DEFAULT 0,
  `evtcnt_countid` int(5) NOT NULL DEFAULT 0,
  `evtcnt_countname` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `evtcnt_countcount` int(6) DEFAULT NULL,
  `evtcnt_notes` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `eventcounts_evtcnt`
--

INSERT INTO `eventcounts_evtcnt` (`evtcnt_eventid`, `evtcnt_countid`, `evtcnt_countname`, `evtcnt_countcount`, `evtcnt_notes`) VALUES
(1, 1, 'Total', 0, ''),
(1, 2, 'Members', 1, ''),
(1, 3, 'Visitors', 0, ''),
(2, 16, 'Members', 0, ''),
(2, 17, 'Visitors', 0, ''),
(2, 18, 'Total', 0, ''),
(3, 1, 'Total', 0, ''),
(3, 2, 'Members', 0, ''),
(3, 3, 'Visitors', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `events_event`
--

CREATE TABLE `events_event` (
  `event_id` int(11) NOT NULL,
  `event_type` int(11) NOT NULL DEFAULT 0,
  `event_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `event_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_text` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_start` datetime NOT NULL,
  `event_end` datetime NOT NULL,
  `inactive` int(1) NOT NULL DEFAULT 0,
  `event_typename` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `location_id` int(11) DEFAULT NULL,
  `primary_contact_person_id` int(11) DEFAULT NULL,
  `secondary_contact_person_id` int(11) DEFAULT NULL,
  `event_url` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `events_event`
--

INSERT INTO `events_event` (`event_id`, `event_type`, `event_title`, `event_desc`, `event_text`, `event_start`, `event_end`, `inactive`, `event_typename`, `location_id`, `primary_contact_person_id`, `secondary_contact_person_id`, `event_url`) VALUES
(1, 1, '2021-06-20-Church Service', 'Church service', '', '2021-06-20 09:00:00', '2021-06-20 11:00:00', 1, '', NULL, NULL, NULL, NULL),
(2, 5, '2021-06-25-Intercessory Prayer', 'MC - Stephany\r\nExhorter - Bryan', '', '2021-06-25 19:00:00', '2021-06-25 21:00:00', 1, '', NULL, NULL, NULL, NULL),
(3, 1, '2021-06-27-Church Service', 'desc', '', '2021-06-27 10:30:00', '2021-06-27 11:30:00', 0, '', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event_attend`
--

CREATE TABLE `event_attend` (
  `attend_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL DEFAULT 0,
  `person_id` int(11) NOT NULL DEFAULT 0,
  `checkin_date` datetime DEFAULT NULL,
  `checkin_id` int(11) DEFAULT NULL,
  `checkout_date` datetime DEFAULT NULL,
  `checkout_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event_attend`
--

INSERT INTO `event_attend` (`attend_id`, `event_id`, `person_id`, `checkin_date`, `checkin_id`, `checkout_date`, `checkout_id`) VALUES
(5, 1, 2, '2021-06-25 00:25:29', NULL, '2021-06-25 20:44:01', NULL),
(6, 1, 3, '2021-06-25 00:25:42', NULL, '2021-06-25 20:44:58', NULL),
(7, 2, 2, '2021-06-25 20:43:30', NULL, '2021-06-25 20:47:54', NULL),
(8, 3, 1, '2021-11-16 22:07:19', NULL, '2021-11-16 22:09:24', NULL),
(10, 3, 2, '2021-11-22 00:03:09', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event_audience`
--

CREATE TABLE `event_audience` (
  `event_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_types`
--

CREATE TABLE `event_types` (
  `type_id` int(11) NOT NULL,
  `type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type_defstarttime` time NOT NULL DEFAULT '00:00:00',
  `type_defrecurtype` enum('none','weekly','monthly','yearly') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'none',
  `type_defrecurDOW` enum('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Sunday',
  `type_defrecurDOM` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `type_defrecurDOY` date NOT NULL DEFAULT '2000-01-01',
  `type_active` int(1) NOT NULL DEFAULT 1,
  `type_grpid` mediumint(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event_types`
--

INSERT INTO `event_types` (`type_id`, `type_name`, `type_defstarttime`, `type_defrecurtype`, `type_defrecurDOW`, `type_defrecurDOM`, `type_defrecurDOY`, `type_active`, `type_grpid`) VALUES
(1, 'Church Service', '10:30:00', 'weekly', 'Sunday', '', '2016-01-01', 1, NULL),
(2, 'Sunday School', '09:30:00', 'weekly', 'Sunday', '', '2016-01-01', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `family_custom`
--

CREATE TABLE `family_custom` (
  `fam_ID` mediumint(9) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `family_custom_master`
--

CREATE TABLE `family_custom_master` (
  `fam_custom_Order` smallint(6) NOT NULL DEFAULT 0,
  `fam_custom_Field` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fam_custom_Name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fam_custom_Special` mediumint(8) UNSIGNED DEFAULT NULL,
  `fam_custom_FieldSec` tinyint(4) NOT NULL DEFAULT 1,
  `type_ID` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `family_fam`
--

CREATE TABLE `family_fam` (
  `fam_ID` mediumint(9) UNSIGNED NOT NULL,
  `fam_Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_Address1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_Address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_City` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_State` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_Zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_Country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_HomePhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_WorkPhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_CellPhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_Email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_WeddingDate` date DEFAULT NULL,
  `fam_DateEntered` datetime NOT NULL,
  `fam_DateLastEdited` datetime DEFAULT NULL,
  `fam_EnteredBy` smallint(5) NOT NULL DEFAULT 0,
  `fam_EditedBy` smallint(5) UNSIGNED DEFAULT 0,
  `fam_scanCheck` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_scanCredit` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `fam_SendNewsLetter` enum('FALSE','TRUE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FALSE',
  `fam_DateDeactivated` date DEFAULT NULL,
  `fam_OkToCanvass` enum('FALSE','TRUE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FALSE',
  `fam_Canvasser` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `fam_Latitude` double DEFAULT NULL,
  `fam_Longitude` double DEFAULT NULL,
  `fam_Envelope` mediumint(9) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fundraiser_fr`
--

CREATE TABLE `fundraiser_fr` (
  `fr_ID` mediumint(9) UNSIGNED NOT NULL,
  `fr_date` date DEFAULT NULL,
  `fr_title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `fr_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `fr_EnteredBy` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `fr_EnteredDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groupprop_3`
--

CREATE TABLE `groupprop_3` (
  `per_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `c1` tinyint(4) DEFAULT NULL,
  `c2` mediumint(9) DEFAULT NULL,
  `c3` date DEFAULT NULL,
  `c4` varchar(50) DEFAULT NULL,
  `c5` varchar(50) DEFAULT NULL,
  `c6` int(11) DEFAULT NULL,
  `c7` date DEFAULT NULL,
  `c8` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groupprop_3`
--

INSERT INTO `groupprop_3` (`per_ID`, `c1`, `c2`, `c3`, `c4`, `c5`, `c6`, `c7`, `c8`) VALUES
(2, 1, 2, '2021-11-22', '5:00 PM', '6:00 PM', 9, '2021-11-29', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `groupprop_4`
--

CREATE TABLE `groupprop_4` (
  `per_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `c1` tinyint(4) DEFAULT NULL,
  `c2` mediumint(9) DEFAULT NULL,
  `c3` date DEFAULT NULL,
  `c4` varchar(50) DEFAULT NULL,
  `c5` varchar(50) DEFAULT NULL,
  `c6` int(11) DEFAULT NULL,
  `c7` date DEFAULT NULL,
  `c8` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groupprop_4`
--

INSERT INTO `groupprop_4` (`per_ID`, `c1`, `c2`, `c3`, `c4`, `c5`, `c6`, `c7`, `c8`) VALUES
(3, 1, 2, '2021-11-22', '5:00 PM', '6:00 PM', 9, '2021-11-29', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `groupprop_5`
--

CREATE TABLE `groupprop_5` (
  `per_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `c1` tinyint(4) DEFAULT NULL,
  `c2` mediumint(9) DEFAULT NULL,
  `c3` date DEFAULT NULL,
  `c4` varchar(50) DEFAULT NULL,
  `c5` varchar(50) DEFAULT NULL,
  `c6` int(11) DEFAULT NULL,
  `c7` date DEFAULT NULL,
  `c8` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groupprop_6`
--

CREATE TABLE `groupprop_6` (
  `per_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `c1` tinyint(4) DEFAULT NULL,
  `c2` mediumint(9) DEFAULT NULL,
  `c3` date DEFAULT NULL,
  `c4` varchar(50) DEFAULT NULL,
  `c5` varchar(50) DEFAULT NULL,
  `c6` int(11) DEFAULT NULL,
  `c7` date DEFAULT NULL,
  `c8` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groupprop_7`
--

CREATE TABLE `groupprop_7` (
  `per_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `c1` tinyint(4) DEFAULT NULL,
  `c2` mediumint(9) DEFAULT NULL,
  `c3` date DEFAULT NULL,
  `c4` varchar(50) DEFAULT NULL,
  `c5` varchar(50) DEFAULT NULL,
  `c6` int(11) DEFAULT NULL,
  `c7` date DEFAULT NULL,
  `c8` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groupprop_8`
--

CREATE TABLE `groupprop_8` (
  `per_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `c1` tinyint(4) DEFAULT NULL,
  `c2` mediumint(9) DEFAULT NULL,
  `c3` date DEFAULT NULL,
  `c4` varchar(50) DEFAULT NULL,
  `c5` varchar(50) DEFAULT NULL,
  `c6` int(11) DEFAULT NULL,
  `c7` date DEFAULT NULL,
  `c8` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groupprop_9`
--

CREATE TABLE `groupprop_9` (
  `per_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `c1` tinyint(4) DEFAULT NULL,
  `c2` mediumint(9) DEFAULT NULL,
  `c3` date DEFAULT NULL,
  `c4` varchar(50) DEFAULT NULL,
  `c5` varchar(50) DEFAULT NULL,
  `c6` int(11) DEFAULT NULL,
  `c7` date DEFAULT NULL,
  `c8` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groupprop_master`
--

CREATE TABLE `groupprop_master` (
  `id` int(11) NOT NULL,
  `grp_ID` mediumint(9) UNSIGNED NOT NULL DEFAULT 0,
  `prop_ID` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `prop_Field` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `prop_Name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prop_Description` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_ID` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `prop_Special` mediumint(9) UNSIGNED DEFAULT NULL,
  `prop_PersonDisplay` enum('false','true') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Group-specific properties order, name, description, type';

--
-- Dumping data for table `groupprop_master`
--

INSERT INTO `groupprop_master` (`id`, `grp_ID`, `prop_ID`, `prop_Field`, `prop_Name`, `prop_Description`, `type_ID`, `prop_Special`, `prop_PersonDisplay`) VALUES
(1, 3, 1, 'c1', 'Status', '', 12, 22, 'true'),
(2, 3, 2, 'c2', 'Name of Teacher', '', 9, 3, 'true'),
(3, 3, 3, 'c3', 'Date Taken', '', 2, NULL, 'true'),
(4, 3, 4, 'c4', 'Time Started', '', 3, NULL, 'true'),
(5, 3, 5, 'c5', 'Time Ended', '', 3, NULL, 'true'),
(6, 3, 6, 'c6', 'Score', '', 8, NULL, 'true'),
(7, 3, 7, 'c7', 'Next Class Date', '', 2, NULL, 'true'),
(8, 3, 8, 'c8', 'Remarks', '', 4, NULL, 'true'),
(9, 4, 1, 'c1', 'Status', '', 12, 22, 'true'),
(10, 4, 2, 'c2', 'Name of Teacher', '', 9, 3, 'true'),
(11, 4, 3, 'c3', 'Date Taken', '', 2, NULL, 'true'),
(12, 4, 4, 'c4', 'Time Started', '', 3, NULL, 'true'),
(13, 4, 5, 'c5', 'Time Ended', '', 3, NULL, 'true'),
(14, 4, 6, 'c6', 'Score', '', 8, NULL, 'true'),
(15, 4, 7, 'c7', 'Next Class Date', '', 2, NULL, 'true'),
(16, 4, 8, 'c8', 'Remarks', '', 4, NULL, 'true'),
(17, 5, 1, 'c1', 'Status', '', 12, 22, 'true'),
(18, 5, 2, 'c2', 'Name of Teacher', '', 9, 3, 'true'),
(19, 5, 3, 'c3', 'Date Taken', '', 2, NULL, 'true'),
(20, 5, 4, 'c4', 'Time Started', '', 3, NULL, 'true'),
(21, 5, 5, 'c5', 'Time Ended', '', 3, NULL, 'true'),
(22, 5, 6, 'c6', 'Score', '', 8, NULL, 'true'),
(23, 5, 7, 'c7', 'Next Class Date', '', 2, NULL, 'true'),
(24, 5, 8, 'c8', 'Remarks', '', 4, NULL, 'true'),
(25, 6, 1, 'c1', 'Status', '', 12, 22, 'true'),
(26, 6, 2, 'c2', 'Name of Teacher', '', 9, 3, 'true'),
(27, 6, 3, 'c3', 'Date Taken', '', 2, NULL, 'true'),
(28, 6, 4, 'c4', 'Time Started', '', 3, NULL, 'true'),
(29, 6, 5, 'c5', 'Time Ended', '', 3, NULL, 'true'),
(30, 6, 6, 'c6', 'Score', '', 8, NULL, 'true'),
(31, 6, 7, 'c7', 'Next Class Date', '', 2, NULL, 'true'),
(32, 6, 8, 'c8', 'Remarks', '', 4, NULL, 'true'),
(33, 7, 1, 'c1', 'Status', '', 12, 22, 'true'),
(34, 7, 2, 'c2', 'Name of Teacher', '', 9, 3, 'true'),
(35, 7, 3, 'c3', 'Date Taken', '', 2, NULL, 'true'),
(36, 7, 4, 'c4', 'Time Started', '', 3, NULL, 'true'),
(37, 7, 5, 'c5', 'Time Ended', '', 3, NULL, 'true'),
(38, 7, 6, 'c6', 'Score', '', 8, NULL, 'true'),
(39, 7, 7, 'c7', 'Next Class Date', '', 2, NULL, 'true'),
(40, 7, 8, 'c8', 'Remarks', '', 4, NULL, 'true'),
(41, 8, 1, 'c1', 'Status', '', 12, 22, 'true'),
(42, 8, 2, 'c2', 'Name of Teacher', '', 9, 3, 'true'),
(43, 8, 3, 'c3', 'Date Taken', '', 2, NULL, 'true'),
(44, 8, 4, 'c4', 'Time Started', '', 3, NULL, 'true'),
(45, 8, 5, 'c5', 'Time Ended', '', 3, NULL, 'true'),
(46, 8, 6, 'c6', 'Score', '', 8, NULL, 'true'),
(47, 8, 7, 'c7', 'Next Class Date', '', 2, NULL, 'true'),
(48, 8, 8, 'c8', 'Remarks', '', 4, NULL, 'true'),
(49, 9, 1, 'c1', 'Status', '', 12, 22, 'true'),
(50, 9, 2, 'c2', 'Name of Teacher', '', 9, 3, 'true'),
(51, 9, 3, 'c3', 'Date Taken', '', 2, NULL, 'true'),
(52, 9, 4, 'c4', 'Time Started', '', 3, NULL, 'true'),
(53, 9, 5, 'c5', 'Time Ended', '', 3, NULL, 'true'),
(54, 9, 6, 'c6', 'Score', '', 8, NULL, 'true'),
(55, 9, 7, 'c7', 'Next Class Date', '', 2, NULL, 'true'),
(56, 9, 8, 'c8', 'Remarks', '', 4, NULL, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `group_grp`
--

CREATE TABLE `group_grp` (
  `grp_ID` mediumint(8) UNSIGNED NOT NULL,
  `grp_Type` tinyint(4) NOT NULL DEFAULT 0,
  `grp_RoleListID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `grp_DefaultRole` mediumint(9) NOT NULL DEFAULT 0,
  `grp_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `grp_Description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `grp_hasSpecialProps` tinyint(1) NOT NULL DEFAULT 0,
  `grp_active` tinyint(1) NOT NULL DEFAULT 1,
  `grp_include_email_export` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `group_grp`
--

INSERT INTO `group_grp` (`grp_ID`, `grp_Type`, `grp_RoleListID`, `grp_DefaultRole`, `grp_Name`, `grp_Description`, `grp_hasSpecialProps`, `grp_active`, `grp_include_email_export`) VALUES
(1, 8, 13, 1, 'Worship Team', '', 0, 1, 0),
(2, 5, 14, 1, 'M1 - Muntinlupa Cluster', '', 0, 1, 1),
(3, 4, 15, 2, 'NCC L1 - Obedience', '', 1, 1, 0),
(4, 4, 16, 2, 'NCC L2 - Gospel', NULL, 1, 1, 1),
(5, 4, 17, 2, 'NCC L3 - Godhead', NULL, 1, 1, 1),
(6, 4, 18, 2, 'NCC L4 - Clustering', NULL, 1, 1, 1),
(7, 4, 19, 2, 'NCC L5 - Prayer', NULL, 1, 1, 1),
(8, 4, 20, 2, 'NCC L6 - Fasting', NULL, 1, 1, 1),
(9, 4, 21, 2, 'NCC L7 - Tithing', NULL, 1, 1, 1),
(10, 4, 23, 2, 'Doctrines', NULL, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `istlookup_lu`
--

CREATE TABLE `istlookup_lu` (
  `lu_fam_ID` mediumint(9) NOT NULL DEFAULT 0,
  `lu_LookupDateTime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `lu_DeliveryLine1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_DeliveryLine2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_City` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_State` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_ZipAddon` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_Zip` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_Addon` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_LOTNumber` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_DPCCheckdigit` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_RecordType` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_LastLine` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_CarrierRoute` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_ReturnCodes` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_ErrorCodes` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lu_ErrorDesc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='US Address Verification Lookups From Intelligent Search Tech';

-- --------------------------------------------------------

--
-- Table structure for table `kioskassginment_kasm`
--

CREATE TABLE `kioskassginment_kasm` (
  `kasm_ID` mediumint(9) UNSIGNED NOT NULL,
  `kasm_kdevId` mediumint(9) DEFAULT NULL,
  `kasm_AssignmentType` mediumint(9) DEFAULT NULL,
  `kasm_EventId` mediumint(9) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kioskdevice_kdev`
--

CREATE TABLE `kioskdevice_kdev` (
  `kdev_ID` mediumint(9) UNSIGNED NOT NULL,
  `kdev_GUIDHash` char(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kdev_Name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kdev_deviceType` mediumint(9) NOT NULL DEFAULT 0,
  `kdev_lastHeartbeat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `kdev_Accepted` tinyint(1) DEFAULT NULL,
  `kdev_PendingCommands` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `list_lst`
--

CREATE TABLE `list_lst` (
  `id` int(11) NOT NULL,
  `lst_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `lst_OptionID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `lst_OptionSequence` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `lst_OptionName` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `list_lst`
--

INSERT INTO `list_lst` (`id`, `lst_ID`, `lst_OptionID`, `lst_OptionSequence`, `lst_OptionName`) VALUES
(4, 1, 5, 1, 'Active'),
(6, 2, 1, 1, 'Head of Household'),
(7, 2, 2, 2, 'Spouse'),
(8, 2, 3, 3, 'Child'),
(9, 2, 4, 4, 'Other Relative'),
(10, 2, 5, 5, 'Non Relative'),
(11, 3, 4, 1, 'Sunday School Class'),
(12, 4, 1, 1, 'True / False'),
(13, 4, 2, 2, 'Date'),
(14, 4, 3, 3, 'Text Field (50 char)'),
(15, 4, 4, 4, 'Text Field (100 char)'),
(16, 4, 5, 5, 'Text Field (Long)'),
(17, 4, 6, 6, 'Year'),
(18, 4, 7, 7, 'Season'),
(19, 4, 8, 8, 'Number'),
(20, 4, 9, 9, 'Person from Group'),
(21, 4, 10, 10, 'Money'),
(22, 4, 11, 11, 'Phone Number'),
(23, 4, 12, 12, 'Custom Drop-Down List'),
(24, 5, 1, 1, 'bAll'),
(25, 5, 2, 2, 'bAdmin'),
(26, 5, 3, 3, 'bAddRecords'),
(27, 5, 4, 4, 'bEditRecords'),
(28, 5, 5, 5, 'bDeleteRecords'),
(29, 5, 7, 7, 'bManageGroups'),
(30, 5, 8, 8, 'bFinance'),
(31, 5, 9, 9, 'bNotes'),
(32, 5, 11, 11, 'bCanvasser'),
(33, 10, 1, 1, 'Teacher'),
(34, 10, 2, 2, 'Student'),
(35, 11, 1, 1, 'Member'),
(36, 12, 1, 1, 'Teacher'),
(37, 12, 2, 2, 'Student'),
(42, 2, 1, 1, 'Head of Household'),
(43, 2, 2, 2, 'Spouse'),
(44, 2, 3, 3, 'Child'),
(45, 2, 4, 4, 'Other Relative'),
(46, 2, 5, 5, 'Non Relative'),
(48, 4, 1, 1, 'True / False'),
(49, 4, 2, 2, 'Date'),
(50, 4, 3, 3, 'Text Field (50 char)'),
(51, 4, 4, 4, 'Text Field (100 char)'),
(52, 4, 5, 5, 'Text Field (Long)'),
(53, 4, 6, 6, 'Year'),
(54, 4, 7, 7, 'Season'),
(55, 4, 8, 8, 'Number'),
(56, 4, 9, 9, 'Person from Group'),
(57, 4, 10, 10, 'Money'),
(58, 4, 11, 11, 'Phone Number'),
(59, 4, 12, 12, 'Custom Drop-Down List'),
(60, 5, 1, 1, 'bAll'),
(61, 5, 2, 2, 'bAdmin'),
(62, 5, 3, 3, 'bAddRecords'),
(63, 5, 4, 4, 'bEditRecords'),
(64, 5, 5, 5, 'bDeleteRecords'),
(65, 5, 7, 7, 'bManageGroups'),
(66, 5, 8, 8, 'bFinance'),
(67, 5, 9, 9, 'bNotes'),
(68, 5, 11, 11, 'bCanvasser'),
(69, 10, 1, 1, 'Teacher'),
(70, 10, 2, 2, 'Student'),
(71, 11, 1, 1, 'Member'),
(72, 12, 1, 1, 'Teacher'),
(73, 12, 2, 2, 'Student'),
(74, 13, 1, 1, 'Member'),
(75, 14, 1, 2, 'Member'),
(76, 3, 5, 2, 'Cluster'),
(77, 3, 6, 3, 'B2M'),
(78, 15, 1, 1, 'Teacher'),
(79, 15, 2, 2, 'Student'),
(80, 16, 1, 1, 'Teacher'),
(81, 16, 2, 2, 'Student'),
(82, 17, 1, 1, 'Teacher'),
(83, 17, 2, 2, 'Student'),
(84, 18, 1, 1, 'Teacher'),
(85, 18, 2, 2, 'Student'),
(86, 19, 1, 1, 'Teacher'),
(87, 19, 2, 2, 'Student'),
(88, 20, 1, 1, 'Teacher'),
(89, 20, 2, 2, 'Student'),
(90, 21, 1, 1, 'Teacher'),
(91, 21, 2, 2, 'Student'),
(92, 22, 1, 1, 'Not yet taken'),
(93, 22, 2, 2, 'Ongoing'),
(94, 22, 3, 3, 'Finished'),
(96, 23, 1, 1, 'Teacher'),
(97, 23, 2, 2, 'Student'),
(103, 2, 1, 1, 'Head of Household'),
(104, 2, 2, 2, 'Spouse'),
(105, 2, 3, 3, 'Child'),
(106, 2, 4, 4, 'Other Relative'),
(107, 2, 5, 5, 'Non Relative'),
(109, 4, 1, 1, 'True / False'),
(110, 4, 2, 2, 'Date'),
(111, 4, 3, 3, 'Text Field (50 char)'),
(112, 4, 4, 4, 'Text Field (100 char)'),
(113, 4, 5, 5, 'Text Field (Long)'),
(114, 4, 6, 6, 'Year'),
(115, 4, 7, 7, 'Season'),
(116, 4, 8, 8, 'Number'),
(117, 4, 9, 9, 'Person from Group'),
(118, 4, 10, 10, 'Money'),
(119, 4, 11, 11, 'Phone Number'),
(120, 4, 12, 12, 'Custom Drop-Down List'),
(121, 5, 1, 1, 'bAll'),
(122, 5, 2, 2, 'bAdmin'),
(123, 5, 3, 3, 'bAddRecords'),
(124, 5, 4, 4, 'bEditRecords'),
(125, 5, 5, 5, 'bDeleteRecords'),
(126, 5, 7, 7, 'bManageGroups'),
(127, 5, 8, 8, 'bFinance'),
(128, 5, 9, 9, 'bNotes'),
(129, 5, 11, 11, 'bCanvasser'),
(130, 10, 1, 1, 'Teacher'),
(131, 10, 2, 2, 'Student'),
(132, 11, 1, 1, 'Member'),
(133, 12, 1, 1, 'Teacher'),
(134, 12, 2, 2, 'Student'),
(135, 24, 1, 1, 'Active'),
(136, 24, 2, 2, 'Inactive'),
(137, 24, 3, 3, 'Disfellowship'),
(138, 24, 4, 4, 'Returning'),
(139, 24, 5, 5, 'Fall Away'),
(140, 24, 6, 6, 'Abroad'),
(141, 24, 7, 7, 'Deceased'),
(143, 3, 7, 4, 'Golden Warriors'),
(144, 3, 8, 5, 'Ministry'),
(145, 14, 2, 1, 'Coordinator'),
(148, 1, 8, 8, 'Fall Away'),
(149, 1, 9, 6, 'Abroad'),
(150, 1, 10, 7, 'Deceased'),
(151, 1, 11, 2, 'Inactive'),
(152, 1, 12, 5, 'Disfellowship'),
(153, 1, 13, 4, 'Returning'),
(154, 1, 14, 3, 'New Convert');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `location_id` int(11) NOT NULL,
  `location_typeId` int(11) NOT NULL,
  `location_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `location_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `location_city` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `location_state` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `location_zip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `location_country` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `location_phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_timzezone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menu_links`
--

CREATE TABLE `menu_links` (
  `linkId` mediumint(8) UNSIGNED NOT NULL,
  `linkName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkUri` text COLLATE utf8_unicode_ci NOT NULL,
  `linkOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `multibuy_mb`
--

CREATE TABLE `multibuy_mb` (
  `mb_ID` mediumint(9) UNSIGNED NOT NULL,
  `mb_per_ID` mediumint(9) NOT NULL DEFAULT 0,
  `mb_item_ID` mediumint(9) NOT NULL DEFAULT 0,
  `mb_count` decimal(8,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `nc_logs`
--

CREATE TABLE `nc_logs` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `per_ID` mediumint(8) NOT NULL,
  `grp_ID` int(11) NOT NULL,
  `c1` tinyint(4) DEFAULT NULL,
  `c2` mediumint(9) DEFAULT NULL,
  `c3` date DEFAULT NULL,
  `c4` varchar(50) DEFAULT NULL,
  `c5` varchar(50) DEFAULT NULL,
  `c6` int(11) DEFAULT NULL,
  `c7` date DEFAULT NULL,
  `c8` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nc_logs`
--

INSERT INTO `nc_logs` (`id`, `per_ID`, `grp_ID`, `c1`, `c2`, `c3`, `c4`, `c5`, `c6`, `c7`, `c8`) VALUES
(3, 2, 3, 1, 2, '2021-11-22', '5:00 PM', '6:00 PM', 9, '2021-11-29', 'test'),
(4, 3, 4, 1, 2, '2021-11-22', '5:00 PM', '6:00 PM', 9, '2021-11-29', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `note_nte`
--

CREATE TABLE `note_nte` (
  `nte_ID` mediumint(8) UNSIGNED NOT NULL,
  `nte_per_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `nte_fam_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `nte_Private` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `nte_Text` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `nte_DateEntered` datetime NOT NULL,
  `nte_DateLastEdited` datetime DEFAULT NULL,
  `nte_EnteredBy` mediumint(8) NOT NULL DEFAULT 0,
  `nte_EditedBy` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `nte_Type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `note_nte`
--

INSERT INTO `note_nte` (`nte_ID`, `nte_per_ID`, `nte_fam_ID`, `nte_Private`, `nte_Text`, `nte_DateEntered`, `nte_DateLastEdited`, `nte_EnteredBy`, `nte_EditedBy`, `nte_Type`) VALUES
(1, 1, 0, 0, 'system user changed password', '2021-11-16 22:00:19', NULL, 1, 0, 'user'),
(2, 2, 0, 0, 'Created', '2021-11-18 20:02:51', NULL, 1, 0, 'create'),
(3, 2, 0, 0, 'Added to group: NCC L1 - Obedience', '2021-11-22 01:02:13', NULL, 1, 0, 'group'),
(4, 3, 0, 0, 'Created', '2021-11-22 01:18:24', NULL, 1, 0, 'create'),
(5, 3, 0, 0, 'Added to group: NCC L1 - Obedience', '2021-11-22 01:19:09', NULL, 1, 0, 'group'),
(6, 3, 0, 0, 'Deleted from group: NCC L1 - Obedience', '2021-11-22 03:26:25', NULL, 1, 0, 'group'),
(7, 3, 0, 0, 'Added to group: NCC L1 - Obedience', '2021-11-22 03:26:56', NULL, 1, 0, 'group'),
(8, 2, 0, 0, 'Deleted from group: NCC L1 - Obedience', '2021-11-24 17:24:33', NULL, 1, 0, 'group'),
(9, 2, 0, 0, 'Added to group: NCC L1 - Obedience', '2021-11-24 17:25:30', NULL, 1, 0, 'group'),
(10, 2, 0, 0, 'Added Report: <br><div class=\"box-body\"><strong>Status</strong>: Not yet taken<br/><strong>Name of Teacher</strong>: Mark James Hernandez<br/><strong>Date Taken</strong>: November 22 2021<br/><strong>Time Started</strong>: 5:00 PM<br/><strong>Time Ended</strong>: 6:00 PM<br/><strong>Score</strong>: 9<br/><strong>Next Class Date</strong>: November 29 2021<br/><strong>Remarks</strong>: test<br/></div>', '2021-11-24 18:28:09', NULL, 1, 0, 'nc'),
(11, 3, 0, 0, 'Updated', '2021-12-01 21:27:42', NULL, 1, 0, 'edit'),
(12, 3, 0, 0, 'Added to group: NCC L2 - Gospel', '2021-12-01 22:23:17', NULL, 1, 0, 'group'),
(13, 3, 0, 0, 'Added Report: <br><div class=\"box-body\"><strong>Status</strong>: Not yet taken<br/><strong>Name of Teacher</strong>: Mark James Hernandez<br/><strong>Date Taken</strong>: November 22 2021<br/><strong>Time Started</strong>: 5:00 PM<br/><strong>Time Ended</strong>: 6:00 PM<br/><strong>Score</strong>: 9<br/><strong>Next Class Date</strong>: November 29 2021<br/><strong>Remarks</strong>: test<br/></div>', '2021-12-01 22:25:18', NULL, 1, 0, 'nc'),
(14, 4, 0, 0, 'Updated', '2021-12-01 22:49:14', NULL, 1, 0, 'edit');

-- --------------------------------------------------------

--
-- Table structure for table `paddlenum_pn`
--

CREATE TABLE `paddlenum_pn` (
  `pn_ID` mediumint(9) UNSIGNED NOT NULL,
  `pn_fr_ID` mediumint(9) UNSIGNED DEFAULT NULL,
  `pn_Num` mediumint(9) UNSIGNED DEFAULT NULL,
  `pn_per_ID` mediumint(9) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `permission_id` int(11) NOT NULL,
  `permission_name` varchar(50) NOT NULL,
  `permission_desc` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`permission_id`, `permission_name`, `permission_desc`) VALUES
(1, 'addPeople', 'Add People'),
(3, 'updatePeople', 'Update People'),
(4, 'deletePeopleRecords', 'Delete People Records'),
(5, 'curdProperties', 'Manage Properties '),
(6, 'crudClassifications', 'Manage Classifications'),
(7, 'crudGroups', 'Manage Groups'),
(8, 'crudRoles', 'Manage Roles'),
(9, 'crudDonations', 'Manage Donations'),
(10, 'curdFinance', 'Manage Finance'),
(11, 'curdNotes', 'Manage Notes'),
(12, 'canvasser', 'Canvasser volunteer'),
(13, 'editSelf', 'Edit own family only'),
(14, 'emailMailto', 'Allow to see Mailto Links'),
(15, 'createDirectory', 'Create Directories'),
(16, 'exportCSV', 'Export CSV files'),
(17, 'usAddressVerification', 'Use IST Address Verification'),
(18, 'crudEvent', 'Manage Events');

-- --------------------------------------------------------

--
-- Table structure for table `person2group2role_p2g2r`
--

CREATE TABLE `person2group2role_p2g2r` (
  `p2g2r_per_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `p2g2r_grp_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `p2g2r_rle_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person2group2role_p2g2r`
--

INSERT INTO `person2group2role_p2g2r` (`p2g2r_per_ID`, `p2g2r_grp_ID`, `p2g2r_rle_ID`) VALUES
(2, 3, 2),
(3, 3, 1),
(3, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `person2volunteeropp_p2vo`
--

CREATE TABLE `person2volunteeropp_p2vo` (
  `p2vo_ID` mediumint(9) NOT NULL,
  `p2vo_per_ID` mediumint(9) DEFAULT NULL,
  `p2vo_vol_ID` mediumint(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `person_custom`
--

CREATE TABLE `person_custom` (
  `per_ID` mediumint(9) NOT NULL DEFAULT 0,
  `c1` date DEFAULT NULL,
  `c2` date DEFAULT NULL,
  `c3` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person_custom`
--

INSERT INTO `person_custom` (`per_ID`, `c1`, `c2`, `c3`) VALUES
(2, NULL, NULL, NULL),
(3, '2011-05-11', '2011-05-11', 1),
(4, '2020-11-05', '2020-11-05', NULL),
(5, '1992-09-26', '1993-04-11', 0),
(6, '2011-03-19', '2011-07-11', 0),
(7, '2017-07-02', '2017-02-26', 0),
(8, '2013-08-11', '2013-03-31', 0),
(9, '1994-02-13', '1994-07-31', 0),
(10, '2009-09-06', '2009-09-03', 0),
(11, '2009-09-22', '2009-09-22', 0),
(12, '2011-05-01', '2011-05-01', 0),
(13, '2011-05-01', '2011-05-01', 0),
(14, '2016-05-07', '2015-11-15', 0),
(15, '1999-11-28', '1999-12-05', 0),
(16, '1992-09-15', '1993-01-10', 0),
(17, '2014-06-27', '2014-07-20', 0),
(18, '2016-10-29', '2016-07-07', 0),
(19, '2018-10-28', '2018-08-19', 0),
(20, '2020-01-19', '2019-09-22', 0),
(21, '2020-01-19', '2020-11-08', 0),
(22, '2018-10-07', '2018-07-15', 0),
(23, '2020-09-06', '2020-11-08', 0),
(24, '2020-10-01', '2020-11-01', 0),
(25, '2020-12-13', '2019-12-01', 0),
(26, '2021-01-10', '2020-12-24', 0),
(27, '2021-01-10', '2021-01-24', 0),
(28, '2021-02-07', '2021-01-24', 0),
(29, '2021-03-07', '2021-02-28', 0),
(30, '2021-03-20', '0000-00-00', 0),
(31, '2021-03-27', '2021-03-21', 0),
(32, '2020-12-20', '2021-01-31', 0),
(33, '2020-11-08', '2020-11-15', 0),
(34, '2020-01-19', '2019-11-17', 0),
(35, '2016-12-31', '2017-02-12', 0),
(36, '2020-12-20', '2020-12-20', 0),
(37, '2014-01-04', '2014-12-23', 0),
(38, '1996-12-06', '1996-12-06', 0),
(39, '2017-12-25', '2018-03-01', 0),
(40, '2018-09-23', '2017-07-07', 0),
(41, '2018-09-23', '2016-12-24', 0),
(42, '2008-04-01', '2008-12-23', 0),
(43, '2018-11-01', '2016-08-18', 0),
(44, '2018-09-23', '0000-00-00', 0),
(45, '1994-03-30', '1994-03-03', 0),
(46, '2005-06-17', '1995-01-01', 0),
(47, '0000-00-00', '0000-00-00', 0),
(48, '2012-07-27', '2012-06-24', 0),
(49, '2010-05-01', '2010-05-01', 0),
(50, '1994-01-11', '1995-04-13', 0),
(51, '2016-09-09', '2016-01-10', 0),
(52, '1993-02-09', '1993-04-11', 0),
(53, '0000-00-00', '0000-00-00', 0),
(54, '2012-10-01', '2012-06-01', 0),
(55, '2021-03-21', '2021-03-21', 0),
(56, '2005-07-07', '2015-01-01', 0),
(57, '2018-09-23', '2018-05-06', 0),
(58, '2009-01-01', '2008-12-01', 0),
(59, '2010-01-01', '2009-12-01', 0),
(60, '2010-01-01', '2009-12-01', 0),
(61, '2010-01-01', '2009-12-01', 0),
(62, '2015-09-27', '2015-08-23', 0),
(63, '2016-05-08', '2015-12-20', 0),
(64, '2016-01-02', '2015-09-19', 0),
(65, '2016-04-05', '2016-02-28', 0),
(66, '2016-04-05', '2016-03-13', 0),
(67, '0000-00-00', '0000-00-00', 0),
(68, '2017-01-07', '2017-09-09', 0),
(69, '2014-04-06', '2014-01-26', 0),
(70, '2016-04-24', '0000-00-00', 0),
(71, '2017-02-27', '2016-12-22', 0),
(72, '2015-07-05', '2015-01-18', 0),
(73, '0000-00-00', '0000-00-00', 0),
(74, '2020-01-19', '2019-07-28', 0),
(75, '2015-04-20', '2013-03-15', 0),
(76, '2013-07-21', '2013-07-21', 0),
(77, '2021-04-25', '2021-04-25', 0),
(78, '2021-04-25', '2016-01-01', 0),
(79, '2011-05-08', '2011-04-09', 0),
(80, '1997-11-22', '1997-11-22', 0),
(81, '2016-12-16', '2016-10-02', 0),
(82, '0000-00-00', '0000-00-00', 0),
(83, '2021-01-17', '2019-11-30', 0),
(84, '0000-00-00', '2017-06-16', 0),
(85, '0000-00-00', '2017-06-16', 0),
(86, '2019-07-28', '2019-08-18', 0),
(87, '2021-01-17', '2021-01-17', 0),
(88, '0000-00-00', '2020-02-09', 0),
(89, '0000-00-00', '2017-05-16', 0),
(90, '0000-00-00', '2017-10-02', 0),
(91, '1997-01-09', '1992-04-02', 0),
(92, '2012-12-09', '2019-09-19', 0),
(93, '1998-08-08', '1998-03-22', 0),
(94, '2020-08-25', '2021-08-12', 0),
(95, '0000-00-00', '0000-00-00', 0),
(96, '1998-04-07', '1998-04-07', 0),
(97, '0000-00-00', '0000-00-00', 0),
(98, '2019-11-22', '2019-09-06', 0),
(99, '2019-05-27', '2019-04-03', 0),
(100, '2019-05-27', '2019-04-03', 0),
(101, '2016-12-18', '2016-07-16', 0),
(102, '1995-01-08', '1994-01-07', 0),
(103, '2003-01-05', '2003-01-02', 0),
(104, '2011-01-05', '2011-01-05', 0),
(105, '2011-04-18', '2011-04-24', 0),
(106, '0000-00-00', '0000-00-00', 0),
(107, '2021-12-01', '2019-11-30', 0),
(108, '1999-01-05', '1999-01-05', 0),
(109, '2019-09-23', '2018-09-13', 0),
(110, '0000-00-00', '2019-04-19', 0),
(111, '2019-01-27', '2018-10-28', 0),
(112, '0000-00-00', '0000-00-00', 0),
(113, '0000-00-00', '0000-00-00', 0),
(114, '0000-00-00', '0000-00-00', 0),
(115, '0000-00-00', '0000-00-00', 0),
(116, '0000-00-00', '0000-00-00', 0),
(117, '0000-00-00', '0000-00-00', 0),
(118, '2020-01-12', '2019-12-28', 0),
(119, '0000-00-00', '0000-00-00', 0),
(120, '2020-09-05', '2020-11-15', 0),
(121, '2020-09-05', '2020-11-15', 0),
(122, '2020-09-05', '2020-11-23', 0),
(123, '2020-09-05', '2020-06-06', 0),
(124, '2017-04-01', '2017-04-01', 0),
(125, '2017-04-01', '2017-04-01', 0),
(126, '2017-04-01', '2017-04-01', 0),
(127, '0000-00-00', '0000-00-00', 0),
(128, '2020-09-05', '0000-00-00', 0),
(129, '0000-00-00', '0000-00-00', 0),
(130, '0000-00-00', '0000-00-00', 0),
(131, '1992-08-26', '1993-03-28', 0),
(132, '1993-03-14', '1993-04-16', 0),
(133, '2016-12-27', '2016-12-22', 0),
(134, '2008-05-09', '2010-07-09', 0),
(135, '2020-10-11', '0000-00-00', 0),
(136, '2020-10-11', '0000-00-00', 0),
(137, '2010-07-11', '2010-07-11', 0),
(138, '2008-03-30', '2008-08-30', 0),
(139, '2013-03-11', '2013-04-08', 0),
(140, '2014-01-20', '2013-12-07', 0),
(141, '0000-00-00', '0000-00-00', 0),
(142, '2017-05-21', '2016-12-30', 0),
(143, '2013-04-30', '2013-04-29', 0),
(144, '2019-01-09', '2019-04-19', 0),
(145, '2019-03-06', '2019-08-04', 0),
(146, '0000-00-00', '0000-00-00', 0),
(147, '0000-00-00', '0000-00-00', 0),
(148, '2020-10-03', '0000-00-00', 0),
(149, '2018-01-01', '2018-01-01', 0),
(150, '2017-01-01', '2017-01-01', 0),
(151, '2013-07-21', '2013-03-17', 0),
(152, '2000-01-01', '2000-01-01', 0),
(153, '2021-01-24', '2021-01-24', 0),
(154, '0000-00-00', '0000-00-00', 0),
(155, '0000-00-00', '0000-00-00', 0),
(156, '0000-00-00', '0000-00-00', 0),
(157, '2000-10-30', '2000-11-05', 0),
(158, '2018-03-04', '2017-07-01', 0),
(159, '2004-01-01', '2004-01-01', 0),
(160, '2007-01-01', '2007-01-01', 0),
(161, '2016-01-01', '2015-12-26', 0),
(162, '0000-00-00', '0000-00-00', 0),
(163, '2016-01-01', '2016-01-01', 0),
(164, '0000-00-00', '0000-00-00', 0),
(165, '0000-00-00', '0000-00-00', 0),
(166, '1993-01-06', '1997-11-22', 0),
(167, '2006-03-01', '2006-02-06', 0),
(168, '0000-00-00', '0000-00-00', 0),
(169, '2006-04-09', '2006-03-26', 0),
(170, '2019-03-17', '2019-06-30', 0),
(171, '2020-10-04', '2020-10-25', 0),
(172, '2010-04-11', '2010-03-13', 0),
(173, '2020-10-04', '0000-00-00', 0),
(174, '2001-01-01', '2001-01-01', 0),
(175, '2016-01-01', '0000-00-00', 0),
(176, '2016-05-29', '2015-12-21', 0),
(177, '2017-04-02', '2016-02-28', 0),
(178, '2014-03-16', '2014-02-04', 0),
(179, '2014-08-03', '2014-06-22', 0),
(180, '2013-03-16', '2014-01-19', 0),
(181, '2014-10-19', '2015-08-09', 0),
(182, '2016-05-29', '2015-12-21', 0),
(183, '2020-10-24', '0000-00-00', 0),
(184, '2020-11-15', '2020-12-12', 0),
(185, '2020-12-20', '2020-12-20', 0),
(186, '2020-12-20', '0000-00-00', 0),
(187, '2021-01-10', '2021-01-17', 0),
(188, '0000-00-00', '0000-00-00', 0),
(189, '0000-00-00', '0000-00-00', 0),
(190, '1998-01-01', '1998-01-01', 0),
(191, '2017-04-06', '1998-03-01', 0),
(192, '2016-04-17', '2016-03-20', 0),
(193, '2014-10-05', '2013-12-01', 0),
(194, '1999-04-11', '1999-04-11', 0),
(195, '1999-04-11', '1999-04-11', 0),
(196, '2014-04-20', '2014-03-23', 0),
(197, '1997-01-01', '1997-01-01', 0),
(198, '2004-01-01', '2004-01-01', 0),
(199, '2013-04-07', '2012-04-14', 0),
(200, '2011-01-01', '2011-01-01', 0),
(201, '2000-06-01', '2000-06-01', 0),
(202, '2016-05-08', '2016-01-03', 0),
(203, '2015-07-07', '2015-02-01', 0),
(204, '1994-03-01', '1994-07-01', 0),
(205, '1994-07-01', '1998-04-01', 0),
(206, '1998-06-28', '1998-06-28', 0),
(207, '1998-06-28', '1998-06-28', 0),
(208, '2000-01-01', '2000-01-01', 0),
(209, '2020-07-03', '0000-00-00', 0),
(210, '2019-04-17', '2018-05-27', 0),
(211, '2017-08-02', '2017-05-28', 0),
(212, '2015-02-24', '2015-03-01', 0),
(213, '2014-03-30', '2014-01-26', 0),
(214, '0000-00-00', '0000-00-00', 0),
(215, '2014-11-23', '2015-01-11', 0),
(216, '2016-10-17', '2016-09-04', 0),
(217, '2017-05-14', '2015-03-22', 0),
(218, '2014-10-05', '2021-10-28', 0),
(219, '2017-02-24', '2016-12-23', 0),
(220, '2015-03-15', '2017-05-14', 0),
(221, '2015-11-15', '2015-08-16', 0),
(222, '2021-03-07', '2021-02-28', 0),
(223, '1991-10-05', '1991-11-17', 0),
(224, '2016-09-18', '2017-11-19', 0),
(225, '1998-10-04', '1998-11-08', 0),
(226, '1991-10-05', '1991-10-27', 0),
(227, '2000-07-09', '2000-07-02', 0),
(228, '1998-03-01', '1999-02-14', 0),
(229, '2004-05-15', '2005-02-13', 0),
(230, '2006-02-25', '2008-12-07', 0),
(231, '2019-08-21', '2017-04-02', 0),
(232, '2019-08-21', '0000-00-00', 0),
(233, '2021-02-21', '2021-02-21', 0),
(234, '2019-08-21', '0000-00-00', 0),
(235, '2019-08-21', '0000-00-00', 0),
(236, '2019-08-21', '0000-00-00', 0),
(237, '2019-08-21', '0000-00-00', 0),
(238, '2019-08-21', '0000-00-00', 0),
(239, '2019-08-21', '2020-10-04', 0),
(240, '2019-08-21', '0000-00-00', 0),
(241, '2019-08-21', '0000-00-00', 0),
(242, '2019-08-21', '0000-00-00', 0),
(243, '2019-08-21', '0000-00-00', 0),
(244, '2013-05-05', '2013-04-19', 0),
(245, '2013-05-05', '2013-03-10', 0),
(246, '2015-10-01', '2015-09-01', 0),
(247, '2013-05-05', '2013-01-27', 0),
(248, '2018-11-28', '2018-11-30', 0),
(249, '2018-12-27', '2018-12-27', 0),
(250, '2004-03-04', '2004-02-15', 0),
(251, '2015-06-21', '2015-04-12', 0),
(252, '2015-04-15', '2015-03-22', 0),
(253, '2014-07-07', '2013-11-24', 0),
(254, '0000-00-00', '0000-00-00', 0),
(255, '1997-06-01', '1997-06-01', 0),
(256, '1993-05-09', '1992-07-01', 0),
(257, '2009-09-28', '2009-07-11', 0),
(258, '2017-05-01', '2017-01-04', 0),
(259, '2020-03-01', '2019-12-29', 0),
(260, '2018-05-27', '2016-11-13', 0),
(261, '2015-11-22', '2015-02-12', 0),
(262, '0000-00-00', '0000-00-00', 0),
(263, '1989-12-01', '2017-08-01', 0),
(264, '2020-10-04', '2018-12-28', 0),
(265, '2020-10-04', '0000-00-00', 0),
(266, '1992-01-10', '1993-03-30', 0),
(267, '2013-03-30', '2012-08-24', 0),
(268, '2007-04-07', '2007-02-25', 0),
(269, '2007-03-01', '1990-12-31', 0),
(270, '2018-12-03', '2018-11-28', 0),
(271, '2013-12-23', '2013-12-02', 0),
(272, '2013-12-23', '2013-11-26', 0),
(273, '0000-00-00', '2020-11-14', 0),
(274, '2021-03-07', '2010-01-01', 0),
(275, '2021-03-28', '0000-00-00', 0),
(276, '0000-00-00', '0000-00-00', 0),
(277, '1991-03-01', '1992-02-01', 0),
(278, '2009-09-06', '2009-09-09', 0),
(279, '2009-04-14', '2009-04-11', 0),
(280, '2009-09-06', '2009-09-06', 0),
(281, '2009-09-06', '2009-09-06', 0),
(282, '1998-01-12', '2000-12-01', 0),
(283, '2000-01-04', '2000-05-01', 0),
(284, '2007-01-01', '2006-01-01', 0),
(285, '2013-01-01', '2012-11-01', 0),
(286, '1995-07-13', '1996-01-28', 0),
(287, '1988-03-15', '1990-06-10', 0),
(288, '2009-10-25', '2009-10-11', 0),
(289, '2013-01-11', '2013-11-01', 0),
(290, '2016-12-26', '2015-03-01', 0),
(291, '2010-04-12', '2009-11-01', 0),
(292, '2012-05-05', '2012-08-12', 0),
(293, '2010-04-04', '2010-04-04', 0),
(294, '2021-03-07', '2021-02-17', 0),
(295, '2021-03-07', '0000-00-00', 0),
(296, '2021-03-07', '0000-00-00', 0),
(297, '2021-03-07', '2021-03-07', 0),
(298, '2020-12-13', '2021-03-14', 0),
(299, '0000-00-00', '0000-00-00', 0),
(300, '2020-10-19', '0000-00-00', 0),
(301, '2020-10-19', '0000-00-00', 0),
(302, '0000-00-00', '0000-00-00', 0),
(303, '0000-00-00', '0000-00-00', 0),
(304, '0000-00-00', '0000-00-00', 0),
(305, '0000-00-00', '0000-00-00', 0),
(306, '2013-09-22', '2013-12-24', 0),
(307, '2009-02-01', '2009-01-25', 0),
(308, '2019-03-17', '2021-02-14', 0),
(309, '2007-01-01', '0000-00-00', 0),
(310, '2010-01-01', '2010-01-01', 0),
(311, '2011-01-01', '2012-01-01', 0),
(312, '2011-01-01', '2012-01-01', 0),
(313, '2009-12-26', '2009-12-25', 0),
(314, '2009-03-07', '2009-04-12', 0),
(315, '2009-12-26', '2009-12-25', 0),
(316, '0000-00-00', '2008-11-25', 0),
(317, '2020-01-12', '2020-12-28', 0),
(318, '2018-03-18', '2017-12-30', 0),
(319, '2020-03-12', '2019-09-01', 0),
(320, '0000-00-00', '0000-00-00', 0),
(321, '2016-01-10', '2021-10-11', 0),
(322, '2016-04-24', '2016-04-24', 0),
(323, '2020-12-20', '2020-12-20', 0),
(324, '2016-03-03', '2016-04-24', 0),
(325, '2020-10-10', '2021-11-15', 0),
(326, '2020-12-20', '0000-00-00', 0),
(327, '2017-06-12', '0000-00-00', 0),
(328, '2019-12-11', '2020-12-25', 0),
(329, '1986-05-08', '1994-04-15', 0),
(330, '1989-10-01', '1990-05-10', 0),
(331, '2010-11-21', '2011-04-21', 0),
(332, '2003-07-08', '2003-07-27', 0),
(333, '2007-12-24', '2004-05-16', 0),
(334, '2008-12-28', '2008-08-17', 0),
(335, '2009-12-27', '2009-12-25', 0),
(336, '2011-11-13', '2015-08-30', 0),
(337, '2011-11-13', '2011-05-01', 0),
(338, '2019-12-30', '2019-06-23', 0),
(339, '2000-10-22', '2000-12-25', 0),
(340, '1999-08-08', '1999-08-15', 0),
(341, '2019-12-30', '2019-11-01', 0),
(342, '2000-02-27', '2000-02-27', 0),
(343, '2011-05-01', '2011-05-01', 0),
(344, '2011-05-01', '2011-05-01', 0),
(345, '2017-11-01', '2015-10-01', 0),
(346, '2019-12-30', '2019-10-09', 0),
(347, '2000-02-27', '2000-02-27', 0),
(348, '2015-12-06', '2016-11-15', 0),
(349, '0000-00-00', '0000-00-00', 0),
(350, '0000-00-00', '0000-00-00', 0),
(351, '0000-00-00', '0000-00-00', 0),
(352, '2021-02-14', '0000-00-00', 0),
(353, '2016-03-04', '2016-03-01', 0),
(354, '0000-00-00', '0000-00-00', 0),
(355, '0000-00-00', '0000-00-00', 0),
(356, '0000-00-00', '0000-00-00', 0),
(357, '0000-00-00', '0000-00-00', 0),
(358, '0000-00-00', '0000-00-00', 0),
(359, '0000-00-00', '0000-00-00', 0),
(360, '0000-00-00', '0000-00-00', 0),
(361, '0000-00-00', '0000-00-00', 0),
(362, '0000-00-00', '0000-00-00', 0),
(363, '0000-00-00', '0000-00-00', 0),
(364, '0000-00-00', '0000-00-00', 0),
(365, '0000-00-00', '0000-00-00', 0),
(366, '0000-00-00', '2021-04-07', 0),
(367, '0000-00-00', '0000-00-00', 0),
(368, '0000-00-00', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `person_custom_master`
--

CREATE TABLE `person_custom_master` (
  `custom_Order` smallint(6) NOT NULL DEFAULT 0,
  `custom_Field` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `custom_Name` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `custom_Special` mediumint(8) UNSIGNED DEFAULT NULL,
  `custom_FieldSec` tinyint(4) NOT NULL,
  `type_ID` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person_custom_master`
--

INSERT INTO `person_custom_master` (`custom_Order`, `custom_Field`, `custom_Name`, `custom_Special`, `custom_FieldSec`, `type_ID`) VALUES
(2, 'c1', 'Baptism in Water', NULL, 1, 2),
(3, 'c2', 'Baptism in Spirit', NULL, 1, 2),
(1, 'c3', 'Status', 24, 1, 12);

-- --------------------------------------------------------

--
-- Table structure for table `person_per`
--

CREATE TABLE `person_per` (
  `per_ID` mediumint(9) UNSIGNED NOT NULL,
  `per_Title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_FirstName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_MiddleName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_LastName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Suffix` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Address1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Address2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_City` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_State` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Zip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_HomePhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_WorkPhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_CellPhone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_WorkEmail` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_BirthMonth` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `per_BirthDay` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `per_BirthYear` year(4) DEFAULT NULL,
  `per_MembershipDate` date DEFAULT NULL,
  `per_Gender` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `per_fmr_ID` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `per_cls_ID` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `per_fam_ID` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `per_Envelope` smallint(5) UNSIGNED DEFAULT NULL,
  `per_DateLastEdited` datetime DEFAULT NULL,
  `per_DateEntered` datetime NOT NULL,
  `per_EnteredBy` smallint(5) NOT NULL DEFAULT 0,
  `per_EditedBy` smallint(5) UNSIGNED DEFAULT 0,
  `per_FriendDate` date DEFAULT NULL,
  `per_Flags` mediumint(9) NOT NULL DEFAULT 0,
  `per_Facebook` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_Twitter` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `per_LinkedIn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `person_per`
--

INSERT INTO `person_per` (`per_ID`, `per_Title`, `per_FirstName`, `per_MiddleName`, `per_LastName`, `per_Suffix`, `per_Address1`, `per_Address2`, `per_City`, `per_State`, `per_Zip`, `per_Country`, `per_HomePhone`, `per_WorkPhone`, `per_CellPhone`, `per_Email`, `per_WorkEmail`, `per_BirthMonth`, `per_BirthDay`, `per_BirthYear`, `per_MembershipDate`, `per_Gender`, `per_fmr_ID`, `per_cls_ID`, `per_fam_ID`, `per_Envelope`, `per_DateLastEdited`, `per_DateEntered`, `per_EnteredBy`, `per_EditedBy`, `per_FriendDate`, `per_Flags`, `per_Facebook`, `per_Twitter`, `per_LinkedIn`) VALUES
(1, NULL, 'Church', NULL, 'Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, 0, NULL, '2021-11-15 16:02:47', '2021-11-15 16:02:47', 1, 0, NULL, 0, NULL, NULL, '2'),
(2, '', 'Mark James', 'Fajadan', 'Hernandez', '', '', '', 'Muntinlupa', '', '', 'Philippines', '', '', '', '', '', 8, 2, 1997, NULL, 1, 0, 0, 0, 0, NULL, '2021-11-18 20:02:51', 1, 0, '2021-11-18', 0, '', '', ''),
(3, '', 'Gwen', '', 'Realuyo', '', '', '', 'Muntinlupa', '', '', 'Philippines', '', '', '', '', '', 3, 29, 1999, NULL, 2, 0, 0, 0, 0, '2021-12-01 21:27:42', '2021-11-22 01:18:23', 1, 1, '2021-11-22', 0, '', '', ''),
(4, '', 'Paul David Isaiah', 'Uy', 'Sison', '', '', '', 'Manila', '', '', 'Philippines', '', '', '', '', '', 4, 5, 2016, '2020-11-05', 1, 0, 2, 0, 0, '2021-12-01 22:49:14', '0000-00-00 00:00:00', 1, 1, '2020-11-05', 0, '', '', ''),
(5, '', 'MARK LOUIS', 'VILLAPANDO', 'BIROT', '', 'B45 L13 UNIT B QUERCUS ST., SOUTH GREENHEIGHTS ', 'VILLAGE, PUTATAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9179618657', 'makoy11@gmail.com', '', 1, 11, 1982, '0000-00-00', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(6, '', 'JAMIE', 'BINIZA', 'BIROT', '', 'B45 L13 UNIT B QUERCUS ST., SOUTH GREENHEIGHTS ', 'VILLAGE, PUTATAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9164919997', 'jamie.biniza@gmail.com', '', 8, 15, 1992, '0000-00-00', 2, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(7, '', 'MARZILLUZ', 'AJAMI', 'VILLAFLOR', '', '101 ACERO ST., TUGATOG, MALABON CITY', '', 'MALABON', '', '', 'PHILIPPINES', '', '', '9167843527', 'maru.villaflor05@gmail.com', '', 5, 10, 1994, '0000-00-00', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(8, '', 'JOVY', '', 'SINGCOY', '', '16- B SAN VICENTE ST., BRGY SAN VICENTE', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9291281281', 'jovy_singocy29@gmail.com', '', 10, 29, 1993, '0000-00-00', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(9, '', 'FABIAN', 'T.', 'SANTOS', '', 'B33 L22 SOLDIERS HILLS VILLAGE MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9328767770', 'fabyan17@gmail.com', '', 1, 17, 1984, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(10, '', 'JOSSAN', 'D.', 'SANTOS', '', 'B33 L22 SOLDIERS HILLS VILLAGE MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9328767771', 'jossanah17@gmail.com', '', 2, 16, 1987, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(11, '', 'MARILYN', 'V.', 'REALUYO', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9064468467', 'bengvillegas22@gmail.com', '', 10, 22, 1970, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(12, '', 'GWEN', 'V.', 'REALUYO', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9053974436', 'gwenrealuyo29@gmail.com', '', 3, 29, 1999, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(13, '', 'JUAN CARLOS', 'V.', 'REALUYO', '', '81 NATIONAL ROAD PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9472145439', 'yuannn07@gmail.com', '', 12, 7, 2000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(14, '', 'AJI', 'V.', 'REALUYO', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9750024271', 'areluyo6@gmail.com', '', 10, 27, 2006, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(15, '', 'JANINE FEBIE', 'B.', 'REALUYO', '', '81 NATIONAL ROAD PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9051444563', 'realuyojaninefebie@gmail.com', '', 2, 10, 1991, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(16, '', 'TERESITA', 'B.', 'REALUYO', '', '81 NATIONAL ROAD PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9391669033', 'teresitarealuyo@gmail.com', '', 6, 13, 1957, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(17, '', 'MARK VINCENT', '', 'GALVAN', '', 'B33 L8 CENTRAL TAGUIG BICUTAN, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9983028451', 'markgalvan009@gmali.com', '', 7, 10, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(18, '', 'JOY MAE', 'B.', 'SATSATIN', '', 'BRGY. SAN CARLOS,PUROK 1 CENTRAL', 'BICUTAN TAGUIG CITY', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9396604690', 'joymaesatsatin10@gmail.com', '', 7, 20, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(19, '', 'JIA DENISE', 'C.', 'VINLUAN', '', '501 LUNA EXT.SAN ROQUES, SAN PEDRO LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9231581271', 'jiavinluan@gmail.com', '', 5, 21, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(20, '', 'HIROLLY', 'B.', 'MORALES', '', '327 LAGUERTA ST. SAN VICENTE, SAN PEDRO LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9519786709', 'bhoshirollimorales@gmail.com', '', 12, 24, 2000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(21, '', 'JAMES', 'G.', 'JAGAPE', '', 'B56 L4 ZONE 5 BRGY. UB SITIO PAG-ASA', 'SAN PEDRO, LAGUNA 4023', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9637632437', 'jamesjagape@gmail.com', '', 2, 24, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(22, '', 'AARON JUDE', 'C.', 'TANAEL', '', 'B10 L2 SOLDIERS HILLS VILLAGE MUNTINLUPA, CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '919019739', 'aarontanael@gmail.com', '', 9, 10, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(23, '', 'JULLIANE DIVINA', 'C.', 'VINLUAN', '', '501 LUNA EXT.SAN ROQUES, SAN PEDRO LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9128804663', 'vinluanjul@gmail.com', '', 2, 8, 2008, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(24, '', 'NICHOLAS', 'CANATOY', 'HERNANDEZ', '', '7031 A. BONIFACIO ST. SAN DIONISIO PARANAQUE CITY', '', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9567760163', 'thenicholashernandez@gmail.com', '', 9, 10, 1992, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(25, '', 'JOANNE', 'B.', 'SATSATIN', '', 'SAN CARLOS CABA, LA UNION', '', 'LA UNION', '', '', 'PHILIPPINES', '', '', '9955482195', 'joannesatatin143@gmail.com', '', 9, 27, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(26, '', 'KENSHIN', 'V.', 'TACLIBON', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9279738257', 'taclibonkenshin80@gmail.com', '', 2, 2, 2010, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(27, '', 'AWIT RYL', 'V.', 'TACLIBON', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9610630870', 'awitryl@gmail.com', '', 2, 18, 2006, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(28, '', 'ELLA MAE', 'V.', 'JAVIER', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9563443581', 'ellamaejavier04@gmail.com', '', 5, 4, 2008, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(29, '', 'DESERRE', 'C.', 'VINLUAN', '', '501 LUNA EXT.SAN ROQUES, SAN PEDRO LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9326735636', 'deserre.mia@gmail.com', '', 10, 19, 2003, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(30, '', 'SANBIEL FAITH', '', 'SANTOS', '', 'B33 L22 SOLDIERS HILLS VILLAGE MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9328767771', '', '', 4, 11, 2015, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(31, '', 'JEN MARIE', 'D.', 'LAGORRA', '', '128 MLQ ST NEW LOWER BICUTAN, TAGUIG CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9519152418', 'lagorraj@gmail.com', '', 1, 14, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(32, '', 'ANA MARIE', '', 'AGUSTIN', '', 'ISAIAH ST., SAMATIERRA COMPOUND', 'ALMAZA UNO, LAS PINAS CITY', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9469478570', 'anaagustin370@gmail.com', '', 3, 21, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(33, '', 'PAULENE', 'R.', 'BALISI', '', '3254 MAG-IMPOK ST., CAA BF INTERNATIONAL VILLAGE', 'LAS PINAS CITY', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9753841508', 'pulen.balisi@gmail.com', '', 8, 2, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(34, '', 'MARY JOY', '', 'BAYLON', '', 'B17 L4 DAHLIA ST., TS CRUZ SUBD, ALMANZA DOS', 'LAS PINAS CITY', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9218157431', 'majoybaylon05@gmail.com', '', 5, 7, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(35, '', 'NOEL', '', 'BUQUID', '', 'BLK 7 LOT 60 ST.JOSEPH HOMES CALAMBA LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9092640463', 'region5supervisor@gmail.com', '', 5, 7, 1976, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(36, '', 'TRICIA MAY', 'E.', 'CAGAOAN', '', 'ISAIAH ST., SAMATIERRA COMPOUND', 'ALMAZA UNO, LAS PINAS CITY', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9085230769', 'cagaoan.tme@pnu.edu.ph', '', 5, 21, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(37, '', 'ALYSSA MAURIDETH', '', 'CLEMENTE', '', 'B15 L16 DALMORE DRIVE GREENLANE SUBD', 'PAMPLONA 3 LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9175911575', 'alyssamsc04@gmail.com', '', 7, 28, 2004, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(38, '', 'JOY', '', 'CLEMENTE', '', 'B15 L16 DALMORE DRIVE GREENLANE SUBD', 'PAMPLONA 3 LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9175911575', 'jhoyclemente420@gmail.com', '', 4, 4, 1971, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(39, '', 'HELEN', '', 'CUSTODIO', '', '41 CONCORDIA, SIBUNAG, GUIMARAS', '', 'GUIMARAS', '', '', 'PHILIPPINES', '', '', '', '', '', 8, 13, 1979, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(40, '', 'KEITH NORLYN', '', 'CUSTODIO', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', '', '', 6, 26, 2004, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(41, '', 'PATRICK', '', 'CUSTODIO', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', '', '', 3, 18, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(42, '', 'MARITESS', '', 'FERNANDEZ', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9982106347', 'maritesscustodio@yahoo.com', '', 3, 1, 1976, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(43, '', 'MARK', '', 'FERNANDEZ', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9982558402', 'mark_18@yahoo.com', '', 3, 6, 1981, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(44, '', 'MARK ANTHONY', '', 'JUELAR', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9995527667', 'markanthonyjuelar@gmail.com', '', 1, 3, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(45, '', 'NONA', '', 'LANDAGAN', '', 'P1-2A MANLUNAS ST. BRGY. 183 VILLAMOR PASAY CITY', '', 'PASAY', '', '', 'PHILIPPINES', '', '', '9369256082', 'gandanonz@gmail.com', '', 4, 6, 1981, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(46, '', 'GAUDENCIO', '', 'LINGAMEN', 'III', '14C BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9258796403', 'gaudencio.lingamen111@gmail.com', '', 9, 19, 1984, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(47, '', 'GAUDENCIO', '', 'LINGAMEN', 'IV', '14C BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', '', '', 4, 12, 2014, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(48, '', 'MARINEL', '', 'LINGAMEN', '', '14C BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9396316895', 'marinel.lingamen@gmail.com', '', 4, 21, 1983, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(49, '', 'KATE MURIELLE', '', 'LOPEZ', '', 'BLK 3 LOT 1  CRIMSON ST.PH2 AUBURN PLACE', 'TALON UNO, LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9568810265', 'kateee.murielleee@gmail.com', '', 7, 6, 2002, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(50, '', 'MARILOU', '', 'LOPEZ', '', 'BLK 3 LOT 1  CRIMSON ST.PH2 AUBURN PLACE', 'TALON UNO, LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9157318388', 'lopezmalou1984@gmail.com', '', 9, 21, 1984, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(51, '', 'MARCUS ETHAN', '', 'LOPEZ', '', 'BLK 3 LOT 1  CRIMSON ST.PH2 AUBURN PLACE', 'TALON UNO, LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', 'lopez.marcus@ess.edu.ph', '', 9, 30, 2010, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(52, '', 'MARK ALVERT', '', 'LOPEZ', '', 'BLK 3 LOT 1  CRIMSON ST.PH2 AUBURN PLACE', 'TALON UNO, LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9167734875', 'alvertokram@gmail.com', '', 5, 7, 1976, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(53, '', 'MARCO EZEKIEL', '', 'LOPEZ', '', 'BLK 3 LOT 1  CRIMSON ST.PH2 AUBURN PLACE', 'TALON UNO, LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', '', '', 10, 4, 2018, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(54, '', 'ALFRED', '', 'MORCILLO', '', '658 MULLET COMPND, CUPANG, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9519080841', 'alfredmorcillo@gmail.com', '', 12, 22, 1977, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(55, '', 'JOY ANN', '', 'MODELO', '', '180 EBONY ST. SAMATA VILLAGE TALON V LAS PINAS', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9951036931', 'joyannemodelo1011@yahoo.com', '', 10, 11, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(56, '', 'PRAXEDES', '', 'OPEÑA', '', '14B BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', '', '', 7, 7, 1944, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(57, '', 'CORIE JOY', '', 'SOTOMIL', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9074039682', 'sotomilperi033185@gmail.com', '', 3, 31, 1985, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(58, '', 'JOJIT', '', 'VILLARDO', '', '14B BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9177411445', 'jojitvillardo.gc@gmail.com', '', 11, 2, 1975, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(59, '', 'JOJIT', '', 'VILLARDO', 'JR', '14B BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9452576849', 'jojitvillardo10@gmail.com', '', 3, 18, 2003, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(60, '', 'JOSHEL AIZE', '', 'VILLARDO', '', '14B BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9199604181', 'josh1014.jv@gmail.com', '', 10, 14, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(61, '', 'SHIELA', '', 'VILLARDO', '', '14B BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9979649539', 'villardoshy0609@gmail.com', '', 6, 9, 1976, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(62, '', 'ERJHON PASCUAL', '', 'DELVIAR', '', 'B34 M JOAQUIN COMPOUND PUROK 8', 'ALABANG, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9613197912', 'erjhondelviar6@gmail.com', '', 6, 12, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(63, '', 'JOLINA ARCIAGA', '', 'YMASA', '', 'E5 HARMONY VILLE VINALO ST CUPANG MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9954541524', 'aniloj.agaicra6@gmail.com', '', 6, 2, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(64, '', 'JEANINE', '', 'BALANE', '', '55 NARRA STREET INDANG CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9364658897', 'Balanej30@gmail.com', '', 11, 25, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(65, '', 'MAY', '', 'HALINA', '', 'VILLA MUNSOD 1 SAN JOAQUIN,PASIG CITY', '', 'PASIG', '', '', 'PHILIPPINES', '', '', '9213444162', 'halinamay7@gmail.com', '', 5, 6, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(66, '', 'KARYLL', '', 'HALINA', '', 'VILLA MUNSOD 1 SAN JOAQUIN,PASIG CITY', '', 'PASIG', '', '', 'PHILIPPINES', '', '', '9611842239', 'Halinakaryll80@gmail.com', '', 8, 23, 2003, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(67, '', 'JAZZER RINGO', 'MONES', 'FLORES', '', 'BLOCK 6 LOT 2 PARTRIDGE ST.CAMELLA HOME WOODHILLS', 'BARANGAY SAN ANTONIO, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9668624684', '', '', 10, 21, 1986, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(68, '', 'ALAIZA', '', 'FLORES', '', 'BLOCK 6 LOT 2 PARTRIDGE ST.CAMELLA HOME WOODHILLS', 'BARANGAY SAN ANTONIO, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9287628431', '', '', 11, 3, 1988, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(69, '', 'BERYL ANGEL', 'A.', 'MONTAÑEZ', '', 'BLK 9B LOT 4 BARRERA ST. LESSANDRA HEIGHTS', 'MOLINO IV, BACOOR, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '966826772', '', '', 11, 18, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(70, '', 'KHAEL', '', 'LUNA', '', 'BLOCK 4 LOT 32 FREEWILL SUBD MUNTINLUPA', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9194480355', '', '', 11, 20, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(71, '', 'LOVELY ROSE', '', 'PATUGA', '', '36 KATIHAN ST. POBLACION, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9356090383', 'patugalovelyrose@gmail.com', '', 10, 28, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(72, '', 'GRACE', '', 'RIOVEROS', '', 'P2 BLK3 PCS86 LOT43 SOUTHVILLE 3', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9756378323', 'Rioverosgrace01@gmail.com', '', 6, 19, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(73, '', 'JC', '', 'FERNANDEZ', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(74, '', 'ANGELA', '', 'CANOY', '', 'BLK 18 LOT 33 CELINA PLAINS SUBD PHASE 4', 'BRGY POOK STA ROSA, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '09224087455/ 09612652906', 'Yoongela05@gmail.com', '', 7, 5, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(75, '', 'DARRYL', '', 'GUTIERREZ', '', '191 BUENDIA ST. TUNASAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9755973290', 'gutierrezf247cs@gmail.com', '', 1, 11, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(76, '', 'ERICKA', '', 'VELASCO', '', 'MAKABUHAY ST. NBP RESERVATION', 'POBLACION MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9362887318', 'mangadaericka@gmail.com', '', 10, 21, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(77, '', 'JOHN LOUIE', 'D.', 'ESTOPA', '', '#22 ST. JAMES STREET TENSUAN SITE KATIHAN', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9488108500', 'johnlouis987654321@gmail.com', '', 5, 3, 1989, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(78, '', 'DIANE', 'L.', 'FLORES', '', '#22 ST. JAMES STREET TENSUAN SITE KATIHAN', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9261885758', 'floresdiane26@gmail.com', '', 4, 4, 1986, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(79, '', 'EMIL', '', 'PASCUAL', '', 'B 9 L 87 MARIGOLD. ST. PH3C GREENWOODS HGHTS', 'PALIPARAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9178046004', 'emilpascual88@yahoo.com', '', 12, 17, 1984, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(80, '', 'MARIFLOR', 'ANTONIO', 'PASCUAL', '', 'B 9 L 87 MARIGOLD. ST. PH3C GREENWOODS HGHTS', 'PALIPARAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9176724753', 'kelvipascual@gmail.com', '', 5, 21, 1979, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(81, '', 'ETHAN KOWJI', '', 'PASCUAL', '', 'B 9 L 87 MARIGOLD. ST. PH3C GREENWOODS HGHTS', 'PALIPARAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9165963382', 'ethankowjipascual@gmail.com', '', 10, 4, 2017, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(82, '', 'EUAN KENJI', '', 'PASCUAL', '', 'B 9 L 87 MARIGOLD. ST. PH3C GREENWOODS HGHTS', 'PALIPARAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 5, 28, 2013, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(83, '', 'LETICIA', 'CAGASCAS', 'PASCUAL', '', 'B 87 L 6 WALING WALING ST. BRGY. DATU ESMAEL', 'DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9672485825', '', '', 11, 15, 1952, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(84, '', 'EMILY', '', 'PASCUAL', '', 'B 87 L 6 WALING WALING ST. BRGY. DATU ESMAEL', 'DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9553027727', '', '', 4, 11, 1992, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(85, '', 'ELAINE', '', 'PASCUAL', '', 'B 87 L 6 WALING WALING ST. BRGY. DATU ESMAEL', 'DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9150551394', '', '', 2, 9, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(86, '', 'LEOLYN', 'B.', 'PAJANOSTAN', '', 'B 7 L30 LEGIAN PH B CARSADANG BAGO 1', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9267436147', '', '', 2, 17, 1992, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(87, '', 'KYLE', 'DARVINNE', 'PAJANOSTAN', '', 'B 7 L30 LEGIAN PH B CARSADANG BAGO 1', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 10, 26, 2013, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(88, '', 'LEA', 'ROSE', 'MERIOLES', '', 'B 7 L30 LEGIAN PH B CARSADANG BAGO 1', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9205891813', 'emilpascual88@yahoo.com', '', 10, 9, 1993, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(89, '', 'KRISTINE', '', 'CAGASCAS', '', 'B 7 L30 LEGIAN PH B CARSADANG BAGO 1', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9506568673', 'emilpascual88@yahoo.com', '', 9, 30, 1986, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(90, '', 'PRINCE LAWRENCE', '', 'CAGASCAS', '', 'B 7 L30 LEGIAN PH B CARSADANG BAGO 1', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', 'emilpascual88@yahoo.com', '', 12, 15, 2007, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(91, '', 'PIO', '', 'EVANGELISTA', '', 'B7 L7 YAKAL COR MOLAVE ST. PERPETUAL VILLAGE 6', 'MAMBOG 4, BACOOR CITY CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9171600410', 'sdevan19.24@gmail.com', '', 4, 10, 1960, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(92, '', 'LAWRENCE', '', 'ALCORDO', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9496083896', 'alcordo.grace8757@yahoo.com', '', 8, 9, 1965, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(93, '', 'GRACE', 'LARUGAL', 'ALCORDO', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9216302823', 'alcordo.grace8757@yahoo.com', '', 6, 6, 1974, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(94, '', 'NICOLE', 'JHAE', 'LARUGAL', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', 'alcordo.grace8757@yahoo.com', '', 9, 10, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(95, '', 'ILLUMINADA', '', 'LARUGAL', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9054381030', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(96, '', 'EULALIA', 'LARUGAL', 'RUIZ', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9054381030', 'alcordo.grace8757@yahoo.com', '', 10, 31, 1983, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(97, '', 'GABRIEL JHAE', '', 'RUIZ', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9054381030', '', '', 9, 15, 2016, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(98, '', 'MARK ANGELO', '', 'LAVA', '', 'B4 L3 WESTBAY HOMES, DAAN-BUKID,BACOOR CITY', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9061783930', 'lavamarkangelo@gmail.com', '', 3, 24, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(99, '', 'ZENAIDA', '', 'MALAYAO', '', '383 SAN NICOLAS III, BACOOR CITY CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '903102990', 'kleyrmalayao@gmail.com', '', 9, 29, 1955, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(100, '', 'CLAIRE ANN JOYCE', 'R.', 'MALAYAO', '', '383 SAN NICOLAS III, BACOOR CITY CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9752972703', 'kleyrmalayao@gmail.com', '', 10, 19, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(101, '', 'MAEGAN', 'G.', 'MALAYAO', '', '383 SAN NICOLAS III, BACOOR CITY CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9384380795', 'malayaomaegan05@gmail.com', '', 5, 5, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(102, '', 'APOLINARIO', '', 'SOMEROS', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9175563484', '238angelicasers@gmail.com', '', 9, 21, 1964, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(103, '', 'RIZA', '', 'SOMEROS', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9998813461', '238angelicasers@gmail.com', '', 10, 31, 1972, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(104, '', 'MARIA BERNADETTE', 'C.', 'SOMEROS', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9209573045', '238angelicasers@gmail.com', '', 5, 24, 2004, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(105, '', 'ANGEL', 'C.', 'SOMEROS', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9215987464', '238angelicasers@gmail.com', '', 10, 5, 2006, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(106, '', 'EVANGELINE', '', 'TECIO', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '238angelicasers@gmail.com', '', 4, 7, 1982, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(107, '', 'JULIE ANNE', '', 'MONTIALBUCIO', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9459644130', '238angelicasers@gmail.com', '', 7, 15, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(108, '', 'EUSEBIA JONALYN', '', 'CAUTON', '', 'B6 L3 YAKAL ST. PINEWOOD VILLAGE, BUCANDALA 4', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9453191617', 'joycauton18@gmail.com', '', 12, 18, 1972, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(109, '', 'PATRICK JOHN', 'C.', 'CAUTON', '', 'B6 L3 YAKAL ST. PINEWOOD VILLAGE, BUCANDALA 4', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9453191617', 'cauton90@gmail.com', '', 9, 23, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(110, '', 'MARK JOSHUA', '', 'CAUTON', '', 'B6 L3 YAKAL ST. PINEWOOD VILLAGE, BUCANDALA 4', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9453191617', 'Joshuacauton12@gmail.com', '', 12, 16, 2000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(111, '', 'KURT JUSTIN', '', 'CAUTON', '', 'B6 L3 YAKAL ST. PINEWOOD VILLAGE, BUCANDALA 4', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9453191617', 'kurtjustincauton13@gmail.com', '', 5, 31, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(112, '', 'JAMES ANDREW', '', 'CAUTON', '', 'B6 L3 YAKAL ST. PINEWOOD VILLAGE, BUCANDALA 4', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9453191617', 'Joycauton18@gmail.com', '', 12, 10, 2009, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(113, '', 'PAUL', '', 'CUSTODIO', '', 'ILOILO CITY', '', 'ILOILO', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(114, '', 'HAYDEE', '', 'VISAYA', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(115, '', 'ZENAIDA', '', 'VISAYA', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(116, '', 'HANNAH FRANCHESKA', '', 'BANDOJO', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(117, '', 'ISMAEL', '', 'SAGAYAP', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(118, '', 'ROSE ANN', '', 'CAUSAREN', '', 'SITIO GUBAT, MAITIM 1ST, AMADEO CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9177458112', '', '', 8, 16, 1988, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(119, '', 'JERRY', '', 'CAUSAREN', '', 'SITIO GUBAT, MAITIM 1ST, AMADEO CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9156085283', '', '', 12, 9, 1987, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(120, '', 'CLARISSE ANNE', '', 'MALAYAO', '', '383 C SAN NICOLAS 3 BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 5, 25, 2004, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(121, '', 'JUVY', '', 'MALAYAO', '', '47 KATIHAN ST. POBLACION, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 7, 16, 2003, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(122, '', 'PRECIOUS MIKEE', '', 'MALAYAO', '', '47 KATIHAN ST. POBLACION, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 4, 20, 2007, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(123, '', 'LUISA JANE', '', 'MEDRIANO', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(124, '', 'ARGEL', '', 'HEK', '', 'APARTMENT 7 DOOR 3 JAYSONVILLE II', 'PUTATAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9362452545', 'argelalochek24@gmail.com', '', 1, 24, 1989, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(125, '', 'FERMARIE', 'JANINE', 'HEK', '', 'APARTMENT 7 DOOR 3 JAYSONVILLE II', 'PUTATAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9266484242', 'argelalochek24@gmail.com', '', 1, 28, 1990, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(126, '', 'ANDREA', 'JANINE', 'HEK', '', 'APARTMENT 7 DOOR 3 JAYSONVILLE II', 'PUTATAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9362452545', 'argelalochek24@gmail.com', '', 4, 13, 2010, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(127, '', 'FHREDIE', 'RICK', 'BANDOJO', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(128, '', 'CRISTINA ALAIZA', '', 'MALAYAO', '', '383 C. SAN NICOLAS 3 BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 12, 30, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(129, '', 'MERCEDES', '', 'LAVA', '', 'BLK 4 LOT 3 DINAR ST. WESTBAY HOMES', 'DAANG-BUKID, BACOOR, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 3, 17, 1970, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(130, '', 'EDGARDO', '', 'LAVA', '', 'BLK 4 LOT 3 DINAR ST. WESTBAY HOMES', 'DAANG-BUKID, BACOOR, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 11, 22, 1961, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(131, '', 'DARWIN', '', 'BIROT', '', 'B15 L25 UNITE SHV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9179608682', 'wintot.darbo@gmail.com', '', 12, 27, 1979, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(132, '', 'CATHERINE', '', 'BIROT', '', 'B15 L25 UNITE SHV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9212355578', 'wincat_04@yahoo.com', '', 9, 4, 1978, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(133, '', 'CATERINA JIREH', '', 'BIROT', '', 'B15 L25 UNITE SHV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 2, 4, 2010, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(134, '', 'NELZON', '', 'DELOS REYES', '', 'UMALI COMPOUND SUMMITVILLE', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9158287204', 'nelzondelosreyes.ndr@gmail.com', '', 3, 16, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(135, '', 'HAZEL', '', 'GUERRERO', '', 'UMALI COMPOUND SUMMITVILLE', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9158287205', 'cjjccute@gmail.com', '', 6, 10, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(136, '', 'JAN CHLOE', '', 'GUERRERO', '', 'UMALI COMPOUND SUMMITVILLE', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9955077408', 'janclhoie.gmail.com', '', 1, 18, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(137, '', 'MICHELLE', 'TIAMSING', 'SHROCK', '', 'BLK 30 LOT 3 PROVERBS STREET TEOSEJO SUBDIVISION', 'TUNASAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9278119240', '', '', 11, 20, 1979, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(138, '', 'MARLA', 'TIAMSING', 'MOLAVIZAR', '', 'BLK 30 LOT 3 PROVERBS STREET TEOSEJO SUBDIVISION', 'TUNASAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9171266966', '', '', 10, 22, 1989, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(139, '', 'CHERRY MAE', '', 'NAZARENO', '', 'CROSLEY LANE LIBERTY HOMES CUPANG MUNTINLUOA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9234227801', '', '', 3, 3, 1990, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(140, '', 'LAILANE', '', 'HIPONIA', '', 'PHASE 1 RIVERA HEIGHTS BLK 5 LOT 43', 'BRGY STA ROSA LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9564751582', '', '', 10, 20, 1991, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(141, '', 'ANA', '', 'TANAEL', '', 'BLK 30 LOT 2 SOLDIERS HILLS VILLAGE', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9178679068', '', '', 12, 3, 1969, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(142, '', 'MHERVIN', '', 'ALIVEN', '', 'BENDIX LANE LIBERTY HOMES CUPANG MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9307686221', '', '', 10, 26, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(143, '', 'JINKY', '', 'ROMERO', '', 'CROSLEY LANE LIBERTY HOMES CUPANG MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9266173699', '', '', 9, 4, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(144, '', 'ERIN', '', 'TANAEL', '', 'BLK 30 LOT 2 SOLDIERS HILLS VILLAGE', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9564002503', '', '', 3, 4, 2004, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(145, '', 'JENELYN', '', 'MANDAL', '', 'PHASE 1 RIVERA HEIGHTS BLK 5 LOT 43', 'BRGY STA ROSA, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '92688521131', '', '', 2, 3, 1989, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(146, '', 'JULLIAH MARIE', '', 'ESCARO', '', 'ILOILO CITY', '', 'ILOILO', '', '', 'PHILIPPINES', '', '', '9386778770', '', '', 8, 6, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(147, '', 'JEAN MEAGAN', '', 'ESCARO', '', 'ILOILO CITY', '', 'ILOILO', '', '', 'PHILIPPINES', '', '', '9386778770', '', '', 10, 15, 2006, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(148, '', 'LOWI JUNE', '', 'ROMERO', '', 'CROSLEY LANE LIBERTY HOMES CUPANG MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9266173699', '', '', 6, 29, 2000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(149, '', 'MILAN', '', 'TIAMSING', '', 'CROSLEY LANE LIBERTY HOMES CUPANG MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9234227801', '', '', 10, 26, 1981, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(150, '', 'MARILOU', '', 'TIAMSING', '', 'HINIGARAN NEGROS OCCIDENTAL', '', 'NEGROS', '', '', 'PHILIPPINES', '', '', '9972213887', '', '', 9, 23, 1959, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(151, '', 'MARK', '', 'ZAPANTA', '', 'SAN PEDRO LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9150995922', '', '', 11, 16, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(152, '', 'RACY JOY', 'F.', 'ASIS', '', 'BLOCK 8 LOT 47 LOVEBIRDS ST. CAMELLA HOMES', 'WOODHILLS, SAN PEDRO LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9989275543', 'racy.asis@gmail.com', '', 12, 20, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(153, '', 'LANCE JORDAN', 'O.', 'ASIS', '', 'BLOCK 8 LOT 47 LOVEBIRDS ST. CAMELLA HOMES', 'WOODHILLS, SAN PEDRO LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9754790584', 'mg.lance.asis@gmail.com', '', 11, 6, 1959, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(154, '', 'BRENDALYN', 'A.', 'PEREZ', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9612887015', 'bengadornado@gmail.com', '', 5, 25, 1957, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(155, '', 'EVELYN', '', 'ADORNADO', '', 'INFANTA QUEZON', '', 'QUEZON PROVINCE', '', '', 'PHILIPPINES', '', '', '9070897713', 'n.a', '', 10, 5, 1976, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(156, '', 'EDWIN', '', 'ADORNADO', '', 'INFANTA QUEZON', '', 'QUEZON PROVINCE', '', '', 'PHILIPPINES', '', '', '9070897713', 'n.a', '', 11, 17, 2009, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(157, '', 'MELANI', '', 'LABALAN', '', 'ADDRESS: B1 L2 ST JOSEPH RICHFIELD SUBD ', 'TAGAPO STA ROSA LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9199214950', 'n.a', '', 10, 13, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(158, '', 'SHANNIEL EINREB', 'T.', 'SIMPRON', '', 'ADDRESS: B1 L2 ST JOSEPH RICHFIELD SUBD ', 'TAGAPO STA ROSA LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9239902278', 'n.a', '', 12, 20, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(159, '', 'ALVINO', 'C.', 'CUARTERO', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9666987262', 'n.a', '', 11, 16, 1984, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(160, '', 'GLENDA', 'A.', 'CUARTERO', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9958168177', 'glendamacuartero@gmail.com', '', 11, 17, 1991, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(161, '', 'JEFFERSON', '', 'TOLENTINO', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9070897715', 'n.a', '', 10, 18, 2006, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(162, '', 'JEAN ALEXA', '', 'TOLENTINO', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9156480666', 'n.a', '', 12, 5, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(163, '', 'JENINE', '', 'ADORNADO', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9208151980', 'n.a', '', 8, 10, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(164, '', 'KYLE', 'RANIOLA', 'VELA', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', 'n.a', '', 12, 5, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(165, '', 'JUSTIN', '', 'SIA', '', 'A AND A APARTMENT UNIT J MANGGAHAN SOLDIER', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9517370952', '', '', 8, 1, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(166, '', 'PAULO', 'C.', 'SISON', '', '10FLOOR 8ADRIATICO BLDG. PADRE FAURA COR.', 'BOCOBO ST., ERMITA MANILA', 'MANILA', '', '', 'PHILIPPINES', '', '', '9088118450', 'psison2000@yahoo.com', '', 11, 25, 1979, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(167, '', 'MA. CRISELDA', 'U.', 'SISON', '', '10FLOOR 8ADRIATICO BLDG. PADRE FAURA COR.', 'BOCOBO ST., ERMITA MANILA', 'MANILA', '', '', 'PHILIPPINES', '', '', '9088112407', 'irishuysison@gmail.com', '', 7, 31, 1981, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', '');
INSERT INTO `person_per` (`per_ID`, `per_Title`, `per_FirstName`, `per_MiddleName`, `per_LastName`, `per_Suffix`, `per_Address1`, `per_Address2`, `per_City`, `per_State`, `per_Zip`, `per_Country`, `per_HomePhone`, `per_WorkPhone`, `per_CellPhone`, `per_Email`, `per_WorkEmail`, `per_BirthMonth`, `per_BirthDay`, `per_BirthYear`, `per_MembershipDate`, `per_Gender`, `per_fmr_ID`, `per_cls_ID`, `per_fam_ID`, `per_Envelope`, `per_DateLastEdited`, `per_DateEntered`, `per_EnteredBy`, `per_EditedBy`, `per_FriendDate`, `per_Flags`, `per_Facebook`, `per_Twitter`, `per_LinkedIn`) VALUES
(168, '', 'PAUL DAVID', 'U.', 'SISON', '', '10FLOOR 8ADRIATICO BLDG. PADRE FAURA COR.', 'BOCOBO ST., ERMITA MANILA', 'MANILA', '', '', 'PHILIPPINES', '', '', '', '', '', 4, 5, 2016, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(169, '', 'ARMELE', '', 'LEE', '', '1101 TOMAS MAPUA ST., STA CRUZ. MANILA', '', 'MANILA', '', '', 'PHILIPPINES', '', '', '9171503686', '', '', 2, 8, 1989, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(170, '', 'JONATHAN', '', 'LEE', '', '1101 TOMAS MAPUA ST., STA CRUZ. MANILA', '', 'MANILA', '', '', 'PHILIPPINES', '', '', '9166467929', '', '', 10, 18, 1985, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(171, '', 'ALSH', '', 'LEE', '', '1101 TOMAS MAPUA ST., STA CRUZ. MANILA', '', 'MANILA', '', '', 'PHILIPPINES', '', '', '9166467929', '', '', 3, 10, 2011, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(172, '', 'ARLENE', 'CASTRO', 'SUNGA', '', ' B25 L17 CHRYSANTHEMUM VIL, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9176593636', '', '', 6, 9, 1992, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(173, '', 'MA. ELENITA', '', 'CASTRO', 'JR', ' B25 L17 CHRYSANTHEMUM VIL, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9153641047', '', '', 6, 10, 1968, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(174, '', 'MARLOWE', '', 'PAMINTUAN', '', 'MARIKINA', '', 'MARIKINA', '', '', 'PHILIPPINES', '', '', '', '', '', 4, 1, 1993, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(175, '', 'ROMMEL', '', 'PAMINTUAN', '', 'MARIKINA', '', 'MARIKINA', '', '', 'PHILIPPINES', '', '', '9171124962', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(176, '', 'ROSEMEL', '', 'MISSION', '', '137C SAN DIEGO STREET BGY 88 EAST GRACE PARK', 'CALOOCAN CITY 1400', 'CALOOCAN', '', '', 'PHILIPPINES', '', '', '9052107354', '', '', 1, 3, 1979, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(177, '', 'AVEGEAL', '', 'SENERES', '', 'BLOCK 14 LOT 122 PHASE 3 E1 BRGY. LONGOS ', 'MALABON CITY', 'MALABON', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(178, '', 'PIOUS', '', 'SENERES', '', 'BLOCK 14 LOT 122 PHASE 3 E1 BRGY. LONGOS ', 'MALABON CITY', 'MALABON', '', '', 'PHILIPPINES', '', '', '9278717723', '', '', 11, 21, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(179, '', 'DOMINGO ALLAN', '', 'SAMUS', '', 'BLK 1 LOT 7 PAXTON STREET EAST FAIRVIEW', 'QUEZON CITY', 'QUEZON', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(180, '', 'JERAMIE', '', 'TUGAY', '', 'FAIRVIEW, QUEZON CITY', '', 'QUEZON', '', '', 'PHILIPPINES', '', '', '9161223970', '', '', 5, 29, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(181, '', 'MARY ALBERT', 'JOSE', 'GELACIO', '', 'FAIRVIEW, QUEZON CITY', '', 'QUEZON', '', '', 'PHILIPPINES', '', '', '9357193970', '', '', 10, 19, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(182, '', 'NENITA', '', 'JOSE', '', 'FAIRVIEW, QUEZON CITY', '', 'QUEZON', '', '', 'PHILIPPINES', '', '', '9532907049', '', '', 10, 19, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(183, '', 'REGINA', '', 'BENIPAYO', '', 'MOONWALK, PARANAQUE', '', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9164882819', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(184, '', 'KEANNA', '', 'SAAVEDRA', '', 'TAGAYTAY', '', 'TAGAYTAY', '', '', 'PHILIPPINES', '', '', '9616307427', '', '', 1, 10, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(185, '', 'KAEZLE', '', 'VINA', '', 'PHASE 3 BLOCK 42 LOT 14 GREEN ESTATE MALAGASANG', 'IMUS CAVITE ', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9279421447', '', '', 8, 19, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(186, '', 'EVE', '', 'VILLANUEVA', '', '400 MERCEDES ST. BUNLO, BOCAUE, BULACAN', '', 'BULACAN', '', '', 'PHILIPPINES', '', '', '', '', '', 9, 19, 1988, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(187, '', 'ISAMU', '', 'KATO', '', '#9 REAL STREET METROPOLIS 1', 'BRGY. STA LUCIA, PASIG CITY, 1608', 'PASIG', '', '', 'PHILIPPINES', '', '', '9171450411', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(188, '', 'SAHLEE', '', 'CHUA', '', 'B4 L16 GEN. SANTOS ST., SOUTH CITY HOMES', 'BINAN CITY, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9083965232', '', '', 7, 11, 1965, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(189, '', 'EVELYN', '', 'MAGNO', '', 'SAN FRANCISCO, USA', '', 'SAN FRANCISCO', '', '', 'USA', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(190, '', 'AIZELLE ROSE', 'LAUREANO', 'BERNARDO', '', 'BLK 29 LOT 5 LINNET ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9353101617', 'aizellelaureano06@gmail.com', '', 11, 6, 1991, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(191, '', 'EDSON MICKEL', '', 'BERNARDO', '', 'BLK 29 LOT 5 LINNET ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9175119106', 'kana.caleb@gmail.com', '', 9, 2, 1987, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(192, '', 'CHRISTINE ANNE', '', 'CABANTOG', '', 'BLK 29 LOT 30 LANDCO VILLAGE BRGY MAGSAYSAY', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9452377129', 'christineannecabantog@gmail.com', '', 8, 21, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(193, '', 'CLAUSELYN', '', 'CUASAY', '', 'BLK 1 LOT 6 CONSOLE 12 ELVINDA BRGY SAN VICENTE', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9669339345', 'claucuasay@yahoo.com', '', 10, 14, 1966, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(194, '', 'PIA VEREA', '', 'EVANGELISTA', '', 'BLK 7 LOT 7 MOLAVE ST CORNER YAKAL ST.', 'PERPETUAL VILLAGE 6, BRGY. MAMBOG, BACOOR, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9955672843', 'pia.vg.elista@gmail.com', '', 7, 5, 1990, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(195, '', 'STEPHANY', '', 'EVANGELISTA', '', 'BLK 7 LOT 7 MOLAVE ST CORNER YAKAL ST.', 'PERPETUAL VILLAGE 6 BRGY. MAMBOG, BACOOR, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9153270509', 'sdevan19.24@gmail.com', '', 8, 24, 1991, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(196, '', 'JAN KONRAD', '', 'SANTOS', '', '36 CATTLEYA ST. SAMPAGUITA VILLAGE', 'SAN PEDRO LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9565669808', 'konradviescasantos@gmail.com>', '', 1, 20, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(197, '', 'HELEN', '', 'LAUREANO', '', 'BLK 30 LOT 4 KESTREL ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9093806357', 'helenlaureano1210@gmail.com', '', 12, 10, 1965, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(198, '', 'JON MICHAEL', '', 'LAUREANO', '', 'BLK 30 LOT 4 KESTREL ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9772473137', 'jonmichaellaureano22@gmail.com', '', 7, 22, 1993, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(199, '', 'KLAUDINE ALLYANNA', '', 'LAUREANO', '', 'BLK 30 LOT 4 KESTREL ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9669339345', 'cuasayklaudine@gmail.com', '', 1, 25, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(200, '', 'JULIANNE ROSE', '', 'LAUREANO', '', 'BLK 30 LOT 4 KESTREL ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9067356073', 'Yanaaalaureano@gmail.com', '', 7, 24, 2004, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(201, '', 'JOVELYN', 'ESPAÑOL', 'NAVARRO', '', 'MALIPAY 3 MOLINO 4 BACOOR CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9984373773', 'jovelynnavarro5@gmail.com', '', 3, 14, 1989, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(202, '', 'MELANIE', '', 'NGITNGIT', '', '117 PHASE 2 SITIO BAYN BAYANAN BRGY. SAN VICENTE', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9190937255', 'melanie.gatessoftcorp@gmail.com', '', 3, 17, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(203, '', 'TIMOTHY', 'ISAIAH', 'PADERNAL', '', 'BLK 39 LOT 5 PLOVER ST. CAMELLA WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9152837332', 'timothypdnim@gmail.com', '', 12, 13, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(204, '', 'AMELIA', '', 'TUD', '', 'E. RODRIGUEZ AVE., SAN AGUSTIN VILLAGE', 'BRGY MOONWALK, PARANAQUE CITY', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9216140210', '321meams@gmail.com', '', 5, 12, 1967, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(205, '', 'JOSELITO', '', 'TUD', '', 'E. RODRIGUEZ AVE., SAN AGUSTIN VILLAGE', 'BRGY MOONWALK, PARANAQUE CITY', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9215928722', 'jtud18@gmail.com', '', 5, 30, 1964, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(206, '', 'KEVIN LLOYD', '', 'TUD', '', 'E. RODRIGUEZ AVE., SAN AGUSTIN VILLAGE', 'BRGY MOONWALK, PARANAQUE CITY', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9217613098', 'kevinlloydtud@gmail.com', '', 1, 6, 1991, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(207, '', 'KRISTINE LOUISE', '', 'TUD', '', 'E. RODRIGUEZ AVE., SAN AGUSTIN VILLAGE', 'BRGY MOONWALK, PARANAQUE CITY', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9369357896', 'kristinetud26@gmail.com', '', 12, 26, 1992, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(208, '', 'MARK', 'LUEN', 'FLORES', '', '254 JP RIZAL ST., SALA, CABUYAO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9081518649', 'markluen.flores@gmail.com', '', 8, 21, 1991, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(209, '', 'CZESCELINE', '', 'FLORES', '', '254 JP RIZAL ST., SALA, CABUYAO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9493167817', 'czes.lauguico@gmail.com', '', 10, 13, 1992, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(210, '', 'ANGELO', 'G.', 'LAGRIADA', '', 'BLOCK 5 LOT 3 MIMOSA ST. SJV10 PH3 BGRY.', 'LANGGAM SAN PEDRO LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9458468672', 'gelohuesca@gmail.com', '', 1, 19, 2001, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(211, '', 'ANNA VERONICA', 'S.', 'CHUA', '', 'MAJOR HOMES BRGY. SIRANGLUPA CALAMBA LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9151866183', 'imannachua@gmail.com', '', 10, 21, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(212, '', 'ARIEL', '', 'REMUGAT', '', 'BLK 4 LOT 9 BRGY, UNITED BAYANIHAN', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '63817009', 'arielremugat@gmail.com', '', 11, 9, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(213, '', 'BRYAN', 'V.', 'STA. ROSA', '', '7630 NARRA RD. BAYAN-BAYANAN, SAN VICENTE', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9154960359', 'bryanst.rose@gmail.com', '', 5, 30, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(214, '', 'MARK DANIEL', 'V.', 'REMUGAT', '', 'BLK 4 LOT 9 BRGY, UNITED BAYANIHAN', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9993269681', 'dremugat@gmail.com', '', 8, 12, 2000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(215, '', 'JAKE PATRICK', 'B.', 'IMPERIAL', '', 'BLK 7 LOT 13 VILLA PAZ VILLAGE, BRGY. LANDAYAN', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9063323371', 'jakeimperial28@gmail.com', '', 7, 28, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(216, '', 'JOHN AERON', '', 'BERROYA', '', '193 C SAN PABLO ST. CARMONA CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9498919765', 'aeronberroya@gmail.com', '', 11, 4, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(217, '', 'JULIE PEARL', '', 'DIAZ', '', '740 PUROK 4 BRGY. SINALHAN', 'SANTA ROSA LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9260978327', 'juliepearldiaz@gmail.com', '', 5, 19, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(218, '', 'KIM SHEENA', 'A.', 'CUASAY', '', 'BLK 1 LOT 6 BEL AIR ST. CONSOLE 12', 'BRGY. FATIMA, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9665801337', 'kimsheenac@gmail.com', '', 6, 8, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(219, '', 'MARK JAMES', 'F.', 'HERNANDEZ', '', 'PHASE 4, 082 SITIO B-BAYANAN, BRGY. SAN VICENTE', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9279596216', 'mjhernandez8297@gmail.com', '', 8, 2, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(220, '', 'ROXANNE', 'A.', 'RUFO', '', 'PH2 BLK3 LOT12 JACINTO ST OLIVAREZ HOMES', 'SOUTH BIÑAN LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9457526627', 'roxanneadevarufo@gmail.com', '', 5, 22, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(221, '', 'SOPHIA', '', 'PATUGA', '', 'PH 3 BLK 35 LOT 19 SOUTHVILLE 3', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9456513688', 'sophiapatuga@gmail.com', '', 8, 2, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(222, '', 'VIRGINIA MARI', 'E.', 'BATITIS', 'II', 'BLK 2 LOT 82 PHASE4B GOLDEN CITY', 'SANTA ROSA, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9278392214', 'vbatitis@gmail.com', '', 5, 4, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(223, '', 'ALMA', '', 'MAURO', '', '1281 AGUADAS COR SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9331232191', 'maalsudario@gmail.com', '', 4, 13, 1967, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(224, '', 'MANUEL', '', 'SUDARIO', '', '1281 AGUADAS COR SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9331232191', '', '', 3, 12, 1938, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(225, '', 'LOLITO', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9167541164', '', '', 5, 5, 1958, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(226, '', 'MA. NANCY', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9167541164', 'ma.nanz06@yahoo.com', '', 11, 6, 1964, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(227, '', 'MARC SYDNY', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9488228446', 'marcsydnymostacesa@gmail.com', '', 1, 27, 1987, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(228, '', 'HANNAH JEAN', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9773893923', 'hmostacesa90@gmail.com', '', 6, 22, 1990, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(229, '', 'JAHZEEL', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9771064096', 'mostacesajahzeel@gmail.com', '', 4, 6, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(230, '', 'JEMUEL', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9054173917', 'yexel341@yahoo.com', '', 9, 6, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(231, '', 'EMMANUEL JOHN', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9060843965', 'johnmostacesa234@gmail.com', '', 2, 12, 2006, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(232, '', 'FAUSTINO', '', 'DEL PILAR', '', '68 STO. NIÑO ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9157859987', '', '', 3, 6, 1943, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(233, '', 'DINAH', '', 'DEL PILAR', '', '68 STO. NIÑO ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9456209753', 'dinahsam02@gmail.com', '', 11, 14, 1983, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(234, '', 'DARYL', '', 'SEVILLA', '', 'STA. POTENCIANA ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9157859987', 'daryl.sevilla1676@gmail.com', '', 9, 16, 1976, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(235, '', 'PELMAR', '', 'ALDE', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9124073928', '', '', 3, 7, 2002, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(236, '', 'MA. PELMA', '', 'ALDE', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9091291068', '', '', 7, 13, 2003, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(237, '', 'LUZVIMINDA', '', 'GERILLA', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '', '', '', 5, 30, 1969, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(238, '', 'JAYSANN', '', 'GERNATO', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9487698150', '', '', 2, 11, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(239, '', 'JAY ANN', '', 'GERNATO', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9487698150', '', '', 1, 7, 2007, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(240, '', 'JESSICA', '', 'GERNATO', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '', '', '', 6, 18, 2009, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(241, '', 'ANGELITA', '', 'MAROTO', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '', '', '', 12, 25, 1954, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(242, '', 'ALDWIN', '', 'GALLAMOS', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '', '', '', 3, 29, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(243, '', 'CAIRA', '', 'CULAS', '', 'BRGY. TAMBUCO, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '', '', '', 4, 18, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(244, '', 'RITCH', '', 'DACULES', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9985393906', 'ritch_dacules@yahoo.com', '', 5, 23, 1985, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(245, '', 'HANNAH JANE', 'S.', 'DACULES', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9122157812', 'dacules18.hannah.jane@gmail.com', '', 10, 10, 1990, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(246, '', 'RHEALYN', 'S.', 'BASBANIO', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9480842626', 'rhealynbasbanio99@gmail.com', '', 4, 23, 1999, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(247, '', 'GERALDINE', 'D.', 'RIMANDO', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9557193270', 'geraldinerimando1989@yahoo.com', '', 1, 11, 1989, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(248, '', 'NORMAN', 'B.', 'RIMANDO', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9361641488', 'normanrimando1990@yahoo.com', '', 8, 9, 1990, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(249, '', 'ADIMAR', 'M.', 'OXENIOLA', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9985393906', 'adimaroxeniola@yahoo.com', '', 4, 8, 1987, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(250, '', 'KARL', 'LORENZ', 'TUD', '', '15039 B SAN MARTIN STREET, SAN AGUSTIN VILLAGE', 'MOONWALK PARANAQUE CITY', 'PARAÑAQUE', '', '', 'PHILIPPINES', '', '', '9217613063', 'karllorenztud@gmail.com', '', 9, 28, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(251, '', 'RIZA', '', 'PANGANIBAN', '', 'PUROK 1, BLOCK 1, DOLLETON STREET', 'BAYANAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9351940340', 'panganiban0077@gmail.com', '', 12, 30, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(252, '', 'VIC LORENZ', '', 'NUÑEZ', '', '247 SITIO SAN ANTONIO, POBLACION, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9759790039', 'vlnunezf247cs@gmail.com', '', 5, 24, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(253, '', 'ALEXANDER', '', 'TABLATE', '', '153 C KAPPIVILLE KATIHAN STREET', 'POBLACION MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9057256577', 'tablatealexandert@gmail.com', '', 5, 21, 1992, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(254, '', 'GABRIEL JAMES', '', 'MEJIA', '', 'BLOCK 7 LOT 3, PINAGPALA SAN GUILLERMO STREET', 'BAYANAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9495893166', 'mejia.gabrieljames@gmail.com', '', 10, 1, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(255, '', 'ROY', '', 'SANTILLAN', '', 'BLOCK 2 LOT 5, BERNOULLI STREET, AIRSPEED VILLE', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9566326332', 'cecilia.labrador.santillan@gmail.com', '', 8, 20, 1978, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(256, '', 'IKIT', '', 'SANTILLAN', '', 'BLOCK 2 LOT 5, BERNOULLI STREET, AIRSPEED VILLE', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9272345657', 'cecilia.labrador.santillan@gmail.com', '', 5, 10, 1984, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(257, '', 'VICTORIE MERL', '', 'CABERO', '', '88 KATIHAN ST., KAPPIVILLE, POBLACION', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9355059184', 'victoriecabero@gmail.com', '', 7, 22, 1991, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(258, '', 'PIA', 'MARCHESA', 'BUE', '', 'B3 L13 JAMCEL VILLA ARCIAGA, BAYANAN', 'MUNTINLUPA CITY ', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9552358561', 'p.marchesa06@gmail.com', '', 2, 6, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(259, '', 'LENIEL CHRISTIAN', '', 'DESUNIA', '', 'B3 L13 JAMCEL VILLA ARCIAGA, BAYANAN', 'MUNTINLUPA CITY ', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9127705303', 'lenieldesunia@gmail.com', '', 10, 10, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(260, '', 'RAYMARK', 'MARTINEZ', 'VOLANTE', '', 'UNITY COMPOUND ESTANISLAO RD PUTATAN MUNTINLUPA', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9457507219', '', '', 7, 7, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(261, '', 'APRIL JOY', '', 'BALANE', '', 'BLK 3 LOT 2 KATARUNGAN VILLAGE 2', 'POBLACION MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9661608100', 'balaneajoy143@gmail.com', '', 4, 27, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(262, '', 'MARY ROSE', '', 'FRANCISCO', '', 'SAMPAGUITA ROAD, POBLACION MUNTINLUPA', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9179924289', 'maryrose.francisco05@gmail.com', '', 11, 5, 1992, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(263, '', 'PRISCILLA', 'ORTINEZ', 'OPLEDA', '', 'QUEEN VEE ST. MANGGAHAN ITAAS', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9553419680', 'paulamaeopleda@gmail.com', '', 8, 27, 1980, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(264, '', 'PAULA MAE', 'ORTINEZ', 'OPLEDA', '', 'QUEEN VEE ST. MANGGAHAN ITAAS', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9050723488', 'paulamaeopleda@gmail.com', '', 6, 24, 2002, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(265, '', 'PRECIOUS ANN', 'ORTINEZ', 'OPLEDA', '', 'QUEEN VEE ST. MANGGAHAN ITAAS', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9553419680', 'paulamaeopleda@gmail.com', '', 5, 23, 2006, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(266, '', 'ELGENE', '', 'BAUTISTA', '', 'KINGFISHER ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9950213237', 'elgenebautista04@gmail.com', '', 10, 10, 1974, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(267, '', 'JERUM', '', 'OROBIA', '', 'HYACINTH ST. SGV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9637160788', 'jerumorobia@yahoo.com', '', 2, 8, 1988, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(268, '', 'ERICA', 'DEVORA', 'BAUTISTA', '', 'KINGFISHER ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9369392182', 'ericadevora14@gmail.com', '', 3, 30, 1990, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(269, '', 'EMMANUEL', '', 'DELA CRUZ', 'JR', 'KANLAON ST. COUNTRY HOMES, PUTATAN', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9278375599', 'bongmusic1220@gmail.com', '', 6, 8, 1979, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(270, '', 'JOAN TES', '', 'MANANSALA', '', 'CENTENNIAL TOWN HOMES, SAN ISISDRO, CABUYAO', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9478701831', 'jtmanansala55@gmail.com', '', 6, 1, 1992, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(271, '', 'JAYSON NIGEL', '', 'GALIT', '', 'KINGFISHER ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9976950014', 'jasonsarah012594@gmail.com', '', 1, 25, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(272, '', 'SARAH', '', 'GALIT', '', 'KINGFISHER ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9976950014', 'jasonsarah012594@gmail.com', '', 8, 1, 1994, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(273, '', 'MARY JANE', '', 'LAGARTO', '', 'HYACINTH ST. SGV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(274, '', 'EMILY', '', 'LUPANGCO', '', 'TIPAUREL NATIONAL RD. PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9157007551', '', '', 9, 5, 1973, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(275, '', 'ERJOHN', '', 'LAGARTO', '', 'HYACINTH ST. SGV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(276, '', 'ELVIRA', 'MONES', 'DELA CRUZ', '', 'SOLIVEN ST. SGV, PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9175812379', 'elviradlc21@gmail.com', '', 1, 31, 1961, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(277, '', 'JONATHAN', '', 'VIADO', '', 'B42 LOT 81 PHASE2 ALTATIERA HOMES BRY. OLAES', 'GMA, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9476159484', 'jntn020777@gmail.com', '', 2, 7, 1977, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(278, '', 'IREENE', '', 'VIADO', '', 'B42 LOT 81 PHASE2 ALTATIERA HOMES BRY. OLAES', 'GMA, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9476159484', 'ireeneviado777@gmail.com', '', 12, 8, 1985, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(279, '', 'IMEE', '', 'DE VENECIA', '', '985 MAGALLANES ST. BRGY. MADUYA, CARMONA, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9208320514', '', '', 12, 8, 1985, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(280, '', 'AREANE', '', 'ARELLANO', '', '3101 UNIT 6, SAN JOSE ST., BRGY. 3 CARMONA,CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9457482873', 'jamyrr052213@gmail.com', '', 5, 5, 1986, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(281, '', 'MICHEAL', 'MEDINA', 'ARELLANO', '', '3101 UNIT 6, SAN JOSE ST., BRGY. 3 CARMONA,CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9969095779', 'areane.mamauag@deped.gov.ph', '', 7, 17, 1986, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(282, '', 'JONNA', 'DAPITAN', 'SALUMBIDES', '', 'B13 L10 CONEFLOWER ST METROVILLE COMPLEX', 'HALANG BINAN, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9190843512', 'jonna.salumbides@gmail.com', '', 11, 29, 1985, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(283, '', 'MICHAEL', '', 'SALUMBIDES', '', 'B13 L10 CONEFLOWER ST METROVILLE COMPLEX', 'HALANG BINAN, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9190843513', 'salumbides.f247cs@gmail.com', '', 11, 7, 1986, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(284, '', 'ANGELICA', '', 'DAPITAN', '', 'PHASE 3 BLOCK 3 LOT 4 SOUTHVILLE 3 POBLACION', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9475350271', 'angelicadapitan1@gmail.com', '', 12, 22, 1999, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(285, '', 'KAREN', '', 'TAN', '', 'BLK 6 LOT 9 PH3 METROVILLE COMPLEX BIÑAN LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9190735938', 'karen.agapay.tan@gmail.com', '', 5, 9, 1980, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(286, '', 'LINDA', '', 'VALENTIN', '', 'BLK 17 LOT 37 PH4 LILY ST. BRGY. SAN ANTONIO', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9156248296', 'Kazma223130@gmail.com', '', 8, 30, 1972, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(287, '', 'REGGIE', '', 'VALENTIN', '', 'BLK 17 LOT 37 PH4 LILY ST. BRGY. SAN ANTONIO', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9156248296', 'Rcvalentin@gmail.com', '', 1, 31, 1979, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(288, '', 'KAZANDRA', 'MAPLE', 'VALENTIN', '', 'BLK 17 LOT 37 PH4 LILY ST. BRGY. SAN ANTONIO', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9156248296', 'zandravalentin22@gmail.com', '', 10, 22, 2003, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(289, '', 'REMEDIOS', '', 'MAMAUAG', '', 'BLK.5 LOT 64 CEDAR VILLAGE BRGY. LANTIC', 'CARMONA, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9061444430', '', '', 9, 26, 1962, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(290, '', 'AIDA', '', 'ALUMIA', '', '985 MAGALLANES ST. BRGY. MADUYA, CARMONA, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9208320514', '', '', 1, 28, 1959, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(291, '', 'JOYCIE', 'DE VERA', 'RAMOS', '', 'B10 L42 CEDAR1 LANTIC CARMONA, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9972134888', 'Joyciedv@gmail.com', '', 3, 26, 1991, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(292, '', 'DANTE', '', 'RAMOS', '', 'B10 L42 ROSAL ST CEDAR PH1 CARMONA CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9264418267', 'danteramos007@gmail.com', '', 9, 17, 1991, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(293, '', 'JANELO', '', 'DE VERA', '', 'B10 L42 ROSAL ST CEDAR PH1 CARMONA CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9398366934', 'nildevera2693@gmail.com', '', 4, 26, 1993, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(294, '', 'CAREN', '', 'DOLAR', '', 'B84 L21 PHASE4 MABUHAY CITY PALIPARAN 3', 'DASMARINAS CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9368537658', 'Carendolar03@gmail.com', '', 3, 21, 1984, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(295, '', 'MARCIAL', '', 'DOLAR', '', 'B84 L21 PHASE4 MABUHAY CITY PALIPARAN 3', 'DASMARINAS CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9268879175', 'Marcialdolar@gmail.com', '', 12, 7, 1983, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(296, '', 'MARC JEANELLE', '', 'DOLAR', '', 'B84 L21 PHASE4 MABUHAY CITY PALIPARAN 3', 'DASMARINAS CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9262880724', 'Marcjeanelledolar@gmail.com', '', 4, 18, 2003, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(297, '', 'CLARENCE JOYCE', '', 'DOLAR', '', 'B84 L21 PHASE4 MABUHAY CITY PALIPARAN 3', 'DASMARINAS CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', 'Clarencejoycedolar@gmail.com', '', 9, 14, 2010, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(298, '', 'JESUSA', '', 'VELASQUEZ', '', '008 IRENE MATIAS ST PILAR RD. ALMANZA UNO', 'LAS PINAS CITY', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9567805810', 'iamsvela@gmail.com', '', 1, 2, 1965, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(299, '', 'STEPHANIE QUEEN', '', 'DE VENECIA', '', '985 MAGALLANES ST. BRGY. MADUYA, CARMONA, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9208320514', '', '', 3, 14, 2015, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(300, '', 'KENT', '', 'VALENTIN', '', 'BLK 17 LOT 37 PH4 LILY ST. BRGY. SAN ANTONIO', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9156248296', 'Kazma223130@gmail.com', '', 8, 9, 2012, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(301, '', 'JIANNA KYLE', '', 'SALUMBIDES', '', 'B13 L10 CONEFLOWER ST METROVILLE COMPLEX', 'HALANG BINAN, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '', '', '', 3, 22, 2013, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(302, '', 'CHRISTIAN', '', 'GENOTIVA', '', 'MOLINO, BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(303, '', 'IRMA', '', 'GENOTIVA', '', 'MOLINO, BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(304, '', 'ASTERI', '', 'GENOTIVA', '', 'MOLINO, BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(305, '', 'BRANDALE', '', 'GENOTIVA', '', 'MOLINO, BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(306, '', 'CHRISTIAN', 'F.', 'EMAGUE', '', '472 M.L QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9479629579', 'chris.emague@gmail.com', '', 10, 26, 1985, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(307, '', 'IVORY', 'ANDALES', 'EMAGUE', '', '472 M.L QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9175522390', 'ivory.emague@gmail.com', '', 10, 22, 1986, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(308, '', 'EDWIN', 'S.', 'MEJIA', '', 'B7 LOT 3 SAMAHANG PINAGPALA, SAN GUILLERMO ST.', 'BAYANAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9503460244', '', '', 10, 14, 1970, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(309, '', 'ARLENE', 'A.', 'MEJIA', '', 'B7 LOT 3 SAMAHANG PINAGPALA, SAN GUILLERMO ST.', 'BAYANAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9630849615', '', '', 3, 15, 1975, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(310, '', 'CAMAY', 'A.', 'CREBELLO', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9109400161', 'mae100589@gmail.com', '', 10, 15, 1989, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(311, '', 'EDILBERTO', '', 'CREBELLO', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9122327093', '', '', 1, 22, 1981, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(312, '', 'FROILAN', '', 'ANDALES', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9501983439', '', '', 12, 21, 1956, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(313, '', 'ROSANA', '', 'ANDALES', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9109400161', '', '', 9, 2, 1958, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(314, '', 'IRIS', '', 'ANDALES', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9287185139', 'iris.andales@gmail.com', '', 3, 16, 1985, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(315, '', 'FRANCESCA NICOLE', '', 'ANDALES', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9218886499', 'nicoleandales1920@gmail.com', '', 10, 20, 2000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(316, '', 'LAWRENCE AGATHA', '', 'PAJENADO', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9284793006', 'laurenceagathap@gmail.com', '', 10, 30, 2003, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(317, '', 'MARK VINCENT', '', 'GUIS', '', '195 SAN GUILLERMO ST. PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9204998673', 'markygulp@gmail.com', '', 8, 19, 2000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(318, '', 'CEDRIC', '', 'CREBELLO', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9987635855', '', '', 8, 8, 2009, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(319, '', 'EMIL YUAN', '', 'CREBELLO', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9987635855', '', '', 8, 7, 2011, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(320, '', 'LILAC MADISON', 'A.', 'EMAGUE', '', '472 M.L QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 5, 5, 2017, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(321, '', 'JONATHAN', '', 'UNTALAN', '', 'CALAMBA LAGUNA , DONA FELIZA PACIANO RIZAL', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9067702921', '', '', 1, 7, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(322, '', 'MIKHAEL', '', 'LUNA', '', 'B 4 L32 FREEWILL SUBD. BRGY. PUTATAN MUNTINLUPA', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9994876718', '', '', 1, 26, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(323, '', 'KAEZLE', '', 'VIÑA', '', 'PHASE 3 BLOCK 42 LOT 14 GREEN ESTATE MALAGASANG', 'IMUS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9279421447', '', '', 8, 19, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(324, '', 'JUMAR', '', 'TOLEDO', '', '163 NATL RD. TUNASAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9568724738', '', '', 6, 29, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(325, '', 'ANGELICA', '', 'VELASCO', '', '12 F. CAPARAS STREET IVC MARIKINA CITY', '', 'MARIKINA', '', '', 'PHILIPPINES', '', '', '9562835330', '', '', 12, 20, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(326, '', 'MICHAEL', '', 'DACPANO', '', 'VALENZUELA', '', 'VALENZUELA', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 0000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(327, '', 'ROLANDO', '', 'UY', '', 'BGRY. LAIYA IBABAO SAN JUAN BATANGAS', '', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9178298339', '', '', 11, 20, 1956, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(328, '', 'EMILY', '', 'SANTOS', '', 'BGRY. LAIYA IBABAO SAN JUAN BATANGAS', '', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9454100384', '', '', 1, 9, 1967, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(329, '', 'EMETERIO', 'V.', 'ADLONG', '', '#36 SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9776427304', 'longads8@gmail.com', '', 3, 3, 1964, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(330, '', 'EMELITA', 'M.', 'ADLONG', '', '#36 SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9286809960', 'Emelitaadlong@gmail.com', '', 4, 17, 1958, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(331, '', 'RACQUEL', 'M.', 'ADLONG', '', '#36 SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9759357078', 'racq.adz@gmail.com', '', 3, 26, 2000, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(332, '', 'GARY', 'F.', 'MADRID', '', 'LOT 3 BLK 30B PH 4, DAHLIA ST., MT. CLAIRE VILL.', 'SANTA ANASTACIA, SANTO TOMAS CITY, BATANGAS', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9178398382', 'gary.madrid1127@gmail.com', '', 11, 27, 1971, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(333, '', 'DIVINE', 'Y.', 'MADRID', '', 'LOT 3 BLK 30B PH 4, DAHLIA ST., MT. CLAIRE VILL.', 'SANTA ANASTACIA, SANTO TOMAS CITY, BATANGAS', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9293494225', 'divinemadrid11@gmail.com', '', 7, 13, 1969, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', '');
INSERT INTO `person_per` (`per_ID`, `per_Title`, `per_FirstName`, `per_MiddleName`, `per_LastName`, `per_Suffix`, `per_Address1`, `per_Address2`, `per_City`, `per_State`, `per_Zip`, `per_Country`, `per_HomePhone`, `per_WorkPhone`, `per_CellPhone`, `per_Email`, `per_WorkEmail`, `per_BirthMonth`, `per_BirthDay`, `per_BirthYear`, `per_MembershipDate`, `per_Gender`, `per_fmr_ID`, `per_cls_ID`, `per_fam_ID`, `per_Envelope`, `per_DateLastEdited`, `per_DateEntered`, `per_EnteredBy`, `per_EditedBy`, `per_FriendDate`, `per_Flags`, `per_Facebook`, `per_Twitter`, `per_LinkedIn`) VALUES
(334, '', 'SHAILA GAIN', 'Y.', 'MADRID', '', 'LOT 3 BLK 30B PH 4, DAHLIA ST., MT. CLAIRE VILL.', 'SANTA ANASTACIA, SANTO TOMAS CITY, BATANGAS', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9153172191', 'madridshaila11@gmail.com', '', 10, 11, 1996, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(335, '', 'SETH TOBIAS', 'Y.', 'MADRID', '', 'LOT 3 BLK 30B PH 4, DAHLIA ST., MT. CLAIRE VILL.', 'SANTA ANASTACIA, SANTO TOMAS CITY, BATANGAS', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9293494225', 'madrid.toby@gmail.com', '', 2, 5, 2002, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(336, '', 'CLARITO', 'I.', 'ESTELEYDES', '', 'PHASE 1, B16, L11 SOUTHVILLE 3', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9164276811', 'Claritoesteleydes@gmail.com', '', 10, 11, 1977, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(337, '', 'EDWINA', 'M.', 'ESTELEYDES', '', 'PHASE 1, B16, L11 SOUTHVILLE 3', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9053320328', 'Wengmanaloesteleydes@gmail.com', '', 4, 23, 1975, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(338, '', 'CHLOE KATE', 'M.', 'ESTELEYDES', '', 'PHASE 1, B16, L11 SOUTHVILLE 3', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9053320328', 'Nengesteleydes@yahoo.com', '', 7, 26, 2012, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(339, '', 'REGIEMEL', 'M.', 'MORTIL', '', 'BLK 14 LOT 45 DAISY ST., SAINT JOSEPH VILLAGE 9', 'LANGGAM, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9185644802', 'rmm_arjhay@yahoo.com', '', 9, 29, 1984, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(340, '', 'PENNY', 'M.', 'GATMAITAN', '', 'BLK 14 LOT 45 DAISY ST., SAINT JOSEPH VILLAGE 9', 'LANGGAM, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9185644802', 'N/A', '', 2, 19, 1950, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(341, '', 'JOHN DAVID', 'I.', 'GATMAITAN', '', 'BLK 14 LOT 45 DAISY ST., SAINT JOSEPH VILLAGE 9', 'LANGGAM, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9089099179', 'N/A', '', 3, 8, 2007, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(342, '', 'EVELYN', 'E.', 'ZITA', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9998136374', 'evelyn_ronniezita@yahoo.com', '', 9, 21, 1975, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(343, '', 'RONALYN', 'E.', 'ZITA', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9013643473', 'preciousnicolefacto@gmail.com', '', 5, 16, 1997, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(344, '', 'EMIELYN', 'E.', 'ZITA', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '', 'zitaemielyn@gmail.com', '', 5, 28, 1999, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(345, '', 'DIVINA', '', 'ESPAÑOL', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9107861313', 'divineespanol08@gmail.com', '', 6, 28, 2002, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(346, '', 'DENNO', 'E.', 'ZITA', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9998136374', 'N/A', '', 5, 22, 2012, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(347, '', 'JOLIZA', 'B.', 'ESPAÑOL', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9952700456', 'Aziloje@gmail.com', '', 10, 1, 1990, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(348, '', 'GRETCHEN', '', 'BARTOLOME', '', 'PUROK 5 CALAGDAAN, CANTILAN, SURIGAO DEL SUR', '', 'SURIGAO DEL SUR', '', '', 'PHILIPPINES', '', '', '88 6909539550', 'bartolomeezekela@gmail.com', '', 4, 3, 1986, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(349, '', 'KAYLA MARIE', '', 'BARTOLOME', '', 'PUROK 5 CALAGDAAN, CANTILAN, SURIGAO DEL SUR', '', 'SURIGAO DEL SUR', '', '', 'PHILIPPINES', '', '', '9630670863', 'ezekelabartolome@gmail.com', '', 4, 11, 2008, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(350, '', 'REN ABRAHAM', '', 'BUNGAOS', '', 'PRK 5, BARANGAY CONEL,  GENERAL SANTOS CITY', '', 'GENERAL SANTOS', '', '', 'PHILIPPINES', '', '', '9512527111', 'renbungaos@gmail.com', '', 3, 1, 2018, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(351, '', 'REYNALDO', '', 'BUNGAOS', '', 'PRK 5, BARANGAY CONEL,  GENERAL SANTOS CITY', '', 'GENERAL SANTOS', '', '', 'PHILIPPINES', '', '', '9512527111', 'renbungaos@gmail.com', '', 7, 22, 1989, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(352, '', 'JHERBHEE', '', 'CABADING', '', 'UPPER BICUTAN, TAGUIG CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9453414830', 'Jhebcabading@gmail.com', '', 8, 13, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(353, '', 'EVON PEARL', '', 'CALIBAYAN', '', 'PUROK HILLSIDE, BRGY.LAKE LAHIT, LAKE SEBU', 'SOUTH COTABATO', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9516582264', 'cevonpearl@gmail.com', '', 3, 11, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(354, '', 'JEWEL ANGEL', 'GRACE', 'CALIBAYAN', '', 'PUROK HILLSIDE, BRGY.LAKE LAHIT, LAKE SEBU', 'SOUTH COTABATO', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9632331633', 'ezekelabartolome@gmail.com', '', 3, 17, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(355, '', 'CYD CLARENCE', '', 'DEBARBO', '', 'PUROK HILLSIDE, BRGY.LAKE LAHIT, LAKE SEBU', 'SOUTH COTABATO', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9632331635', 'cevonpearl@gmail.com', '', 10, 21, 2005, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(356, '', 'AKISHA MAY', '', 'LIBONA', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9084089476', 'cevonpearl@gmail.com', '', 6, 15, 2011, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(357, '', 'FRANZ HOMER', '', 'LIBONA', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9084089476', 'cevonpearl@gmail.com', '', 3, 23, 2009, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(358, '', 'HARLIE QUEEN', '', 'LIBONA', '', 'TALISAY, LAKE SEBU, SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9484649627', 'Queenying0817@gmail.com', '', 10, 17, 2010, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(359, '', 'MARICEL', '', 'LIBONA', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9084089476', 'cevonpearl@gmail.com', '', 2, 26, 1986, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(360, '', 'MELVIE', '', 'MANACOF', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9655447780', 'melviemanacof@gmailc.om', '', 12, 18, 1999, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(361, '', 'VERGIE', '', 'MANACOF', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9659773755', 'melviemanacof@gmailc.om', '', 8, 8, 1979, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(362, '', 'ANGEL', '', 'MANAFA', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9979557112', 'manafaangel@gmail.com', '', 7, 17, 2003, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(363, '', 'JHONNY', '', 'MANAFA', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9518038368', 'Johnnymanafa@gmail.com', '', 9, 10, 1987, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(364, '', 'FRITZIE LYNN', '', 'OFTANA', '', 'PRK 5, BARANGAY CONEL,  GENERAL SANTOS CITY', '', 'GENERAL SANTOS', '', '', 'PHILIPPINES', '', '', '9098872772', 'renbungaos@gmail.com', '', 4, 28, 1998, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(365, '', 'STEPHANIE NICOLE', '', 'OFTANA', '', 'PRK 5, BARANGAY CONEL,  GENERAL SANTOS CITY', '', 'GENERAL SANTOS', '', '', 'PHILIPPINES', '', '', '9098872772', 'renbungaos@gmail.com', '', 1, 31, 2015, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(366, '', 'JHASPHER', '', 'WALANG', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9635968414', 'Jaspherwalang@gmail.com', '', 4, 14, 1995, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(367, '', 'NHIPS', '', 'WALANG', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9635968414', 'phingkayw@gmail. com', '', 11, 27, 2002, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', ''),
(368, '', 'ISAIAS', '', 'DELA CRUZ', 'JR', 'MUNTINLUPA', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 7, 1, 1960, '0000-00-00', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1, '0000-00-00', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `person_permission`
--

CREATE TABLE `person_permission` (
  `per_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `person_per_no_teacher`
--

CREATE TABLE `person_per_no_teacher` (
  `COL 1` int(3) NOT NULL,
  `COL 2` varchar(10) DEFAULT NULL,
  `COL 3` varchar(17) DEFAULT NULL,
  `COL 4` varchar(10) DEFAULT NULL,
  `COL 5` varchar(12) DEFAULT NULL,
  `COL 6` varchar(3) DEFAULT NULL,
  `COL 7` varchar(49) DEFAULT NULL,
  `COL 8` varchar(49) DEFAULT NULL,
  `COL 9` varchar(15) DEFAULT NULL,
  `COL 10` varchar(10) DEFAULT NULL,
  `COL 11` varchar(10) DEFAULT NULL,
  `COL 12` varchar(11) DEFAULT NULL,
  `COL 13` varchar(10) DEFAULT NULL,
  `COL 14` varchar(10) DEFAULT NULL,
  `COL 15` varchar(24) DEFAULT NULL,
  `COL 16` varchar(36) DEFAULT NULL,
  `COL 17` varchar(10) DEFAULT NULL,
  `COL 18` int(2) DEFAULT NULL,
  `COL 19` int(2) DEFAULT NULL,
  `COL 20` int(4) DEFAULT NULL,
  `COL 21` varchar(10) DEFAULT NULL,
  `COL 22` varchar(1) DEFAULT NULL,
  `COL 23` varchar(1) DEFAULT NULL,
  `COL 24` varchar(1) DEFAULT NULL,
  `COL 25` varchar(1) DEFAULT NULL,
  `COL 26` varchar(1) DEFAULT NULL,
  `COL 27` varchar(16) DEFAULT NULL,
  `COL 28` varchar(16) DEFAULT NULL,
  `COL 29` int(1) DEFAULT NULL,
  `COL 30` int(1) DEFAULT NULL,
  `COL 31` varchar(10) DEFAULT NULL,
  `COL 32` varchar(1) DEFAULT NULL,
  `COL 33` varchar(10) DEFAULT NULL,
  `COL 34` varchar(10) DEFAULT NULL,
  `COL 35` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person_per_no_teacher`
--

INSERT INTO `person_per_no_teacher` (`COL 1`, `COL 2`, `COL 3`, `COL 4`, `COL 5`, `COL 6`, `COL 7`, `COL 8`, `COL 9`, `COL 10`, `COL 11`, `COL 12`, `COL 13`, `COL 14`, `COL 15`, `COL 16`, `COL 17`, `COL 18`, `COL 19`, `COL 20`, `COL 21`, `COL 22`, `COL 23`, `COL 24`, `COL 25`, `COL 26`, `COL 27`, `COL 28`, `COL 29`, `COL 30`, `COL 31`, `COL 32`, `COL 33`, `COL 34`, `COL 35`) VALUES
(1, '', 'Church', '', 'Admin', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '0', '0', '0', '0', '', '', '25/08/2004 18:00', 1, 0, '', '0', '', '', ''),
(2, '', 'Gwen', 'Villegas', 'Realuyo', '', '', '', '', '', '', 'Philippines', '', '', '', '', '', 3, 29, 1999, '', '2', '3', '6', '1', '0', '07/08/2021 21:51', '23/06/2021 20:18', 1, 1, '23/06/2021', '0', '', '', ''),
(3, '', 'Paul David Isaiah', 'Uy', 'Sison', '', '', '', 'Manila', '', '', 'Philippines', '', '', '', '', '', 4, 5, 2016, '', '1', '0', '5', '0', '0', '02/10/2021 21:54', '24/06/2021 23:09', 1, 1, '24/06/2021', '0', '', '', ''),
(4, '', 'Jonna', '', 'Salumbides', '', '', '', 'Muntinlupa', '', '', 'Philippines', '', '', '', '', '', 1, 1, 1980, '', '2', '0', '0', '0', '0', '', '11/10/2021 22:25', 1, 0, '11/10/2021', '0', '', '', ''),
(5, '', 'MARK LOUIS', 'VILLAPANDO', 'BIROT', '', 'B45 L13 UNIT B QUERCUS ST., SOUTH GREENHEIGHTS ', 'VILLAGE, PUTATAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9179618657', 'makoy11@gmail.com', '', 1, 11, 1982, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(6, '', 'JAMIE', 'BINIZA', 'BIROT', '', 'B45 L13 UNIT B QUERCUS ST., SOUTH GREENHEIGHTS ', 'VILLAGE, PUTATAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9164919997', 'jamie.biniza@gmail.com', '', 8, 15, 1992, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(7, '', 'MARZILLUZ', 'AJAMI', 'VILLAFLOR', '', '101 ACERO ST., TUGATOG, MALABON CITY', '', 'MALABON', '', '', 'PHILIPPINES', '', '', '9167843527', 'maru.villaflor05@gmail.com', '', 5, 10, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(8, '', 'JOVY', '', 'SINGCOY', '', '16- B SAN VICENTE ST., BRGY SAN VICENTE', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9291281281', 'jovy_singocy29@gmail.com', '', 10, 29, 1993, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(9, '', 'FABIAN', 'T.', 'SANTOS', '', 'B33 L22 SOLDIERS HILLS VILLAGE MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9328767770', 'fabyan17@gmail.com', '', 1, 17, 1984, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(10, '', 'JOSSAN', 'D.', 'SANTOS', '', 'B33 L22 SOLDIERS HILLS VILLAGE MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9328767771', 'jossanah17@gmail.com', '', 2, 16, 1987, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(11, '', 'MARILYN', 'V.', 'REALUYO', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9064468467', 'bengvillegas22@gmail.com', '', 10, 22, 1970, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(12, '', 'GWEN', 'V.', 'REALUYO', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9053974436', 'gwenrealuyo29@gmail.com', '', 3, 29, 1999, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(13, '', 'JUAN CARLOS', 'V.', 'REALUYO', '', '81 NATIONAL ROAD PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9472145439', 'yuannn07@gmail.com', '', 12, 7, 2000, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(14, '', 'AJI', 'V.', 'REALUYO', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9750024271', 'areluyo6@gmail.com', '', 10, 27, 2006, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(15, '', 'JANINE FEBIE', 'B.', 'REALUYO', '', '81 NATIONAL ROAD PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9051444563', 'realuyojaninefebie@gmail.com', '', 2, 10, 1991, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(16, '', 'TERESITA', 'B.', 'REALUYO', '', '81 NATIONAL ROAD PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9391669033', 'teresitarealuyo@gmail.com', '', 6, 13, 1957, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(17, '', 'MARK VINCENT', '', 'GALVAN', '', 'B33 L8 CENTRAL TAGUIG BICUTAN, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9983028451', 'markgalvan009@gmali.com', '', 7, 10, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(18, '', 'JOY MAE', 'B.', 'SATSATIN', '', 'BRGY. SAN CARLOS,PUROK 1 CENTRAL', 'BICUTAN TAGUIG CITY', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9396604690', 'joymaesatsatin10@gmail.com', '', 7, 20, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(19, '', 'JIA DENISE', 'C.', 'VINLUAN', '', '501 LUNA EXT.SAN ROQUES, SAN PEDRO LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9231581271', 'jiavinluan@gmail.com', '', 5, 21, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(20, '', 'HIROLLY', 'B.', 'MORALES', '', '327 LAGUERTA ST. SAN VICENTE, SAN PEDRO LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9519786709', 'bhoshirollimorales@gmail.com', '', 12, 24, 2000, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(21, '', 'JAMES', 'G.', 'JAGAPE', '', 'B56 L4 ZONE 5 BRGY. UB SITIO PAG-ASA', 'SAN PEDRO, LAGUNA 4023', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9637632437', 'jamesjagape@gmail.com', '', 2, 24, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(22, '', 'AARON JUDE', 'C.', 'TANAEL', '', 'B10 L2 SOLDIERS HILLS VILLAGE MUNTINLUPA, CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '919019739', 'aarontanael@gmail.com', '', 9, 10, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(23, '', 'JULLIANE DIVINA', 'C.', 'VINLUAN', '', '501 LUNA EXT.SAN ROQUES, SAN PEDRO LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9128804663', 'vinluanjul@gmail.com', '', 2, 8, 2008, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(24, '', 'NICHOLAS', 'CANATOY', 'HERNANDEZ', '', '7031 A. BONIFACIO ST. SAN DIONISIO PARANAQUE CITY', '', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9567760163', 'thenicholashernandez@gmail.com', '', 9, 10, 1992, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(25, '', 'JOANNE', 'B.', 'SATSATIN', '', 'SAN CARLOS CABA, LA UNION', '', 'LA UNION', '', '', 'PHILIPPINES', '', '', '9955482195', 'joannesatatin143@gmail.com', '', 9, 27, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(26, '', 'KENSHIN', 'V.', 'TACLIBON', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9279738257', 'taclibonkenshin80@gmail.com', '', 2, 2, 2010, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(27, '', 'AWIT RYL', 'V.', 'TACLIBON', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9610630870', 'awitryl@gmail.com', '', 2, 18, 2006, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(28, '', 'ELLA MAE', 'V.', 'JAVIER', '', '25 ROAD 1 NORTH DAANGHARI TAGUIG, CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9563443581', 'ellamaejavier04@gmail.com', '', 5, 4, 2008, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(29, '', 'DESERRE', 'C.', 'VINLUAN', '', '501 LUNA EXT.SAN ROQUES, SAN PEDRO LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9326735636', 'deserre.mia@gmail.com', '', 10, 19, 2003, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(30, '', 'SANBIEL FAITH', '', 'SANTOS', '', 'B33 L22 SOLDIERS HILLS VILLAGE MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9328767771', '', '', 4, 11, 2015, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(31, '', 'JEN MARIE', 'D.', 'LAGORRA', '', '128 MLQ ST NEW LOWER BICUTAN, TAGUIG CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9519152418', 'lagorraj@gmail.com', '', 1, 14, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(32, '', 'ANA MARIE', '', 'AGUSTIN', '', 'ISAIAH ST., SAMATIERRA COMPOUND', 'ALMAZA UNO, LAS PINAS CITY', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9469478570', 'anaagustin370@gmail.com', '', 3, 21, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(33, '', 'PAULENE', 'R.', 'BALISI', '', '3254 MAG-IMPOK ST., CAA BF INTERNATIONAL VILLAGE', 'LAS PINAS CITY', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9753841508', 'pulen.balisi@gmail.com', '', 8, 2, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(34, '', 'MARY JOY', '', 'BAYLON', '', 'B17 L4 DAHLIA ST., TS CRUZ SUBD, ALMANZA DOS', 'LAS PINAS CITY', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9218157431', 'majoybaylon05@gmail.com', '', 5, 7, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(35, '', 'NOEL', '', 'BUQUID', '', 'BLK 7 LOT 60 ST.JOSEPH HOMES CALAMBA LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9092640463', 'region5supervisor@gmail.com', '', 5, 7, 1976, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(36, '', 'TRICIA MAY', 'E.', 'CAGAOAN', '', 'ISAIAH ST., SAMATIERRA COMPOUND', 'ALMAZA UNO, LAS PINAS CITY', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9085230769', 'cagaoan.tme@pnu.edu.ph', '', 5, 21, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(37, '', 'ALYSSA MAURIDETH', '', 'CLEMENTE', '', 'B15 L16 DALMORE DRIVE GREENLANE SUBD', 'PAMPLONA 3 LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9175911575', 'alyssamsc04@gmail.com', '', 7, 28, 2004, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(38, '', 'JOY', '', 'CLEMENTE', '', 'B15 L16 DALMORE DRIVE GREENLANE SUBD', 'PAMPLONA 3 LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9175911575', 'jhoyclemente420@gmail.com', '', 4, 4, 1971, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(39, '', 'HELEN', '', 'CUSTODIO', '', '41 CONCORDIA, SIBUNAG, GUIMARAS', '', 'GUIMARAS', '', '', 'PHILIPPINES', '', '', '', '', '', 8, 13, 1979, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(40, '', 'KEITH NORLYN', '', 'CUSTODIO', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', '', '', 6, 26, 2004, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(41, '', 'PATRICK', '', 'CUSTODIO', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', '', '', 3, 18, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(42, '', 'MARITESS', '', 'FERNANDEZ', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9982106347', 'maritesscustodio@yahoo.com', '', 3, 1, 1976, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(43, '', 'MARK', '', 'FERNANDEZ', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9982558402', 'mark_18@yahoo.com', '', 3, 6, 1981, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(44, '', 'MARK ANTHONY', '', 'JUELAR', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9995527667', 'markanthonyjuelar@gmail.com', '', 1, 3, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(45, '', 'NONA', '', 'LANDAGAN', '', 'P1-2A MANLUNAS ST. BRGY. 183 VILLAMOR PASAY CITY', '', 'PASAY', '', '', 'PHILIPPINES', '', '', '9369256082', 'gandanonz@gmail.com', '', 4, 6, 1981, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(46, '', 'GAUDENCIO', '', 'LINGAMEN', 'III', '14C BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9258796403', 'gaudencio.lingamen111@gmail.com', '', 9, 19, 1984, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(47, '', 'GAUDENCIO', '', 'LINGAMEN', 'IV', '14C BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', '', '', 4, 12, 2014, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(48, '', 'MARINEL', '', 'LINGAMEN', '', '14C BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9396316895', 'marinel.lingamen@gmail.com', '', 4, 21, 1983, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(49, '', 'KATE MURIELLE', '', 'LOPEZ', '', 'BLK 3 LOT 1  CRIMSON ST.PH2 AUBURN PLACE', 'TALON UNO, LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9568810265', 'kateee.murielleee@gmail.com', '', 7, 6, 2002, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(50, '', 'MARILOU', '', 'LOPEZ', '', 'BLK 3 LOT 1  CRIMSON ST.PH2 AUBURN PLACE', 'TALON UNO, LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9157318388', 'lopezmalou1984@gmail.com', '', 9, 21, 1984, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(51, '', 'MARCUS ETHAN', '', 'LOPEZ', '', 'BLK 3 LOT 1  CRIMSON ST.PH2 AUBURN PLACE', 'TALON UNO, LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', 'lopez.marcus@ess.edu.ph', '', 9, 30, 2010, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(52, '', 'MARK ALVERT', '', 'LOPEZ', '', 'BLK 3 LOT 1  CRIMSON ST.PH2 AUBURN PLACE', 'TALON UNO, LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9167734875', 'alvertokram@gmail.com', '', 5, 7, 1976, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(53, '', 'MARCO EZEKIEL', '', 'LOPEZ', '', 'BLK 3 LOT 1  CRIMSON ST.PH2 AUBURN PLACE', 'TALON UNO, LAS PINAS', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', '', '', 10, 4, 2018, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(54, '', 'ALFRED', '', 'MORCILLO', '', '658 MULLET COMPND, CUPANG, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9519080841', 'alfredmorcillo@gmail.com', '', 12, 22, 1977, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(55, '', 'JOY ANN', '', 'MODELO', '', '180 EBONY ST. SAMATA VILLAGE TALON V LAS PINAS', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9951036931', 'joyannemodelo1011@yahoo.com', '', 10, 11, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(56, '', 'PRAXEDES', '', 'OPEÑA', '', '14B BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '', '', '', 7, 7, 1944, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(57, '', 'CORIE JOY', '', 'SOTOMIL', '', '7A CONTINENTAL ST.GREENVIEW VILLAGE.PAMPLONA 3', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9074039682', 'sotomilperi033185@gmail.com', '', 3, 31, 1985, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(58, '', 'JOJIT', '', 'VILLARDO', '', '14B BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9177411445', 'jojitvillardo.gc@gmail.com', '', 11, 2, 1975, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(59, '', 'JOJIT', '', 'VILLARDO', 'JR', '14B BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9452576849', 'jojitvillardo10@gmail.com', '', 3, 18, 2003, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(60, '', 'JOSHEL AIZE', '', 'VILLARDO', '', '14B BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9199604181', 'josh1014.jv@gmail.com', '', 10, 14, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(61, '', 'SHIELA', '', 'VILLARDO', '', '14B BELLET ST. GREENVIEW VILLAGE. PAMPLONA 3 LPC', '', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9979649539', 'villardoshy0609@gmail.com', '', 6, 9, 1976, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(62, '', 'ERJHON PASCUAL', '', 'DELVIAR', '', 'B34 M JOAQUIN COMPOUND PUROK 8', 'ALABANG, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9613197912', 'erjhondelviar6@gmail.com', '', 6, 12, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(63, '', 'JOLINA ARCIAGA', '', 'YMASA', '', 'E5 HARMONY VILLE VINALO ST CUPANG MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9954541524', 'aniloj.agaicra6@gmail.com', '', 6, 2, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(64, '', 'JEANINE', '', 'BALANE', '', '55 NARRA STREET INDANG CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9364658897', 'Balanej30@gmail.com', '', 11, 25, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(65, '', 'MAY', '', 'HALINA', '', 'VILLA MUNSOD 1 SAN JOAQUIN,PASIG CITY', '', 'PASIG', '', '', 'PHILIPPINES', '', '', '9213444162', 'halinamay7@gmail.com', '', 5, 6, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(66, '', 'KARYLL', '', 'HALINA', '', 'VILLA MUNSOD 1 SAN JOAQUIN,PASIG CITY', '', 'PASIG', '', '', 'PHILIPPINES', '', '', '9611842239', 'Halinakaryll80@gmail.com', '', 8, 23, 2003, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(67, '', 'JAZZER RINGO', 'MONES', 'FLORES', '', 'BLOCK 6 LOT 2 PARTRIDGE ST.CAMELLA HOME WOODHILLS', 'BARANGAY SAN ANTONIO, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9668624684', '', '', 10, 21, 1986, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(68, '', 'ALAIZA', '', 'FLORES', '', 'BLOCK 6 LOT 2 PARTRIDGE ST.CAMELLA HOME WOODHILLS', 'BARANGAY SAN ANTONIO, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9287628431', '', '', 11, 3, 1988, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(69, '', 'BERYL ANGEL', 'A.', 'MONTAÑEZ', '', 'BLK 9B LOT 4 BARRERA ST. LESSANDRA HEIGHTS', 'MOLINO IV, BACOOR, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '966826772', '', '', 11, 18, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(70, '', 'KHAEL', '', 'LUNA', '', 'BLOCK 4 LOT 32 FREEWILL SUBD MUNTINLUPA', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9194480355', '', '', 11, 20, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(71, '', 'LOVELY ROSE', '', 'PATUGA', '', '36 KATIHAN ST. POBLACION, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9356090383', 'patugalovelyrose@gmail.com', '', 10, 28, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(72, '', 'GRACE', '', 'RIOVEROS', '', 'P2 BLK3 PCS86 LOT43 SOUTHVILLE 3', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9756378323', 'Rioverosgrace01@gmail.com', '', 6, 19, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(73, '', 'JC', '', 'FERNANDEZ', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(74, '', 'ANGELA', '', 'CANOY', '', 'BLK 18 LOT 33 CELINA PLAINS SUBD PHASE 4', 'BRGY POOK STA ROSA, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '09224087455/ 09612652906', 'Yoongela05@gmail.com', '', 7, 5, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(75, '', 'DARRYL', '', 'GUTIERREZ', '', '191 BUENDIA ST. TUNASAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9755973290', 'gutierrezf247cs@gmail.com', '', 1, 11, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(76, '', 'ERICKA', '', 'VELASCO', '', 'MAKABUHAY ST. NBP RESERVATION', 'POBLACION MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9362887318', 'mangadaericka@gmail.com', '', 10, 21, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(77, '', 'JOHN LOUIE', 'D.', 'ESTOPA', '', '#22 ST. JAMES STREET TENSUAN SITE KATIHAN', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9488108500', 'johnlouis987654321@gmail.com', '', 5, 3, 1989, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(78, '', 'DIANE', 'L.', 'FLORES', '', '#22 ST. JAMES STREET TENSUAN SITE KATIHAN', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9261885758', 'floresdiane26@gmail.com', '', 4, 4, 1986, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(79, '', 'EMIL', '', 'PASCUAL', '', 'B 9 L 87 MARIGOLD. ST. PH3C GREENWOODS HGHTS', 'PALIPARAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9178046004', 'emilpascual88@yahoo.com', '', 12, 17, 1984, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(80, '', 'MARIFLOR', 'ANTONIO', 'PASCUAL', '', 'B 9 L 87 MARIGOLD. ST. PH3C GREENWOODS HGHTS', 'PALIPARAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9176724753', 'kelvipascual@gmail.com', '', 5, 21, 1979, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(81, '', 'ETHAN KOWJI', '', 'PASCUAL', '', 'B 9 L 87 MARIGOLD. ST. PH3C GREENWOODS HGHTS', 'PALIPARAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9165963382', 'ethankowjipascual@gmail.com', '', 10, 4, 2017, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(82, '', 'EUAN KENJI', '', 'PASCUAL', '', 'B 9 L 87 MARIGOLD. ST. PH3C GREENWOODS HGHTS', 'PALIPARAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 5, 28, 2013, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(83, '', 'LETICIA', 'CAGASCAS', 'PASCUAL', '', 'B 87 L 6 WALING WALING ST. BRGY. DATU ESMAEL', 'DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9672485825', '', '', 11, 15, 1952, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(84, '', 'EMILY', '', 'PASCUAL', '', 'B 87 L 6 WALING WALING ST. BRGY. DATU ESMAEL', 'DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9553027727', '', '', 4, 11, 1992, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(85, '', 'ELAINE', '', 'PASCUAL', '', 'B 87 L 6 WALING WALING ST. BRGY. DATU ESMAEL', 'DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9150551394', '', '', 2, 9, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(86, '', 'LEOLYN', 'B.', 'PAJANOSTAN', '', 'B 7 L30 LEGIAN PH B CARSADANG BAGO 1', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9267436147', '', '', 2, 17, 1992, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(87, '', 'KYLE', 'DARVINNE', 'PAJANOSTAN', '', 'B 7 L30 LEGIAN PH B CARSADANG BAGO 1', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 10, 26, 2013, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(88, '', 'LEA', 'ROSE', 'MERIOLES', '', 'B 7 L30 LEGIAN PH B CARSADANG BAGO 1', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9205891813', 'emilpascual88@yahoo.com', '', 10, 9, 1993, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(89, '', 'KRISTINE', '', 'CAGASCAS', '', 'B 7 L30 LEGIAN PH B CARSADANG BAGO 1', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9506568673', 'emilpascual88@yahoo.com', '', 9, 30, 1986, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(90, '', 'PRINCE LAWRENCE', '', 'CAGASCAS', '', 'B 7 L30 LEGIAN PH B CARSADANG BAGO 1', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', 'emilpascual88@yahoo.com', '', 12, 15, 2007, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(91, '', 'PIO', '', 'EVANGELISTA', '', 'B7 L7 YAKAL COR MOLAVE ST. PERPETUAL VILLAGE 6', 'MAMBOG 4, BACOOR CITY CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9171600410', 'sdevan19.24@gmail.com', '', 4, 10, 1960, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(92, '', 'LAWRENCE', '', 'ALCORDO', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9496083896', 'alcordo.grace8757@yahoo.com', '', 8, 9, 1965, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(93, '', 'GRACE', 'LARUGAL', 'ALCORDO', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9216302823', 'alcordo.grace8757@yahoo.com', '', 6, 6, 1974, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(94, '', 'NICOLE', 'JHAE', 'LARUGAL', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', 'alcordo.grace8757@yahoo.com', '', 9, 10, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(95, '', 'ILLUMINADA', '', 'LARUGAL', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9054381030', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(96, '', 'EULALIA', 'LARUGAL', 'RUIZ', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9054381030', 'alcordo.grace8757@yahoo.com', '', 10, 31, 1983, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(97, '', 'GABRIEL JHAE', '', 'RUIZ', '', 'KS 21 B1 L BRGY. NAVARRO GENERAL TRIAS CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9054381030', '', '', 9, 15, 2016, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(98, '', 'MARK ANGELO', '', 'LAVA', '', 'B4 L3 WESTBAY HOMES, DAAN-BUKID,BACOOR CITY', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9061783930', 'lavamarkangelo@gmail.com', '', 3, 24, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(99, '', 'ZENAIDA', '', 'MALAYAO', '', '383 SAN NICOLAS III, BACOOR CITY CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '903102990', 'kleyrmalayao@gmail.com', '', 9, 29, 1955, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(100, '', 'CLAIRE ANN JOYCE', 'R.', 'MALAYAO', '', '383 SAN NICOLAS III, BACOOR CITY CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9752972703', 'kleyrmalayao@gmail.com', '', 10, 19, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(101, '', 'MAEGAN', 'G.', 'MALAYAO', '', '383 SAN NICOLAS III, BACOOR CITY CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9384380795', 'malayaomaegan05@gmail.com', '', 5, 5, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(102, '', 'APOLINARIO', '', 'SOMEROS', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9175563484', '238angelicasers@gmail.com', '', 9, 21, 1964, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(103, '', 'RIZA', '', 'SOMEROS', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9998813461', '238angelicasers@gmail.com', '', 10, 31, 1972, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(104, '', 'MARIA BERNADETTE', 'C.', 'SOMEROS', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9209573045', '238angelicasers@gmail.com', '', 5, 24, 2004, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(105, '', 'ANGEL', 'C.', 'SOMEROS', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9215987464', '238angelicasers@gmail.com', '', 10, 5, 2006, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(106, '', 'EVANGELINE', '', 'TECIO', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '238angelicasers@gmail.com', '', 4, 7, 1982, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(107, '', 'JULIE ANNE', '', 'MONTIALBUCIO', '', 'B 14 L2 PH1 ATHENS ST. SOUTHPLAINS SUBD.', 'SALITAN 1, DASMARINAS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9459644130', '238angelicasers@gmail.com', '', 7, 15, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(108, '', 'EUSEBIA JONALYN', '', 'CAUTON', '', 'B6 L3 YAKAL ST. PINEWOOD VILLAGE, BUCANDALA 4', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9453191617', 'joycauton18@gmail.com', '', 12, 18, 1972, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(109, '', 'PATRICK JOHN', 'C.', 'CAUTON', '', 'B6 L3 YAKAL ST. PINEWOOD VILLAGE, BUCANDALA 4', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9453191617', 'cauton90@gmail.com', '', 9, 23, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(110, '', 'MARK JOSHUA', '', 'CAUTON', '', 'B6 L3 YAKAL ST. PINEWOOD VILLAGE, BUCANDALA 4', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9453191617', 'Joshuacauton12@gmail.com', '', 12, 16, 2000, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(111, '', 'KURT JUSTIN', '', 'CAUTON', '', 'B6 L3 YAKAL ST. PINEWOOD VILLAGE, BUCANDALA 4', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9453191617', 'kurtjustincauton13@gmail.com', '', 5, 31, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(112, '', 'JAMES ANDREW', '', 'CAUTON', '', 'B6 L3 YAKAL ST. PINEWOOD VILLAGE, BUCANDALA 4', 'IMUS CITY, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9453191617', 'Joycauton18@gmail.com', '', 12, 10, 2009, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(113, '', 'PAUL', '', 'CUSTODIO', '', 'ILOILO CITY', '', 'ILOILO', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(114, '', 'HAYDEE', '', 'VISAYA', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(115, '', 'ZENAIDA', '', 'VISAYA', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(116, '', 'HANNAH FRANCHESKA', '', 'BANDOJO', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(117, '', 'ISMAEL', '', 'SAGAYAP', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(118, '', 'ROSE ANN', '', 'CAUSAREN', '', 'SITIO GUBAT, MAITIM 1ST, AMADEO CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9177458112', '', '', 8, 16, 1988, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(119, '', 'JERRY', '', 'CAUSAREN', '', 'SITIO GUBAT, MAITIM 1ST, AMADEO CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9156085283', '', '', 12, 9, 1987, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(120, '', 'CLARISSE ANNE', '', 'MALAYAO', '', '383 C SAN NICOLAS 3 BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 5, 25, 2004, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(121, '', 'JUVY', '', 'MALAYAO', '', '47 KATIHAN ST. POBLACION, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 7, 16, 2003, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(122, '', 'PRECIOUS MIKEE', '', 'MALAYAO', '', '47 KATIHAN ST. POBLACION, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 4, 20, 2007, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(123, '', 'LUISA JANE', '', 'MEDRIANO', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(124, '', 'ARGEL', '', 'HEK', '', 'APARTMENT 7 DOOR 3 JAYSONVILLE II', 'PUTATAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9362452545', 'argelalochek24@gmail.com', '', 1, 24, 1989, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(125, '', 'FERMARIE', 'JANINE', 'HEK', '', 'APARTMENT 7 DOOR 3 JAYSONVILLE II', 'PUTATAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9266484242', 'argelalochek24@gmail.com', '', 1, 28, 1990, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(126, '', 'ANDREA', 'JANINE', 'HEK', '', 'APARTMENT 7 DOOR 3 JAYSONVILLE II', 'PUTATAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9362452545', 'argelalochek24@gmail.com', '', 4, 13, 2010, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(127, '', 'FHREDIE', 'RICK', 'BANDOJO', '', '', '', '', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(128, '', 'CRISTINA ALAIZA', '', 'MALAYAO', '', '383 C. SAN NICOLAS 3 BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 12, 30, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(129, '', 'MERCEDES', '', 'LAVA', '', 'BLK 4 LOT 3 DINAR ST. WESTBAY HOMES', 'DAANG-BUKID, BACOOR, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 3, 17, 1970, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(130, '', 'EDGARDO', '', 'LAVA', '', 'BLK 4 LOT 3 DINAR ST. WESTBAY HOMES', 'DAANG-BUKID, BACOOR, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 11, 22, 1961, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(131, '', 'DARWIN', '', 'BIROT', '', 'B15 L25 UNITE SHV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9179608682', 'wintot.darbo@gmail.com', '', 12, 27, 1979, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(132, '', 'CATHERINE', '', 'BIROT', '', 'B15 L25 UNITE SHV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9212355578', 'wincat_04@yahoo.com', '', 9, 4, 1978, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(133, '', 'CATERINA JIREH', '', 'BIROT', '', 'B15 L25 UNITE SHV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 2, 4, 2010, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(134, '', 'NELZON', '', 'DELOS REYES', '', 'UMALI COMPOUND SUMMITVILLE', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9158287204', 'nelzondelosreyes.ndr@gmail.com', '', 3, 16, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(135, '', 'HAZEL', '', 'GUERRERO', '', 'UMALI COMPOUND SUMMITVILLE', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9158287205', 'cjjccute@gmail.com', '', 6, 10, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(136, '', 'JAN CHLOE', '', 'GUERRERO', '', 'UMALI COMPOUND SUMMITVILLE', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9955077408', 'janclhoie.gmail.com', '', 1, 18, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(137, '', 'MICHELLE', 'TIAMSING', 'SHROCK', '', 'BLK 30 LOT 3 PROVERBS STREET TEOSEJO SUBDIVISION', 'TUNASAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9278119240', '', '', 11, 20, 1979, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(138, '', 'MARLA', 'TIAMSING', 'MOLAVIZAR', '', 'BLK 30 LOT 3 PROVERBS STREET TEOSEJO SUBDIVISION', 'TUNASAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9171266966', '', '', 10, 22, 1989, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(139, '', 'CHERRY MAE', '', 'NAZARENO', '', 'CROSLEY LANE LIBERTY HOMES CUPANG MUNTINLUOA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9234227801', '', '', 3, 3, 1990, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(140, '', 'LAILANE', '', 'HIPONIA', '', 'PHASE 1 RIVERA HEIGHTS BLK 5 LOT 43', 'BRGY STA ROSA LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9564751582', '', '', 10, 20, 1991, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(141, '', 'ANA', '', 'TANAEL', '', 'BLK 30 LOT 2 SOLDIERS HILLS VILLAGE', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9178679068', '', '', 12, 3, 1969, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(142, '', 'MHERVIN', '', 'ALIVEN', '', 'BENDIX LANE LIBERTY HOMES CUPANG MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9307686221', '', '', 10, 26, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(143, '', 'JINKY', '', 'ROMERO', '', 'CROSLEY LANE LIBERTY HOMES CUPANG MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9266173699', '', '', 9, 4, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(144, '', 'ERIN', '', 'TANAEL', '', 'BLK 30 LOT 2 SOLDIERS HILLS VILLAGE', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9564002503', '', '', 3, 4, 2004, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(145, '', 'JENELYN', '', 'MANDAL', '', 'PHASE 1 RIVERA HEIGHTS BLK 5 LOT 43', 'BRGY STA ROSA, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '92688521131', '', '', 2, 3, 1989, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(146, '', 'JULLIAH MARIE', '', 'ESCARO', '', 'ILOILO CITY', '', 'ILOILO', '', '', 'PHILIPPINES', '', '', '9386778770', '', '', 8, 6, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(147, '', 'JEAN MEAGAN', '', 'ESCARO', '', 'ILOILO CITY', '', 'ILOILO', '', '', 'PHILIPPINES', '', '', '9386778770', '', '', 10, 15, 2006, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(148, '', 'LOWI JUNE', '', 'ROMERO', '', 'CROSLEY LANE LIBERTY HOMES CUPANG MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9266173699', '', '', 6, 29, 2000, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(149, '', 'MILAN', '', 'TIAMSING', '', 'CROSLEY LANE LIBERTY HOMES CUPANG MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9234227801', '', '', 10, 26, 1981, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(150, '', 'MARILOU', '', 'TIAMSING', '', 'HINIGARAN NEGROS OCCIDENTAL', '', 'NEGROS', '', '', 'PHILIPPINES', '', '', '9972213887', '', '', 9, 23, 1959, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(151, '', 'MARK', '', 'ZAPANTA', '', 'SAN PEDRO LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9150995922', '', '', 11, 16, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(152, '', 'RACY JOY', 'F.', 'ASIS', '', 'BLOCK 8 LOT 47 LOVEBIRDS ST. CAMELLA HOMES', 'WOODHILLS, SAN PEDRO LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9989275543', 'racy.asis@gmail.com', '', 12, 20, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(153, '', 'LANCE JORDAN', 'O.', 'ASIS', '', 'BLOCK 8 LOT 47 LOVEBIRDS ST. CAMELLA HOMES', 'WOODHILLS, SAN PEDRO LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9754790584', 'mg.lance.asis@gmail.com', '', 11, 6, 1959, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(154, '', 'BRENDALYN', 'A.', 'PEREZ', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9612887015', 'bengadornado@gmail.com', '', 5, 25, 1957, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(155, '', 'EVELYN', '', 'ADORNADO', '', 'INFANTA QUEZON', '', 'QUEZON PROVINCE', '', '', 'PHILIPPINES', '', '', '9070897713', 'n.a', '', 10, 5, 1976, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(156, '', 'EDWIN', '', 'ADORNADO', '', 'INFANTA QUEZON', '', 'QUEZON PROVINCE', '', '', 'PHILIPPINES', '', '', '9070897713', 'n.a', '', 11, 17, 2009, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(157, '', 'MELANI', '', 'LABALAN', '', 'ADDRESS: B1 L2 ST JOSEPH RICHFIELD SUBD ', 'TAGAPO STA ROSA LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9199214950', 'n.a', '', 10, 13, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(158, '', 'SHANNIEL EINREB', 'T.', 'SIMPRON', '', 'ADDRESS: B1 L2 ST JOSEPH RICHFIELD SUBD ', 'TAGAPO STA ROSA LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9239902278', 'n.a', '', 12, 20, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(159, '', 'ALVINO', 'C.', 'CUARTERO', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9666987262', 'n.a', '', 11, 16, 1984, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(160, '', 'GLENDA', 'A.', 'CUARTERO', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9958168177', 'glendamacuartero@gmail.com', '', 11, 17, 1991, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(161, '', 'JEFFERSON', '', 'TOLENTINO', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9070897715', 'n.a', '', 10, 18, 2006, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(162, '', 'JEAN ALEXA', '', 'TOLENTINO', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9156480666', 'n.a', '', 12, 5, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(163, '', 'JENINE', '', 'ADORNADO', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9208151980', 'n.a', '', 8, 10, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(164, '', 'KYLE', 'RANIOLA', 'VELA', '', '75 MANGGAHAN SOLDIERS UPPER PUTATAN ', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', 'n.a', '', 12, 5, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(165, '', 'JUSTIN', '', 'SIA', '', 'A AND A APARTMENT UNIT J MANGGAHAN SOLDIER', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9517370952', '', '', 8, 1, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(166, '', 'PAULO', 'C.', 'SISON', '', '10FLOOR 8ADRIATICO BLDG. PADRE FAURA COR.', 'BOCOBO ST., ERMITA MANILA', 'MANILA', '', '', 'PHILIPPINES', '', '', '9088118450', 'psison2000@yahoo.com', '', 11, 25, 1979, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(167, '', 'MA. CRISELDA', 'U.', 'SISON', '', '10FLOOR 8ADRIATICO BLDG. PADRE FAURA COR.', 'BOCOBO ST., ERMITA MANILA', 'MANILA', '', '', 'PHILIPPINES', '', '', '9088112407', 'irishuysison@gmail.com', '', 7, 31, 1981, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(168, '', 'PAUL DAVID', 'U.', 'SISON', '', '10FLOOR 8ADRIATICO BLDG. PADRE FAURA COR.', 'BOCOBO ST., ERMITA MANILA', 'MANILA', '', '', 'PHILIPPINES', '', '', '', '', '', 4, 5, 2016, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(169, '', 'ARMELE', '', 'LEE', '', '1101 TOMAS MAPUA ST., STA CRUZ. MANILA', '', 'MANILA', '', '', 'PHILIPPINES', '', '', '9171503686', '', '', 2, 8, 1989, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(170, '', 'JONATHAN', '', 'LEE', '', '1101 TOMAS MAPUA ST., STA CRUZ. MANILA', '', 'MANILA', '', '', 'PHILIPPINES', '', '', '9166467929', '', '', 10, 18, 1985, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(171, '', 'ALSH', '', 'LEE', '', '1101 TOMAS MAPUA ST., STA CRUZ. MANILA', '', 'MANILA', '', '', 'PHILIPPINES', '', '', '9166467929', '', '', 3, 10, 2011, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(172, '', 'ARLENE', 'CASTRO', 'SUNGA', '', ' B25 L17 CHRYSANTHEMUM VIL, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9176593636', '', '', 6, 9, 1992, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(173, '', 'MA. ELENITA', '', 'CASTRO', 'JR', ' B25 L17 CHRYSANTHEMUM VIL, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9153641047', '', '', 6, 10, 1968, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(174, '', 'MARLOWE', '', 'PAMINTUAN', '', 'MARIKINA', '', 'MARIKINA', '', '', 'PHILIPPINES', '', '', '', '', '', 4, 1, 1993, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(175, '', 'ROMMEL', '', 'PAMINTUAN', '', 'MARIKINA', '', 'MARIKINA', '', '', 'PHILIPPINES', '', '', '9171124962', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(176, '', 'ROSEMEL', '', 'MISSION', '', '137C SAN DIEGO STREET BGY 88 EAST GRACE PARK', 'CALOOCAN CITY 1400', 'CALOOCAN', '', '', 'PHILIPPINES', '', '', '9052107354', '', '', 1, 3, 1979, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(177, '', 'AVEGEAL', '', 'SENERES', '', 'BLOCK 14 LOT 122 PHASE 3 E1 BRGY. LONGOS ', 'MALABON CITY', 'MALABON', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(178, '', 'PIOUS', '', 'SENERES', '', 'BLOCK 14 LOT 122 PHASE 3 E1 BRGY. LONGOS ', 'MALABON CITY', 'MALABON', '', '', 'PHILIPPINES', '', '', '9278717723', '', '', 11, 21, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(179, '', 'DOMINGO ALLAN', '', 'SAMUS', '', 'BLK 1 LOT 7 PAXTON STREET EAST FAIRVIEW', 'QUEZON CITY', 'QUEZON', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(180, '', 'JERAMIE', '', 'TUGAY', '', 'FAIRVIEW, QUEZON CITY', '', 'QUEZON', '', '', 'PHILIPPINES', '', '', '9161223970', '', '', 5, 29, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(181, '', 'MARY ALBERT', 'JOSE', 'GELACIO', '', 'FAIRVIEW, QUEZON CITY', '', 'QUEZON', '', '', 'PHILIPPINES', '', '', '9357193970', '', '', 10, 19, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(182, '', 'NENITA', '', 'JOSE', '', 'FAIRVIEW, QUEZON CITY', '', 'QUEZON', '', '', 'PHILIPPINES', '', '', '9532907049', '', '', 10, 19, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(183, '', 'REGINA', '', 'BENIPAYO', '', 'MOONWALK, PARANAQUE', '', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9164882819', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(184, '', 'KEANNA', '', 'SAAVEDRA', '', 'TAGAYTAY', '', 'TAGAYTAY', '', '', 'PHILIPPINES', '', '', '9616307427', '', '', 1, 10, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(185, '', 'KAEZLE', '', 'VINA', '', 'PHASE 3 BLOCK 42 LOT 14 GREEN ESTATE MALAGASANG', 'IMUS CAVITE ', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9279421447', '', '', 8, 19, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(186, '', 'EVE', '', 'VILLANUEVA', '', '400 MERCEDES ST. BUNLO, BOCAUE, BULACAN', '', 'BULACAN', '', '', 'PHILIPPINES', '', '', '', '', '', 9, 19, 1988, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(187, '', 'ISAMU', '', 'KATO', '', '#9 REAL STREET METROPOLIS 1', 'BRGY. STA LUCIA, PASIG CITY, 1608', 'PASIG', '', '', 'PHILIPPINES', '', '', '9171450411', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(188, '', 'SAHLEE', '', 'CHUA', '', 'B4 L16 GEN. SANTOS ST., SOUTH CITY HOMES', 'BINAN CITY, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9083965232', '', '', 7, 11, 1965, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(189, '', 'EVELYN', '', 'MAGNO', '', 'SAN FRANCISCO, USA', '', 'SAN FRANCISCO', '', '', 'USA', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(190, '', 'AIZELLE ROSE', 'LAUREANO', 'BERNARDO', '', 'BLK 29 LOT 5 LINNET ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9353101617', 'aizellelaureano06@gmail.com', '', 11, 6, 1991, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(191, '', 'EDSON MICKEL', '', 'BERNARDO', '', 'BLK 29 LOT 5 LINNET ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9175119106', 'kana.caleb@gmail.com', '', 9, 2, 1987, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(192, '', 'CHRISTINE ANNE', '', 'CABANTOG', '', 'BLK 29 LOT 30 LANDCO VILLAGE BRGY MAGSAYSAY', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9452377129', 'christineannecabantog@gmail.com', '', 8, 21, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(193, '', 'CLAUSELYN', '', 'CUASAY', '', 'BLK 1 LOT 6 CONSOLE 12 ELVINDA BRGY SAN VICENTE', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9669339345', 'claucuasay@yahoo.com', '', 10, 14, 1966, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(194, '', 'PIA VEREA', '', 'EVANGELISTA', '', 'BLK 7 LOT 7 MOLAVE ST CORNER YAKAL ST.', 'PERPETUAL VILLAGE 6, BRGY. MAMBOG, BACOOR, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9955672843', 'pia.vg.elista@gmail.com', '', 7, 5, 1990, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(195, '', 'STEPHANY', '', 'EVANGELISTA', '', 'BLK 7 LOT 7 MOLAVE ST CORNER YAKAL ST.', 'PERPETUAL VILLAGE 6 BRGY. MAMBOG, BACOOR, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9153270509', 'sdevan19.24@gmail.com', '', 8, 24, 1991, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(196, '', 'JAN KONRAD', '', 'SANTOS', '', '36 CATTLEYA ST. SAMPAGUITA VILLAGE', 'SAN PEDRO LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9565669808', 'konradviescasantos@gmail.com>', '', 1, 20, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(197, '', 'HELEN', '', 'LAUREANO', '', 'BLK 30 LOT 4 KESTREL ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9093806357', 'helenlaureano1210@gmail.com', '', 12, 10, 1965, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(198, '', 'JON MICHAEL', '', 'LAUREANO', '', 'BLK 30 LOT 4 KESTREL ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9772473137', 'jonmichaellaureano22@gmail.com', '', 7, 22, 1993, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(199, '', 'KLAUDINE ALLYANNA', '', 'LAUREANO', '', 'BLK 30 LOT 4 KESTREL ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9669339345', 'cuasayklaudine@gmail.com', '', 1, 25, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(200, '', 'JULIANNE ROSE', '', 'LAUREANO', '', 'BLK 30 LOT 4 KESTREL ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9067356073', 'Yanaaalaureano@gmail.com', '', 7, 24, 2004, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(201, '', 'JOVELYN', 'ESPAÑOL', 'NAVARRO', '', 'MALIPAY 3 MOLINO 4 BACOOR CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9984373773', 'jovelynnavarro5@gmail.com', '', 3, 14, 1989, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(202, '', 'MELANIE', '', 'NGITNGIT', '', '117 PHASE 2 SITIO BAYN BAYANAN BRGY. SAN VICENTE', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9190937255', 'melanie.gatessoftcorp@gmail.com', '', 3, 17, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(203, '', 'TIMOTHY', 'ISAIAH', 'PADERNAL', '', 'BLK 39 LOT 5 PLOVER ST. CAMELLA WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9152837332', 'timothypdnim@gmail.com', '', 12, 13, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', '');
INSERT INTO `person_per_no_teacher` (`COL 1`, `COL 2`, `COL 3`, `COL 4`, `COL 5`, `COL 6`, `COL 7`, `COL 8`, `COL 9`, `COL 10`, `COL 11`, `COL 12`, `COL 13`, `COL 14`, `COL 15`, `COL 16`, `COL 17`, `COL 18`, `COL 19`, `COL 20`, `COL 21`, `COL 22`, `COL 23`, `COL 24`, `COL 25`, `COL 26`, `COL 27`, `COL 28`, `COL 29`, `COL 30`, `COL 31`, `COL 32`, `COL 33`, `COL 34`, `COL 35`) VALUES
(204, '', 'AMELIA', '', 'TUD', '', 'E. RODRIGUEZ AVE., SAN AGUSTIN VILLAGE', 'BRGY MOONWALK, PARANAQUE CITY', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9216140210', '321meams@gmail.com', '', 5, 12, 1967, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(205, '', 'JOSELITO', '', 'TUD', '', 'E. RODRIGUEZ AVE., SAN AGUSTIN VILLAGE', 'BRGY MOONWALK, PARANAQUE CITY', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9215928722', 'jtud18@gmail.com', '', 5, 30, 1964, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(206, '', 'KEVIN LLOYD', '', 'TUD', '', 'E. RODRIGUEZ AVE., SAN AGUSTIN VILLAGE', 'BRGY MOONWALK, PARANAQUE CITY', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9217613098', 'kevinlloydtud@gmail.com', '', 1, 6, 1991, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(207, '', 'KRISTINE LOUISE', '', 'TUD', '', 'E. RODRIGUEZ AVE., SAN AGUSTIN VILLAGE', 'BRGY MOONWALK, PARANAQUE CITY', 'PARANAQUE', '', '', 'PHILIPPINES', '', '', '9369357896', 'kristinetud26@gmail.com', '', 12, 26, 1992, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(208, '', 'MARK', 'LUEN', 'FLORES', '', '254 JP RIZAL ST., SALA, CABUYAO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9081518649', 'markluen.flores@gmail.com', '', 8, 21, 1991, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(209, '', 'CZESCELINE', '', 'FLORES', '', '254 JP RIZAL ST., SALA, CABUYAO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9493167817', 'czes.lauguico@gmail.com', '', 10, 13, 1992, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(210, '', 'ANGELO', 'G.', 'LAGRIADA', '', 'BLOCK 5 LOT 3 MIMOSA ST. SJV10 PH3 BGRY.', 'LANGGAM SAN PEDRO LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9458468672', 'gelohuesca@gmail.com', '', 1, 19, 2001, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(211, '', 'ANNA VERONICA', 'S.', 'CHUA', '', 'MAJOR HOMES BRGY. SIRANGLUPA CALAMBA LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9151866183', 'imannachua@gmail.com', '', 10, 21, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(212, '', 'ARIEL', '', 'REMUGAT', '', 'BLK 4 LOT 9 BRGY, UNITED BAYANIHAN', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '63817009', 'arielremugat@gmail.com', '', 11, 9, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(213, '', 'BRYAN', 'V.', 'STA. ROSA', '', '7630 NARRA RD. BAYAN-BAYANAN, SAN VICENTE', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9154960359', 'bryanst.rose@gmail.com', '', 5, 30, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(214, '', 'MARK DANIEL', 'V.', 'REMUGAT', '', 'BLK 4 LOT 9 BRGY, UNITED BAYANIHAN', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9993269681', 'dremugat@gmail.com', '', 8, 12, 2000, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(215, '', 'JAKE PATRICK', 'B.', 'IMPERIAL', '', 'BLK 7 LOT 13 VILLA PAZ VILLAGE, BRGY. LANDAYAN', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9063323371', 'jakeimperial28@gmail.com', '', 7, 28, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(216, '', 'JOHN AERON', '', 'BERROYA', '', '193 C SAN PABLO ST. CARMONA CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9498919765', 'aeronberroya@gmail.com', '', 11, 4, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(217, '', 'JULIE PEARL', '', 'DIAZ', '', '740 PUROK 4 BRGY. SINALHAN', 'SANTA ROSA LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9260978327', 'juliepearldiaz@gmail.com', '', 5, 19, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(218, '', 'KIM SHEENA', 'A.', 'CUASAY', '', 'BLK 1 LOT 6 BEL AIR ST. CONSOLE 12', 'BRGY. FATIMA, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9665801337', 'kimsheenac@gmail.com', '', 6, 8, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(219, '', 'MARK JAMES', 'F.', 'HERNANDEZ', '', 'PHASE 4, 082 SITIO B-BAYANAN, BRGY. SAN VICENTE', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9279596216', 'mjhernandez8297@gmail.com', '', 8, 2, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(220, '', 'ROXANNE', 'A.', 'RUFO', '', 'PH2 BLK3 LOT12 JACINTO ST OLIVAREZ HOMES', 'SOUTH BIÑAN LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9457526627', 'roxanneadevarufo@gmail.com', '', 5, 22, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(221, '', 'SOPHIA', '', 'PATUGA', '', 'PH 3 BLK 35 LOT 19 SOUTHVILLE 3', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9456513688', 'sophiapatuga@gmail.com', '', 8, 2, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(222, '', 'VIRGINIA MARI', 'E.', 'BATITIS', 'II', 'BLK 2 LOT 82 PHASE4B GOLDEN CITY', 'SANTA ROSA, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9278392214', 'vbatitis@gmail.com', '', 5, 4, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(223, '', 'ALMA', '', 'MAURO', '', '1281 AGUADAS COR SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9331232191', 'maalsudario@gmail.com', '', 4, 13, 1967, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(224, '', 'MANUEL', '', 'SUDARIO', '', '1281 AGUADAS COR SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9331232191', '', '', 3, 12, 1938, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(225, '', 'LOLITO', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9167541164', '', '', 5, 5, 1958, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(226, '', 'MA. NANCY', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9167541164', 'ma.nanz06@yahoo.com', '', 11, 6, 1964, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(227, '', 'MARC SYDNY', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9488228446', 'marcsydnymostacesa@gmail.com', '', 1, 27, 1987, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(228, '', 'HANNAH JEAN', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9773893923', 'hmostacesa90@gmail.com', '', 6, 22, 1990, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(229, '', 'JAHZEEL', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9771064096', 'mostacesajahzeel@gmail.com', '', 4, 6, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(230, '', 'JEMUEL', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9054173917', 'yexel341@yahoo.com', '', 9, 6, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(231, '', 'EMMANUEL JOHN', '', 'MOSTACESA', '', '938 SAN LUIS ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9060843965', 'johnmostacesa234@gmail.com', '', 2, 12, 2006, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(232, '', 'FAUSTINO', '', 'DEL PILAR', '', '68 STO. NIÑO ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9157859987', '', '', 3, 6, 1943, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(233, '', 'DINAH', '', 'DEL PILAR', '', '68 STO. NIÑO ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9456209753', 'dinahsam02@gmail.com', '', 11, 14, 1983, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(234, '', 'DARYL', '', 'SEVILLA', '', 'STA. POTENCIANA ST., BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9157859987', 'daryl.sevilla1676@gmail.com', '', 9, 16, 1976, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(235, '', 'PELMAR', '', 'ALDE', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9124073928', '', '', 3, 7, 2002, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(236, '', 'MA. PELMA', '', 'ALDE', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9091291068', '', '', 7, 13, 2003, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(237, '', 'LUZVIMINDA', '', 'GERILLA', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '', '', '', 5, 30, 1969, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(238, '', 'JAYSANN', '', 'GERNATO', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9487698150', '', '', 2, 11, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(239, '', 'JAY ANN', '', 'GERNATO', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '9487698150', '', '', 1, 7, 2007, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(240, '', 'JESSICA', '', 'GERNATO', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '', '', '', 6, 18, 2009, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(241, '', 'ANGELITA', '', 'MAROTO', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '', '', '', 12, 25, 1954, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(242, '', 'ALDWIN', '', 'GALLAMOS', '', 'BRGY. SAN JOSE EAST, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '', '', '', 3, 29, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(243, '', 'CAIRA', '', 'CULAS', '', 'BRGY. TAMBUCO, BURAUEN, LEYTE', '', 'LEYTE', '', '', 'PHILIPPINES', '', '', '', '', '', 4, 18, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(244, '', 'RITCH', '', 'DACULES', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9985393906', 'ritch_dacules@yahoo.com', '', 5, 23, 1985, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(245, '', 'HANNAH JANE', 'S.', 'DACULES', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9122157812', 'dacules18.hannah.jane@gmail.com', '', 10, 10, 1990, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(246, '', 'RHEALYN', 'S.', 'BASBANIO', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9480842626', 'rhealynbasbanio99@gmail.com', '', 4, 23, 1999, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(247, '', 'GERALDINE', 'D.', 'RIMANDO', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9557193270', 'geraldinerimando1989@yahoo.com', '', 1, 11, 1989, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(248, '', 'NORMAN', 'B.', 'RIMANDO', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9361641488', 'normanrimando1990@yahoo.com', '', 8, 9, 1990, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(249, '', 'ADIMAR', 'M.', 'OXENIOLA', '', '448 VIRGO ST. BLK 3 PUROK 1', 'BAYANAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9985393906', 'adimaroxeniola@yahoo.com', '', 4, 8, 1987, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(250, '', 'KARL', 'LORENZ', 'TUD', '', '15039 B SAN MARTIN STREET, SAN AGUSTIN VILLAGE', 'MOONWALK PARANAQUE CITY', 'PARAÑAQUE', '', '', 'PHILIPPINES', '', '', '9217613063', 'karllorenztud@gmail.com', '', 9, 28, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(251, '', 'RIZA', '', 'PANGANIBAN', '', 'PUROK 1, BLOCK 1, DOLLETON STREET', 'BAYANAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9351940340', 'panganiban0077@gmail.com', '', 12, 30, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(252, '', 'VIC LORENZ', '', 'NUÑEZ', '', '247 SITIO SAN ANTONIO, POBLACION, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9759790039', 'vlnunezf247cs@gmail.com', '', 5, 24, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(253, '', 'ALEXANDER', '', 'TABLATE', '', '153 C KAPPIVILLE KATIHAN STREET', 'POBLACION MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9057256577', 'tablatealexandert@gmail.com', '', 5, 21, 1992, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(254, '', 'GABRIEL JAMES', '', 'MEJIA', '', 'BLOCK 7 LOT 3, PINAGPALA SAN GUILLERMO STREET', 'BAYANAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9495893166', 'mejia.gabrieljames@gmail.com', '', 10, 1, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(255, '', 'ROY', '', 'SANTILLAN', '', 'BLOCK 2 LOT 5, BERNOULLI STREET, AIRSPEED VILLE', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9566326332', 'cecilia.labrador.santillan@gmail.com', '', 8, 20, 1978, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(256, '', 'IKIT', '', 'SANTILLAN', '', 'BLOCK 2 LOT 5, BERNOULLI STREET, AIRSPEED VILLE', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9272345657', 'cecilia.labrador.santillan@gmail.com', '', 5, 10, 1984, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(257, '', 'VICTORIE MERL', '', 'CABERO', '', '88 KATIHAN ST., KAPPIVILLE, POBLACION', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9355059184', 'victoriecabero@gmail.com', '', 7, 22, 1991, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(258, '', 'PIA', 'MARCHESA', 'BUE', '', 'B3 L13 JAMCEL VILLA ARCIAGA, BAYANAN', 'MUNTINLUPA CITY ', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9552358561', 'p.marchesa06@gmail.com', '', 2, 6, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(259, '', 'LENIEL CHRISTIAN', '', 'DESUNIA', '', 'B3 L13 JAMCEL VILLA ARCIAGA, BAYANAN', 'MUNTINLUPA CITY ', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9127705303', 'lenieldesunia@gmail.com', '', 10, 10, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(260, '', 'RAYMARK', 'MARTINEZ', 'VOLANTE', '', 'UNITY COMPOUND ESTANISLAO RD PUTATAN MUNTINLUPA', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9457507219', '', '', 7, 7, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(261, '', 'APRIL JOY', '', 'BALANE', '', 'BLK 3 LOT 2 KATARUNGAN VILLAGE 2', 'POBLACION MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9661608100', 'balaneajoy143@gmail.com', '', 4, 27, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(262, '', 'MARY ROSE', '', 'FRANCISCO', '', 'SAMPAGUITA ROAD, POBLACION MUNTINLUPA', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9179924289', 'maryrose.francisco05@gmail.com', '', 11, 5, 1992, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(263, '', 'PRISCILLA', 'ORTINEZ', 'OPLEDA', '', 'QUEEN VEE ST. MANGGAHAN ITAAS', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9553419680', 'paulamaeopleda@gmail.com', '', 8, 27, 1980, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(264, '', 'PAULA MAE', 'ORTINEZ', 'OPLEDA', '', 'QUEEN VEE ST. MANGGAHAN ITAAS', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9050723488', 'paulamaeopleda@gmail.com', '', 6, 24, 2002, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(265, '', 'PRECIOUS ANN', 'ORTINEZ', 'OPLEDA', '', 'QUEEN VEE ST. MANGGAHAN ITAAS', 'PUTATAN MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9553419680', 'paulamaeopleda@gmail.com', '', 5, 23, 2006, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(266, '', 'ELGENE', '', 'BAUTISTA', '', 'KINGFISHER ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9950213237', 'elgenebautista04@gmail.com', '', 10, 10, 1974, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(267, '', 'JERUM', '', 'OROBIA', '', 'HYACINTH ST. SGV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9637160788', 'jerumorobia@yahoo.com', '', 2, 8, 1988, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(268, '', 'ERICA', 'DEVORA', 'BAUTISTA', '', 'KINGFISHER ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9369392182', 'ericadevora14@gmail.com', '', 3, 30, 1990, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(269, '', 'EMMANUEL', '', 'DELA CRUZ', 'JR', 'KANLAON ST. COUNTRY HOMES, PUTATAN', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9278375599', 'bongmusic1220@gmail.com', '', 6, 8, 1979, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(270, '', 'JOAN TES', '', 'MANANSALA', '', 'CENTENNIAL TOWN HOMES, SAN ISISDRO, CABUYAO', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9478701831', 'jtmanansala55@gmail.com', '', 6, 1, 1992, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(271, '', 'JAYSON NIGEL', '', 'GALIT', '', 'KINGFISHER ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9976950014', 'jasonsarah012594@gmail.com', '', 1, 25, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(272, '', 'SARAH', '', 'GALIT', '', 'KINGFISHER ST. CAMELLA HOMES WOODHILLS', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9976950014', 'jasonsarah012594@gmail.com', '', 8, 1, 1994, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(273, '', 'MARY JANE', '', 'LAGARTO', '', 'HYACINTH ST. SGV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(274, '', 'EMILY', '', 'LUPANGCO', '', 'TIPAUREL NATIONAL RD. PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9157007551', '', '', 9, 5, 1973, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(275, '', 'ERJOHN', '', 'LAGARTO', '', 'HYACINTH ST. SGV, PUTATAN, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(276, '', 'ELVIRA', 'MONES', 'DELA CRUZ', '', 'SOLIVEN ST. SGV, PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9175812379', 'elviradlc21@gmail.com', '', 1, 31, 1961, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(277, '', 'JONATHAN', '', 'VIADO', '', 'B42 LOT 81 PHASE2 ALTATIERA HOMES BRY. OLAES', 'GMA, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9476159484', 'jntn020777@gmail.com', '', 2, 7, 1977, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(278, '', 'IREENE', '', 'VIADO', '', 'B42 LOT 81 PHASE2 ALTATIERA HOMES BRY. OLAES', 'GMA, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9476159484', 'ireeneviado777@gmail.com', '', 12, 8, 1985, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(279, '', 'IMEE', '', 'DE VENECIA', '', '985 MAGALLANES ST. BRGY. MADUYA, CARMONA, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9208320514', '', '', 12, 8, 1985, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(280, '', 'AREANE', '', 'ARELLANO', '', '3101 UNIT 6, SAN JOSE ST., BRGY. 3 CARMONA,CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9457482873', 'jamyrr052213@gmail.com', '', 5, 5, 1986, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(281, '', 'MICHEAL', 'MEDINA', 'ARELLANO', '', '3101 UNIT 6, SAN JOSE ST., BRGY. 3 CARMONA,CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9969095779', 'areane.mamauag@deped.gov.ph', '', 7, 17, 1986, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(282, '', 'JONNA', 'DAPITAN', 'SALUMBIDES', '', 'B13 L10 CONEFLOWER ST METROVILLE COMPLEX', 'HALANG BINAN, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9190843512', 'jonna.salumbides@gmail.com', '', 11, 29, 1985, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(283, '', 'MICHAEL', '', 'SALUMBIDES', '', 'B13 L10 CONEFLOWER ST METROVILLE COMPLEX', 'HALANG BINAN, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9190843513', 'salumbides.f247cs@gmail.com', '', 11, 7, 1986, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(284, '', 'ANGELICA', '', 'DAPITAN', '', 'PHASE 3 BLOCK 3 LOT 4 SOUTHVILLE 3 POBLACION', 'MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9475350271', 'angelicadapitan1@gmail.com', '', 12, 22, 1999, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(285, '', 'KAREN', '', 'TAN', '', 'BLK 6 LOT 9 PH3 METROVILLE COMPLEX BIÑAN LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9190735938', 'karen.agapay.tan@gmail.com', '', 5, 9, 1980, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(286, '', 'LINDA', '', 'VALENTIN', '', 'BLK 17 LOT 37 PH4 LILY ST. BRGY. SAN ANTONIO', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9156248296', 'Kazma223130@gmail.com', '', 8, 30, 1972, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(287, '', 'REGGIE', '', 'VALENTIN', '', 'BLK 17 LOT 37 PH4 LILY ST. BRGY. SAN ANTONIO', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9156248296', 'Rcvalentin@gmail.com', '', 1, 31, 1979, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(288, '', 'KAZANDRA', 'MAPLE', 'VALENTIN', '', 'BLK 17 LOT 37 PH4 LILY ST. BRGY. SAN ANTONIO', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9156248296', 'zandravalentin22@gmail.com', '', 10, 22, 2003, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(289, '', 'REMEDIOS', '', 'MAMAUAG', '', 'BLK.5 LOT 64 CEDAR VILLAGE BRGY. LANTIC', 'CARMONA, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9061444430', '', '', 9, 26, 1962, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(290, '', 'AIDA', '', 'ALUMIA', '', '985 MAGALLANES ST. BRGY. MADUYA, CARMONA, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9208320514', '', '', 1, 28, 1959, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(291, '', 'JOYCIE', 'DE VERA', 'RAMOS', '', 'B10 L42 CEDAR1 LANTIC CARMONA, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9972134888', 'Joyciedv@gmail.com', '', 3, 26, 1991, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(292, '', 'DANTE', '', 'RAMOS', '', 'B10 L42 ROSAL ST CEDAR PH1 CARMONA CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9264418267', 'danteramos007@gmail.com', '', 9, 17, 1991, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(293, '', 'JANELO', '', 'DE VERA', '', 'B10 L42 ROSAL ST CEDAR PH1 CARMONA CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9398366934', 'nildevera2693@gmail.com', '', 4, 26, 1993, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(294, '', 'CAREN', '', 'DOLAR', '', 'B84 L21 PHASE4 MABUHAY CITY PALIPARAN 3', 'DASMARINAS CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9368537658', 'Carendolar03@gmail.com', '', 3, 21, 1984, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(295, '', 'MARCIAL', '', 'DOLAR', '', 'B84 L21 PHASE4 MABUHAY CITY PALIPARAN 3', 'DASMARINAS CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9268879175', 'Marcialdolar@gmail.com', '', 12, 7, 1983, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(296, '', 'MARC JEANELLE', '', 'DOLAR', '', 'B84 L21 PHASE4 MABUHAY CITY PALIPARAN 3', 'DASMARINAS CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9262880724', 'Marcjeanelledolar@gmail.com', '', 4, 18, 2003, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(297, '', 'CLARENCE JOYCE', '', 'DOLAR', '', 'B84 L21 PHASE4 MABUHAY CITY PALIPARAN 3', 'DASMARINAS CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', 'Clarencejoycedolar@gmail.com', '', 9, 14, 2010, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(298, '', 'JESUSA', '', 'VELASQUEZ', '', '008 IRENE MATIAS ST PILAR RD. ALMANZA UNO', 'LAS PINAS CITY', 'LAS PINAS', '', '', 'PHILIPPINES', '', '', '9567805810', 'iamsvela@gmail.com', '', 1, 2, 1965, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(299, '', 'STEPHANIE QUEEN', '', 'DE VENECIA', '', '985 MAGALLANES ST. BRGY. MADUYA, CARMONA, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9208320514', '', '', 3, 14, 2015, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(300, '', 'KENT', '', 'VALENTIN', '', 'BLK 17 LOT 37 PH4 LILY ST. BRGY. SAN ANTONIO', 'SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9156248296', 'Kazma223130@gmail.com', '', 8, 9, 2012, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(301, '', 'JIANNA KYLE', '', 'SALUMBIDES', '', 'B13 L10 CONEFLOWER ST METROVILLE COMPLEX', 'HALANG BINAN, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '', '', '', 3, 22, 2013, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(302, '', 'CHRISTIAN', '', 'GENOTIVA', '', 'MOLINO, BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(303, '', 'IRMA', '', 'GENOTIVA', '', 'MOLINO, BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(304, '', 'ASTERI', '', 'GENOTIVA', '', 'MOLINO, BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(305, '', 'BRANDALE', '', 'GENOTIVA', '', 'MOLINO, BACOOR, CAVITE', '', 'CAVITE', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(306, '', 'CHRISTIAN', 'F.', 'EMAGUE', '', '472 M.L QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9479629579', 'chris.emague@gmail.com', '', 10, 26, 1985, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(307, '', 'IVORY', 'ANDALES', 'EMAGUE', '', '472 M.L QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9175522390', 'ivory.emague@gmail.com', '', 10, 22, 1986, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(308, '', 'EDWIN', 'S.', 'MEJIA', '', 'B7 LOT 3 SAMAHANG PINAGPALA, SAN GUILLERMO ST.', 'BAYANAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9503460244', '', '', 10, 14, 1970, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(309, '', 'ARLENE', 'A.', 'MEJIA', '', 'B7 LOT 3 SAMAHANG PINAGPALA, SAN GUILLERMO ST.', 'BAYANAN, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9630849615', '', '', 3, 15, 1975, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(310, '', 'CAMAY', 'A.', 'CREBELLO', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9109400161', 'mae100589@gmail.com', '', 10, 15, 1989, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(311, '', 'EDILBERTO', '', 'CREBELLO', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9122327093', '', '', 1, 22, 1981, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(312, '', 'FROILAN', '', 'ANDALES', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9501983439', '', '', 12, 21, 1956, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(313, '', 'ROSANA', '', 'ANDALES', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9109400161', '', '', 9, 2, 1958, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(314, '', 'IRIS', '', 'ANDALES', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9287185139', 'iris.andales@gmail.com', '', 3, 16, 1985, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(315, '', 'FRANCESCA NICOLE', '', 'ANDALES', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9218886499', 'nicoleandales1920@gmail.com', '', 10, 20, 2000, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(316, '', 'LAWRENCE AGATHA', '', 'PAJENADO', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9284793006', 'laurenceagathap@gmail.com', '', 10, 30, 2003, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(317, '', 'MARK VINCENT', '', 'GUIS', '', '195 SAN GUILLERMO ST. PUTATAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9204998673', 'markygulp@gmail.com', '', 8, 19, 2000, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(318, '', 'CEDRIC', '', 'CREBELLO', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9987635855', '', '', 8, 8, 2009, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(319, '', 'EMIL YUAN', '', 'CREBELLO', '', '475 M.L. QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY ', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9987635855', '', '', 8, 7, 2011, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(320, '', 'LILAC MADISON', 'A.', 'EMAGUE', '', '472 M.L QUEZON PUROK 3 CUPANG, MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 5, 5, 2017, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(321, '', 'JONATHAN', '', 'UNTALAN', '', 'CALAMBA LAGUNA , DONA FELIZA PACIANO RIZAL', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9067702921', '', '', 1, 7, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(322, '', 'MIKHAEL', '', 'LUNA', '', 'B 4 L32 FREEWILL SUBD. BRGY. PUTATAN MUNTINLUPA', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9994876718', '', '', 1, 26, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(323, '', 'KAEZLE', '', 'VIÑA', '', 'PHASE 3 BLOCK 42 LOT 14 GREEN ESTATE MALAGASANG', 'IMUS, CAVITE', 'CAVITE', '', '', 'PHILIPPINES', '', '', '9279421447', '', '', 8, 19, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(324, '', 'JUMAR', '', 'TOLEDO', '', '163 NATL RD. TUNASAN MUNTINLUPA CITY', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9568724738', '', '', 6, 29, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(325, '', 'ANGELICA', '', 'VELASCO', '', '12 F. CAPARAS STREET IVC MARIKINA CITY', '', 'MARIKINA', '', '', 'PHILIPPINES', '', '', '9562835330', '', '', 12, 20, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(326, '', 'MICHAEL', '', 'DACPANO', '', 'VALENZUELA', '', 'VALENZUELA', '', '', 'PHILIPPINES', '', '', '', '', '', 1, 0, 1900, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(327, '', 'ROLANDO', '', 'UY', '', 'BGRY. LAIYA IBABAO SAN JUAN BATANGAS', '', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9178298339', '', '', 11, 20, 1956, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(328, '', 'EMILY', '', 'SANTOS', '', 'BGRY. LAIYA IBABAO SAN JUAN BATANGAS', '', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9454100384', '', '', 1, 9, 1967, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(329, '', 'EMETERIO', 'V.', 'ADLONG', '', '#36 SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9776427304', 'longads8@gmail.com', '', 3, 3, 1964, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(330, '', 'EMELITA', 'M.', 'ADLONG', '', '#36 SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9286809960', 'Emelitaadlong@gmail.com', '', 4, 17, 1958, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(331, '', 'RACQUEL', 'M.', 'ADLONG', '', '#36 SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9759357078', 'racq.adz@gmail.com', '', 3, 26, 2000, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(332, '', 'GARY', 'F.', 'MADRID', '', 'LOT 3 BLK 30B PH 4, DAHLIA ST., MT. CLAIRE VILL.', 'SANTA ANASTACIA, SANTO TOMAS CITY, BATANGAS', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9178398382', 'gary.madrid1127@gmail.com', '', 11, 27, 1971, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(333, '', 'DIVINE', 'Y.', 'MADRID', '', 'LOT 3 BLK 30B PH 4, DAHLIA ST., MT. CLAIRE VILL.', 'SANTA ANASTACIA, SANTO TOMAS CITY, BATANGAS', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9293494225', 'divinemadrid11@gmail.com', '', 7, 13, 1969, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(334, '', 'SHAILA GAIN', 'Y.', 'MADRID', '', 'LOT 3 BLK 30B PH 4, DAHLIA ST., MT. CLAIRE VILL.', 'SANTA ANASTACIA, SANTO TOMAS CITY, BATANGAS', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9153172191', 'madridshaila11@gmail.com', '', 10, 11, 1996, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(335, '', 'SETH TOBIAS', 'Y.', 'MADRID', '', 'LOT 3 BLK 30B PH 4, DAHLIA ST., MT. CLAIRE VILL.', 'SANTA ANASTACIA, SANTO TOMAS CITY, BATANGAS', 'BATANGAS', '', '', 'PHILIPPINES', '', '', '9293494225', 'madrid.toby@gmail.com', '', 2, 5, 2002, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(336, '', 'CLARITO', 'I.', 'ESTELEYDES', '', 'PHASE 1, B16, L11 SOUTHVILLE 3', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9164276811', 'Claritoesteleydes@gmail.com', '', 10, 11, 1977, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(337, '', 'EDWINA', 'M.', 'ESTELEYDES', '', 'PHASE 1, B16, L11 SOUTHVILLE 3', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9053320328', 'Wengmanaloesteleydes@gmail.com', '', 4, 23, 1975, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(338, '', 'CHLOE KATE', 'M.', 'ESTELEYDES', '', 'PHASE 1, B16, L11 SOUTHVILLE 3', 'POBLACION, MUNTINLUPA CITY', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '9053320328', 'Nengesteleydes@yahoo.com', '', 7, 26, 2012, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(339, '', 'REGIEMEL', 'M.', 'MORTIL', '', 'BLK 14 LOT 45 DAISY ST., SAINT JOSEPH VILLAGE 9', 'LANGGAM, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9185644802', 'rmm_arjhay@yahoo.com', '', 9, 29, 1984, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(340, '', 'PENNY', 'M.', 'GATMAITAN', '', 'BLK 14 LOT 45 DAISY ST., SAINT JOSEPH VILLAGE 9', 'LANGGAM, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9185644802', 'N/A', '', 2, 19, 1950, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(341, '', 'JOHN DAVID', 'I.', 'GATMAITAN', '', 'BLK 14 LOT 45 DAISY ST., SAINT JOSEPH VILLAGE 9', 'LANGGAM, SAN PEDRO, LAGUNA', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9089099179', 'N/A', '', 3, 8, 2007, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(342, '', 'EVELYN', 'E.', 'ZITA', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9998136374', 'evelyn_ronniezita@yahoo.com', '', 9, 21, 1975, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(343, '', 'RONALYN', 'E.', 'ZITA', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9013643473', 'preciousnicolefacto@gmail.com', '', 5, 16, 1997, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(344, '', 'EMIELYN', 'E.', 'ZITA', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '', 'zitaemielyn@gmail.com', '', 5, 28, 1999, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(345, '', 'DIVINA', '', 'ESPAÑOL', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9107861313', 'divineespanol08@gmail.com', '', 6, 28, 2002, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(346, '', 'DENNO', 'E.', 'ZITA', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9998136374', 'N/A', '', 5, 22, 2012, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(347, '', 'JOLIZA', 'B.', 'ESPAÑOL', '', 'SITIO RUSTAN, BRGY. LANGGAM, SAN PEDRO, LAGUNA', '', 'LAGUNA', '', '', 'PHILIPPINES', '', '', '9952700456', 'Aziloje@gmail.com', '', 10, 1, 1990, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(348, '', 'GRETCHEN', '', 'BARTOLOME', '', 'PUROK 5 CALAGDAAN, CANTILAN, SURIGAO DEL SUR', '', 'SURIGAO DEL SUR', '', '', 'PHILIPPINES', '', '', '88 6909539550', 'bartolomeezekela@gmail.com', '', 4, 3, 1986, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(349, '', 'KAYLA MARIE', '', 'BARTOLOME', '', 'PUROK 5 CALAGDAAN, CANTILAN, SURIGAO DEL SUR', '', 'SURIGAO DEL SUR', '', '', 'PHILIPPINES', '', '', '9630670863', 'ezekelabartolome@gmail.com', '', 4, 11, 2008, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(350, '', 'REN ABRAHAM', '', 'BUNGAOS', '', 'PRK 5, BARANGAY CONEL,  GENERAL SANTOS CITY', '', 'GENERAL SANTOS', '', '', 'PHILIPPINES', '', '', '9512527111', 'renbungaos@gmail.com', '', 3, 1, 2018, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(351, '', 'REYNALDO', '', 'BUNGAOS', '', 'PRK 5, BARANGAY CONEL,  GENERAL SANTOS CITY', '', 'GENERAL SANTOS', '', '', 'PHILIPPINES', '', '', '9512527111', 'renbungaos@gmail.com', '', 7, 22, 1989, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(352, '', 'JHERBHEE', '', 'CABADING', '', 'UPPER BICUTAN, TAGUIG CITY', '', 'TAGUIG', '', '', 'PHILIPPINES', '', '', '9453414830', 'Jhebcabading@gmail.com', '', 8, 13, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(353, '', 'EVON PEARL', '', 'CALIBAYAN', '', 'PUROK HILLSIDE, BRGY.LAKE LAHIT, LAKE SEBU', 'SOUTH COTABATO', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9516582264', 'cevonpearl@gmail.com', '', 3, 11, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(354, '', 'JEWEL ANGEL', 'GRACE', 'CALIBAYAN', '', 'PUROK HILLSIDE, BRGY.LAKE LAHIT, LAKE SEBU', 'SOUTH COTABATO', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9632331633', 'ezekelabartolome@gmail.com', '', 3, 17, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(355, '', 'CYD CLARENCE', '', 'DEBARBO', '', 'PUROK HILLSIDE, BRGY.LAKE LAHIT, LAKE SEBU', 'SOUTH COTABATO', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9632331635', 'cevonpearl@gmail.com', '', 10, 21, 2005, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(356, '', 'AKISHA MAY', '', 'LIBONA', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9084089476', 'cevonpearl@gmail.com', '', 6, 15, 2011, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(357, '', 'FRANZ HOMER', '', 'LIBONA', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9084089476', 'cevonpearl@gmail.com', '', 3, 23, 2009, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(358, '', 'HARLIE QUEEN', '', 'LIBONA', '', 'TALISAY, LAKE SEBU, SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9484649627', 'Queenying0817@gmail.com', '', 10, 17, 2010, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(359, '', 'MARICEL', '', 'LIBONA', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9084089476', 'cevonpearl@gmail.com', '', 2, 26, 1986, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(360, '', 'MELVIE', '', 'MANACOF', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9655447780', 'melviemanacof@gmailc.om', '', 12, 18, 1999, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(361, '', 'VERGIE', '', 'MANACOF', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9659773755', 'melviemanacof@gmailc.om', '', 8, 8, 1979, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(362, '', 'ANGEL', '', 'MANAFA', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9979557112', 'manafaangel@gmail.com', '', 7, 17, 2003, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(363, '', 'JHONNY', '', 'MANAFA', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9518038368', 'Johnnymanafa@gmail.com', '', 9, 10, 1987, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(364, '', 'FRITZIE LYNN', '', 'OFTANA', '', 'PRK 5, BARANGAY CONEL,  GENERAL SANTOS CITY', '', 'GENERAL SANTOS', '', '', 'PHILIPPINES', '', '', '9098872772', 'renbungaos@gmail.com', '', 4, 28, 1998, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(365, '', 'STEPHANIE NICOLE', '', 'OFTANA', '', 'PRK 5, BARANGAY CONEL,  GENERAL SANTOS CITY', '', 'GENERAL SANTOS', '', '', 'PHILIPPINES', '', '', '9098872772', 'renbungaos@gmail.com', '', 1, 31, 2015, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(366, '', 'JHASPHER', '', 'WALANG', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9635968414', 'Jaspherwalang@gmail.com', '', 4, 14, 1995, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(367, '', 'NHIPS', '', 'WALANG', '', 'LAKE SEBU SOUTH COTABATO', '', 'SOUTH COTABATO', '', '', 'PHILIPPINES', '', '', '9635968414', 'phingkayw@gmail. com', '', 11, 27, 2002, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', ''),
(368, '', 'ISAIAS', '', 'DELA CRUZ', 'JR', 'MUNTINLUPA', '', 'MUNTINLUPA', '', '', 'PHILIPPINES', '', '', '', '', '', 7, 1, 1960, '', '', '', '', '', '', '', '', 1, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `person_roles`
--

CREATE TABLE `person_roles` (
  `per_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pledge_plg`
--

CREATE TABLE `pledge_plg` (
  `plg_plgID` mediumint(9) NOT NULL,
  `plg_FamID` mediumint(9) DEFAULT NULL,
  `plg_FYID` mediumint(9) DEFAULT NULL,
  `plg_date` date DEFAULT NULL,
  `plg_amount` decimal(8,2) DEFAULT NULL,
  `plg_schedule` enum('Weekly','Monthly','Quarterly','Once','Other') COLLATE utf8_unicode_ci DEFAULT NULL,
  `plg_method` enum('CREDITCARD','CHECK','CASH','BANKDRAFT','EGIVE') COLLATE utf8_unicode_ci DEFAULT NULL,
  `plg_comment` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `plg_DateLastEdited` date NOT NULL DEFAULT '2016-01-01',
  `plg_EditedBy` mediumint(9) NOT NULL DEFAULT 0,
  `plg_PledgeOrPayment` enum('Pledge','Payment') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pledge',
  `plg_fundID` tinyint(3) UNSIGNED DEFAULT NULL,
  `plg_depID` mediumint(9) UNSIGNED DEFAULT NULL,
  `plg_CheckNo` bigint(16) UNSIGNED DEFAULT NULL,
  `plg_Problem` tinyint(1) DEFAULT NULL,
  `plg_scanString` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `plg_aut_ID` mediumint(9) NOT NULL DEFAULT 0,
  `plg_aut_Cleared` tinyint(1) NOT NULL DEFAULT 0,
  `plg_aut_ResultID` mediumint(9) NOT NULL DEFAULT 0,
  `plg_NonDeductible` decimal(8,2) NOT NULL,
  `plg_GroupKey` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `propertytype_prt`
--

CREATE TABLE `propertytype_prt` (
  `prt_ID` mediumint(9) NOT NULL,
  `prt_Class` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `prt_Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `prt_Description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `propertytype_prt`
--

INSERT INTO `propertytype_prt` (`prt_ID`, `prt_Class`, `prt_Name`, `prt_Description`) VALUES
(1, 'p', 'General', 'General Person Properties'),
(2, 'f', 'General', 'General Family Properties'),
(3, 'g', 'General', 'General Group Properties');

-- --------------------------------------------------------

--
-- Table structure for table `property_pro`
--

CREATE TABLE `property_pro` (
  `pro_ID` mediumint(8) UNSIGNED NOT NULL,
  `pro_Class` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pro_prt_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `pro_Name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `pro_Description` text COLLATE utf8_unicode_ci NOT NULL,
  `pro_Prompt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `property_pro`
--

INSERT INTO `property_pro` (`pro_ID`, `pro_Class`, `pro_prt_ID`, `pro_Name`, `pro_Description`, `pro_Prompt`) VALUES
(1, 'p', 1, 'Disabled', 'has a disability.', 'What is the nature of the disability?'),
(2, 'f', 2, 'Single Parent', 'is a single-parent household.', ''),
(3, 'g', 3, 'Youth', 'is youth-oriented.', '');

-- --------------------------------------------------------

--
-- Table structure for table `queryparameteroptions_qpo`
--

CREATE TABLE `queryparameteroptions_qpo` (
  `qpo_ID` smallint(5) UNSIGNED NOT NULL,
  `qpo_qrp_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `qpo_Display` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `qpo_Value` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `queryparameteroptions_qpo`
--

INSERT INTO `queryparameteroptions_qpo` (`qpo_ID`, `qpo_qrp_ID`, `qpo_Display`, `qpo_Value`) VALUES
(1, 4, 'Male', '1'),
(2, 4, 'Female', '2'),
(3, 6, 'Male', '1'),
(4, 6, 'Female', '2'),
(5, 15, 'Name', 'CONCAT(COALESCE(`per_FirstName`,\'\'),COALESCE(`per_MiddleName`,\'\'),COALESCE(`per_LastName`,\'\'))'),
(6, 15, 'Zip Code', 'fam_Zip'),
(7, 15, 'State', 'fam_State'),
(8, 15, 'City', 'fam_City'),
(9, 15, 'Home Phone', 'per_HomePhone'),
(10, 27, '2012/2013', '17'),
(11, 27, '2013/2014', '18'),
(12, 27, '2014/2015', '19'),
(13, 27, '2015/2016', '20'),
(14, 28, '2012/2013', '17'),
(15, 28, '2013/2014', '18'),
(16, 28, '2014/2015', '19'),
(17, 28, '2015/2016', '20'),
(18, 30, '2012/2013', '17'),
(19, 30, '2013/2014', '18'),
(20, 30, '2014/2015', '19'),
(21, 30, '2015/2016', '20'),
(22, 31, '2012/2013', '17'),
(23, 31, '2013/2014', '18'),
(24, 31, '2014/2015', '19'),
(25, 31, '2015/2016', '20'),
(26, 15, 'Email', 'per_Email'),
(27, 15, 'WorkEmail', 'per_WorkEmail'),
(28, 32, '2012/2013', '17'),
(29, 32, '2013/2014', '18'),
(30, 32, '2014/2015', '19'),
(31, 32, '2015/2016', '20'),
(32, 33, 'Member', '1'),
(33, 33, 'Regular Attender', '2'),
(34, 33, 'Guest', '3'),
(35, 33, 'Non-Attender', '4'),
(36, 33, 'Non-Attender (staff)', '5');

-- --------------------------------------------------------

--
-- Table structure for table `queryparameters_qrp`
--

CREATE TABLE `queryparameters_qrp` (
  `qrp_ID` mediumint(8) UNSIGNED NOT NULL,
  `qrp_qry_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `qrp_Type` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `qrp_OptionSQL` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrp_Name` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrp_Description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrp_Alias` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrp_Default` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qrp_Required` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `qrp_InputBoxSize` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `qrp_Validation` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `qrp_NumericMax` int(11) DEFAULT NULL,
  `qrp_NumericMin` int(11) DEFAULT NULL,
  `qrp_AlphaMinLength` int(11) DEFAULT NULL,
  `qrp_AlphaMaxLength` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `queryparameters_qrp`
--

INSERT INTO `queryparameters_qrp` (`qrp_ID`, `qrp_qry_ID`, `qrp_Type`, `qrp_OptionSQL`, `qrp_Name`, `qrp_Description`, `qrp_Alias`, `qrp_Default`, `qrp_Required`, `qrp_InputBoxSize`, `qrp_Validation`, `qrp_NumericMax`, `qrp_NumericMin`, `qrp_AlphaMinLength`, `qrp_AlphaMaxLength`) VALUES
(1, 4, 0, NULL, 'Minimum Age', 'The minimum age for which you want records returned.', 'min', '0', 0, 5, 'n', 120, 0, NULL, NULL),
(2, 4, 0, NULL, 'Maximum Age', 'The maximum age for which you want records returned.', 'max', '120', 1, 5, 'n', 120, 0, NULL, NULL),
(4, 6, 1, '', 'Gender', 'The desired gender to search the database for.', 'gender', '1', 1, 0, '', 0, 0, 0, 0),
(5, 7, 2, 'SELECT lst_OptionID as Value, lst_OptionName as Display FROM list_lst WHERE lst_ID=2 ORDER BY lst_OptionSequence', 'Family Role', 'Select the desired family role.', 'role', '1', 0, 0, '', 0, 0, 0, 0),
(6, 7, 1, '', 'Gender', 'The gender for which you would like records returned.', 'gender', '1', 1, 0, '', 0, 0, 0, 0),
(8, 9, 2, 'SELECT pro_ID AS Value, pro_Name as Display \r\nFROM property_pro\r\nWHERE pro_Class= \'p\' \r\nORDER BY pro_Name ', 'Property', 'The property for which you would like person records returned.', 'PropertyID', '0', 1, 0, '', 0, 0, 0, 0),
(9, 10, 2, 'SELECT distinct don_date as Value, don_date as Display FROM donations_don ORDER BY don_date ASC', 'Beginning Date', 'Please select the beginning date to calculate total contributions for each member (i.e. YYYY-MM-DD). NOTE: You can only choose dates that conatain donations.', 'startdate', '1', 1, 0, '0', 0, 0, 0, 0),
(10, 10, 2, 'SELECT distinct don_date as Value, don_date as Display FROM donations_don\r\nORDER BY don_date DESC', 'Ending Date', 'Please enter the last date to calculate total contributions for each member (i.e. YYYY-MM-DD).', 'enddate', '1', 1, 0, '', 0, 0, 0, 0),
(14, 15, 0, '', 'Search', 'Enter any part of the following: Name, City, State, Zip, Home Phone, Email, or Work Email.', 'searchstring', '', 1, 0, '', 0, 0, 0, 0),
(15, 15, 1, '', 'Field', 'Select field to search for.', 'searchwhat', '1', 1, 0, '', 0, 0, 0, 0),
(16, 11, 2, 'SELECT distinct don_date as Value, don_date as Display FROM donations_don ORDER BY don_date ASC', 'Beginning Date', 'Please select the beginning date to calculate total contributions for each member (i.e. YYYY-MM-DD). NOTE: You can only choose dates that conatain donations.', 'startdate', '1', 1, 0, '0', 0, 0, 0, 0),
(17, 11, 2, 'SELECT distinct don_date as Value, don_date as Display FROM donations_don\r\nORDER BY don_date DESC', 'Ending Date', 'Please enter the last date to calculate total contributions for each member (i.e. YYYY-MM-DD).', 'enddate', '1', 1, 0, '', 0, 0, 0, 0),
(18, 18, 0, '', 'Month', 'The birthday month for which you would like records returned.', 'birthmonth', '1', 1, 0, '', 12, 1, 1, 2),
(19, 19, 2, 'SELECT grp_ID AS Value, grp_Name AS Display FROM group_grp ORDER BY grp_Type', 'Class', 'The sunday school class for which you would like records returned.', 'group', '1', 1, 0, '', 12, 1, 1, 2),
(20, 20, 2, 'SELECT grp_ID AS Value, grp_Name AS Display FROM group_grp ORDER BY grp_Type', 'Class', 'The sunday school class for which you would like records returned.', 'group', '1', 1, 0, '', 12, 1, 1, 2),
(21, 21, 2, 'SELECT grp_ID AS Value, grp_Name AS Display FROM group_grp ORDER BY grp_Type', 'Registered students', 'Group of registered students', 'group', '1', 1, 0, '', 12, 1, 1, 2),
(22, 22, 0, '', 'Month', 'The membership anniversary month for which you would like records returned.', 'membermonth', '1', 1, 0, '', 12, 1, 1, 2),
(25, 25, 2, 'SELECT vol_ID AS Value, vol_Name AS Display FROM volunteeropportunity_vol ORDER BY vol_Name', 'Volunteer opportunities', 'Choose a volunteer opportunity', 'volopp', '1', 1, 0, '', 12, 1, 1, 2),
(26, 26, 0, '', 'Months', 'Number of months since becoming a friend', 'friendmonths', '1', 1, 0, '', 24, 1, 1, 2),
(27, 28, 1, '', 'First Fiscal Year', 'First fiscal year for comparison', 'fyid1', '9', 1, 0, '', 12, 9, 0, 0),
(28, 28, 1, '', 'Second Fiscal Year', 'Second fiscal year for comparison', 'fyid2', '9', 1, 0, '', 12, 9, 0, 0),
(30, 30, 1, '', 'First Fiscal Year', 'Pledged this year', 'fyid1', '9', 1, 0, '', 12, 9, 0, 0),
(31, 30, 1, '', 'Second Fiscal Year', 'but not this year', 'fyid2', '9', 1, 0, '', 12, 9, 0, 0),
(32, 32, 1, '', 'Fiscal Year', 'Fiscal Year.', 'fyid', '9', 1, 0, '', 12, 9, 0, 0),
(33, 18, 1, '', 'Classification', 'Member, Regular Attender, etc.', 'percls', '1', 1, 0, '', 12, 1, 1, 2),
(100, 100, 2, 'SELECT vol_ID AS Value, vol_Name AS Display FROM volunteeropportunity_vol ORDER BY vol_Name', 'Volunteer opportunities', 'First volunteer opportunity choice', 'volopp1', '1', 1, 0, '', 12, 1, 1, 2),
(101, 100, 2, 'SELECT vol_ID AS Value, vol_Name AS Display FROM volunteeropportunity_vol ORDER BY vol_Name', 'Volunteer opportunities', 'Second volunteer opportunity choice', 'volopp2', '1', 1, 0, '', 12, 1, 1, 2),
(200, 200, 2, 'SELECT custom_field as Value, custom_Name as Display FROM person_custom_master', 'Custom field', 'Choose customer person field', 'custom', '1', 0, 0, '', 0, 0, 0, 0),
(201, 200, 0, '', 'Field value', 'Match custom field to this value', 'value', '1', 0, 0, '', 0, 0, 0, 0),
(202, 201, 3, 'SELECT event_id as Value, event_title as Display FROM events_event ORDER BY event_start DESC', 'Event', 'Select the desired event', 'event', '', 1, 0, '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `query_qry`
--

CREATE TABLE `query_qry` (
  `qry_ID` mediumint(8) UNSIGNED NOT NULL,
  `qry_SQL` text COLLATE utf8_unicode_ci NOT NULL,
  `qry_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `qry_Description` text COLLATE utf8_unicode_ci NOT NULL,
  `qry_Count` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `query_qry`
--

INSERT INTO `query_qry` (`qry_ID`, `qry_SQL`, `qry_Name`, `qry_Description`, `qry_Count`) VALUES
(3, 'SELECT CONCAT(\'<a href=v2/family/\',fam_ID,\'>\',fam_Name,\'</a>\') AS \'Family Name\', COUNT(*) AS \'No.\'\nFROM person_per\nINNER JOIN family_fam\nON fam_ID = per_fam_ID\nGROUP BY per_fam_ID\nORDER BY \'No.\' DESC', 'Family Member Count', 'Returns each family and the total number of people assigned to them.', 0),
(4, 'SELECT per_ID as AddToCart,CONCAT(\'<a\r\nhref=PersonView.php?PersonID=\',per_ID,\'>\',per_FirstName,\'\r\n\',per_LastName,\'</a>\') AS Name,\r\nCONCAT(per_BirthMonth,\'/\',per_BirthDay,\'/\',per_BirthYear) AS \'Birth Date\',\r\nDATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(CONCAT(per_BirthYear,\'-\',per_BirthMonth,\'-\',per_BirthDay))),\'%Y\')+0 AS  \'Age\'\r\nFROM person_per\r\nWHERE\r\nDATE_ADD(CONCAT(per_BirthYear,\'-\',per_BirthMonth,\'-\',per_BirthDay),INTERVAL\r\n~min~ YEAR) <= CURDATE()\r\nAND\r\nDATE_ADD(CONCAT(per_BirthYear,\'-\',per_BirthMonth,\'-\',per_BirthDay),INTERVAL\r\n(~max~ + 1) YEAR) >= CURDATE()', 'Person by Age', 'Returns any person records with ages between two given ages.', 1),
(6, 'SELECT COUNT(per_ID) AS Total FROM person_per WHERE per_Gender = ~gender~', 'Total By Gender', 'Total of records matching a given gender.', 0),
(7, 'SELECT per_ID as AddToCart, CONCAT(per_FirstName,\' \',per_LastName) AS Name FROM person_per WHERE per_fmr_ID = ~role~ AND per_Gender = ~gender~', 'Person by Role and Gender', 'Selects person records with the family role and gender specified.', 1),
(9, 'SELECT \r\nper_ID as AddToCart, \r\nCONCAT(per_FirstName,\' \',per_LastName) AS Name, \r\nCONCAT(r2p_Value,\' \') AS Value\r\nFROM person_per,record2property_r2p\r\nWHERE per_ID = r2p_record_ID\r\nAND r2p_pro_ID = ~PropertyID~\r\nORDER BY per_LastName', 'Person by Property', 'Returns person records which are assigned the given property.', 1),
(15, 'SELECT per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',per_ID,\'>\',COALESCE(`per_FirstName`,\'\'),\' \',COALESCE(`per_MiddleName`,\'\'),\' \',COALESCE(`per_LastName`,\'\'),\'</a>\') AS Name, fam_City as City, fam_State as State, fam_Zip as ZIP, per_HomePhone as HomePhone, per_Email as Email, per_WorkEmail as WorkEmail FROM person_per RIGHT JOIN family_fam ON family_fam.fam_id = person_per.per_fam_id WHERE ~searchwhat~ LIKE \'%~searchstring~%\'', 'Advanced Search', 'Search by any part of Name, City, State, Zip, Home Phone, Email, or Work Email.', 1),
(18, 'SELECT per_ID as AddToCart, per_BirthDay as Day, CONCAT(per_FirstName,\' \',per_LastName) AS Name FROM person_per WHERE per_cls_ID=~percls~ AND per_BirthMonth=~birthmonth~ ORDER BY per_BirthDay', 'Birthdays', 'People with birthdays in a particular month', 0),
(21, 'SELECT per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',per_ID,\'>\',per_FirstName,\' \',per_LastName,\'</a>\') AS Name FROM person_per LEFT JOIN person2group2role_p2g2r ON per_id = p2g2r_per_ID WHERE p2g2r_grp_ID=~group~ ORDER BY per_LastName', 'Registered students', 'Find Registered students', 1),
(22, 'SELECT per_ID as AddToCart, DAYOFMONTH(per_MembershipDate) as Day, per_MembershipDate AS DATE, CONCAT(per_FirstName,\' \',per_LastName) AS Name FROM person_per WHERE per_cls_ID=1 AND MONTH(per_MembershipDate)=~membermonth~ ORDER BY per_MembershipDate', 'Membership anniversaries', 'Members who joined in a particular month', 0),
(23, 'SELECT usr_per_ID as AddToCart, CONCAT(a.per_FirstName,\' \',a.per_LastName) AS Name FROM user_usr LEFT JOIN person_per a ON per_ID=usr_per_ID ORDER BY per_LastName', 'Select database users', 'People who are registered as database users', 0),
(24, 'SELECT per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',per_ID,\'>\',per_FirstName,\' \',per_LastName,\'</a>\') AS Name FROM person_per WHERE per_cls_id =1', 'Select all members', 'People who are members', 0),
(25, 'SELECT per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',per_ID,\'>\',per_FirstName,\' \',per_LastName,\'</a>\') AS Name FROM person_per LEFT JOIN person2volunteeropp_p2vo ON per_id = p2vo_per_ID WHERE p2vo_vol_ID = ~volopp~ ORDER BY per_LastName', 'Volunteers', 'Find volunteers for a particular opportunity', 1),
(26, 'SELECT per_ID as AddToCart, CONCAT(per_FirstName,\' \',per_LastName) AS Name FROM person_per WHERE DATE_SUB(NOW(),INTERVAL ~friendmonths~ MONTH)<per_FriendDate ORDER BY per_MembershipDate', 'Recent friends', 'Friends who signed up in previous months', 0),
(27, 'SELECT per_ID as AddToCart, CONCAT(per_FirstName,\' \',per_LastName) AS Name FROM person_per inner join family_fam on per_fam_ID=fam_ID where per_fmr_ID<>3 AND fam_OkToCanvass=\"TRUE\" ORDER BY fam_Zip', 'Families to Canvass', 'People in families that are ok to canvass.', 0),
(28, 'SELECT fam_Name, a.plg_amount as PlgFY1, b.plg_amount as PlgFY2 from family_fam left join pledge_plg a on a.plg_famID = fam_ID and a.plg_FYID=~fyid1~ and a.plg_PledgeOrPayment=\'Pledge\' left join pledge_plg b on b.plg_famID = fam_ID and b.plg_FYID=~fyid2~ and b.plg_PledgeOrPayment=\'Pledge\' order by fam_Name', 'Pledge comparison', 'Compare pledges between two fiscal years', 1),
(30, 'SELECT per_ID as AddToCart, CONCAT(per_FirstName,\' \',per_LastName) AS Name, fam_address1, fam_city, fam_state, fam_zip FROM person_per join family_fam on per_fam_id=fam_id where per_fmr_id<>3 and per_fam_id in (select fam_id from family_fam inner join pledge_plg a on a.plg_famID=fam_ID and a.plg_FYID=~fyid1~ and a.plg_amount>0) and per_fam_id not in (select fam_id from family_fam inner join pledge_plg b on b.plg_famID=fam_ID and b.plg_FYID=~fyid2~ and b.plg_amount>0)', 'Missing pledges', 'Find people who pledged one year but not another', 1),
(32, 'SELECT fam_Name, fam_Envelope, b.fun_Name as Fund_Name, a.plg_amount as Pledge from family_fam left join pledge_plg a on a.plg_famID = fam_ID and a.plg_FYID=~fyid~ and a.plg_PledgeOrPayment=\'Pledge\' and a.plg_amount>0 join donationfund_fun b on b.fun_ID = a.plg_fundID order by fam_Name, a.plg_fundID', 'Family Pledge by Fiscal Year', 'Pledge summary by family name for each fund for the selected fiscal year', 1),
(100, 'SELECT a.per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',a.per_ID,\'>\',a.per_FirstName,\' \',a.per_LastName,\'</a>\') AS Name FROM person_per AS a LEFT JOIN person2volunteeropp_p2vo p2v1 ON (a.per_id = p2v1.p2vo_per_ID AND p2v1.p2vo_vol_ID = ~volopp1~) LEFT JOIN person2volunteeropp_p2vo p2v2 ON (a.per_id = p2v2.p2vo_per_ID AND p2v2.p2vo_vol_ID = ~volopp2~) WHERE p2v1.p2vo_per_ID=p2v2.p2vo_per_ID ORDER BY per_LastName', 'Volunteers', 'Find volunteers for who match two specific opportunity codes', 1),
(200, 'SELECT a.per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',a.per_ID,\'>\',a.per_FirstName,\' \',a.per_LastName,\'</a>\') AS Name FROM person_per AS a LEFT JOIN person_custom pc ON a.per_id = pc.per_ID WHERE pc.~custom~=\'~value~\' ORDER BY per_LastName', 'CustomSearch', 'Find people with a custom field value', 1),
(201, 'SELECT per_ID as AddToCart, CONCAT(\'<a href=PersonView.php?PersonID=\',per_ID,\'>\',per_FirstName,\',per_LastName,\'</a>\') AS Name, per_LastName AS Lastname FROM person_per LEFT OUTER JOIN (SELECT event_attend.attend_id, event_attend.person_id FROM event_attend WHERE event_attend.event_id IN (~event~)) a ON person_per.per_ID = a.person_id WHERE a.attend_id is NULL ORDER BY person_per.per_LastName, person_per.per_FirstName', 'Missing people', 'Find people who didn\'t attend an event', 1);

-- --------------------------------------------------------

--
-- Table structure for table `record2property_r2p`
--

CREATE TABLE `record2property_r2p` (
  `r2p_pro_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `r2p_record_ID` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `r2p_Value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `result_res`
--

CREATE TABLE `result_res` (
  `res_ID` mediumint(9) NOT NULL,
  `res_echotype1` text COLLATE utf8_unicode_ci NOT NULL,
  `res_echotype2` text COLLATE utf8_unicode_ci NOT NULL,
  `res_echotype3` text COLLATE utf8_unicode_ci NOT NULL,
  `res_authorization` text COLLATE utf8_unicode_ci NOT NULL,
  `res_order_number` text COLLATE utf8_unicode_ci NOT NULL,
  `res_reference` text COLLATE utf8_unicode_ci NOT NULL,
  `res_status` text COLLATE utf8_unicode_ci NOT NULL,
  `res_avs_result` text COLLATE utf8_unicode_ci NOT NULL,
  `res_security_result` text COLLATE utf8_unicode_ci NOT NULL,
  `res_mac` text COLLATE utf8_unicode_ci NOT NULL,
  `res_decline_code` text COLLATE utf8_unicode_ci NOT NULL,
  `res_tran_date` text COLLATE utf8_unicode_ci NOT NULL,
  `res_merchant_name` text COLLATE utf8_unicode_ci NOT NULL,
  `res_version` text COLLATE utf8_unicode_ci NOT NULL,
  `res_EchoServer` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `role_desc` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`, `role_desc`) VALUES
(1, 'Welcome Committee', NULL),
(2, 'Clergy', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `token` varchar(99) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reference_id` int(9) NOT NULL,
  `valid_until_date` datetime DEFAULT NULL,
  `remainingUses` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `userconfig_ucfg`
--

CREATE TABLE `userconfig_ucfg` (
  `ucfg_per_id` mediumint(9) UNSIGNED NOT NULL,
  `ucfg_id` int(11) NOT NULL DEFAULT 0,
  `ucfg_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ucfg_value` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ucfg_type` enum('text','number','date','boolean','textarea') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `ucfg_tooltip` text COLLATE utf8_unicode_ci NOT NULL,
  `ucfg_permission` enum('FALSE','TRUE') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'FALSE',
  `ucfg_cat` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userconfig_ucfg`
--

INSERT INTO `userconfig_ucfg` (`ucfg_per_id`, `ucfg_id`, `ucfg_name`, `ucfg_value`, `ucfg_type`, `ucfg_tooltip`, `ucfg_permission`, `ucfg_cat`) VALUES
(0, 0, 'bEmailMailto', '1', 'boolean', 'User permission to send email via mailto: links', 'TRUE', ''),
(0, 1, 'sMailtoDelimiter', ',', 'text', 'Delimiter to separate emails in mailto: links', 'TRUE', ''),
(0, 5, 'bCreateDirectory', '0', 'boolean', 'User permission to create directories', 'FALSE', 'SECURITY'),
(0, 6, 'bExportCSV', '0', 'boolean', 'User permission to export CSV files', 'FALSE', 'SECURITY'),
(0, 7, 'bUSAddressVerification', '0', 'boolean', 'User permission to use IST Address Verification', 'FALSE', ''),
(0, 10, 'bAddEvent', '0', 'boolean', 'Allow user to add new event', 'FALSE', 'SECURITY'),
(1, 0, 'bEmailMailto', '1', 'boolean', 'User permission to send email via mailto: links', 'TRUE', ''),
(1, 1, 'sMailtoDelimiter', ',', 'text', 'user permission to send email via mailto: links', 'TRUE', ''),
(1, 5, 'bCreateDirectory', '1', 'boolean', 'User permission to create directories', 'TRUE', ''),
(1, 6, 'bExportCSV', '1', 'boolean', 'User permission to export CSV files', 'TRUE', ''),
(1, 7, 'bUSAddressVerification', '1', 'boolean', 'User permission to use IST Address Verification', 'TRUE', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_settings`
--

CREATE TABLE `user_settings` (
  `user_id` int(11) NOT NULL,
  `setting_name` varchar(50) NOT NULL,
  `setting_value` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_usr`
--

CREATE TABLE `user_usr` (
  `usr_per_ID` mediumint(9) UNSIGNED NOT NULL DEFAULT 0,
  `usr_Password` varchar(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `usr_NeedPasswordChange` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `usr_LastLogin` datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
  `usr_LoginCount` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `usr_FailedLogins` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `usr_AddRecords` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `usr_EditRecords` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `usr_DeleteRecords` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `usr_MenuOptions` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `usr_ManageGroups` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `usr_Finance` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `usr_Notes` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `usr_Admin` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `usr_SearchLimit` tinyint(4) DEFAULT 10,
  `usr_Style` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'Style.css',
  `usr_showPledges` tinyint(1) NOT NULL DEFAULT 0,
  `usr_showPayments` tinyint(1) NOT NULL DEFAULT 0,
  `usr_showSince` date NOT NULL DEFAULT '2016-01-01',
  `usr_defaultFY` mediumint(9) NOT NULL DEFAULT 10,
  `usr_currentDeposit` mediumint(9) NOT NULL DEFAULT 0,
  `usr_UserName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_apiKey` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_EditSelf` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `usr_CalStart` date DEFAULT NULL,
  `usr_CalEnd` date DEFAULT NULL,
  `usr_CalNoSchool1` date DEFAULT NULL,
  `usr_CalNoSchool2` date DEFAULT NULL,
  `usr_CalNoSchool3` date DEFAULT NULL,
  `usr_CalNoSchool4` date DEFAULT NULL,
  `usr_CalNoSchool5` date DEFAULT NULL,
  `usr_CalNoSchool6` date DEFAULT NULL,
  `usr_CalNoSchool7` date DEFAULT NULL,
  `usr_CalNoSchool8` date DEFAULT NULL,
  `usr_SearchFamily` tinyint(3) DEFAULT NULL,
  `usr_Canvasser` tinyint(1) NOT NULL DEFAULT 0,
  `usr_TwoFactorAuthSecret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usr_TwoFactorAuthLastKeyTimestamp` int(11) DEFAULT NULL,
  `usr_TwoFactorAuthRecoveryCodes` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_usr`
--

INSERT INTO `user_usr` (`usr_per_ID`, `usr_Password`, `usr_NeedPasswordChange`, `usr_LastLogin`, `usr_LoginCount`, `usr_FailedLogins`, `usr_AddRecords`, `usr_EditRecords`, `usr_DeleteRecords`, `usr_MenuOptions`, `usr_ManageGroups`, `usr_Finance`, `usr_Notes`, `usr_Admin`, `usr_SearchLimit`, `usr_Style`, `usr_showPledges`, `usr_showPayments`, `usr_showSince`, `usr_defaultFY`, `usr_currentDeposit`, `usr_UserName`, `usr_apiKey`, `usr_EditSelf`, `usr_CalStart`, `usr_CalEnd`, `usr_CalNoSchool1`, `usr_CalNoSchool2`, `usr_CalNoSchool3`, `usr_CalNoSchool4`, `usr_CalNoSchool5`, `usr_CalNoSchool6`, `usr_CalNoSchool7`, `usr_CalNoSchool8`, `usr_SearchFamily`, `usr_Canvasser`, `usr_TwoFactorAuthSecret`, `usr_TwoFactorAuthLastKeyTimestamp`, `usr_TwoFactorAuthRecoveryCodes`) VALUES
(1, '9e7f2a1694151defa46d09f8a670c90cc35951b619bfcb1f3a2c9174be7406c5', 0, '2021-12-13 14:40:18', 11, 0, 0, 0, 0, 0, 0, 0, 0, 1, 10, 'skin-red', 0, 0, '2016-01-01', 10, 0, 'Admin', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `version_ver`
--

CREATE TABLE `version_ver` (
  `ver_ID` mediumint(9) UNSIGNED NOT NULL,
  `ver_version` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ver_update_start` datetime DEFAULT NULL,
  `ver_update_end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `version_ver`
--

INSERT INTO `version_ver` (`ver_ID`, `ver_version`, `ver_update_start`, `ver_update_end`) VALUES
(1, '4.4.3', '2021-11-15 09:46:22', '2021-11-15 09:46:58'),
(2, '4.4.5', '2021-11-15 23:24:18', '2021-11-15 23:24:18');

-- --------------------------------------------------------

--
-- Table structure for table `volunteeropportunity_vol`
--

CREATE TABLE `volunteeropportunity_vol` (
  `vol_ID` int(3) NOT NULL,
  `vol_Order` int(3) NOT NULL DEFAULT 0,
  `vol_Active` enum('true','false') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `vol_Name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vol_Description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `whycame_why`
--

CREATE TABLE `whycame_why` (
  `why_ID` mediumint(9) NOT NULL,
  `why_per_ID` mediumint(9) NOT NULL DEFAULT 0,
  `why_join` text COLLATE utf8_unicode_ci NOT NULL,
  `why_come` text COLLATE utf8_unicode_ci NOT NULL,
  `why_suggest` text COLLATE utf8_unicode_ci NOT NULL,
  `why_hearOfUs` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure for view `email_count`
--
DROP TABLE IF EXISTS `email_count`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `email_count`  AS SELECT `email_list`.`email` AS `email`, count(0) AS `total` FROM `email_list` GROUP BY `email_list`.`email` ;

-- --------------------------------------------------------

--
-- Structure for view `email_list`
--
DROP TABLE IF EXISTS `email_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `email_list`  AS SELECT `family_fam`.`fam_Email` AS `email`, 'family' AS `type`, `family_fam`.`fam_ID` AS `id` FROM `family_fam` WHERE `family_fam`.`fam_Email` is not null AND `family_fam`.`fam_Email` <> '' ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `calendars`
--
ALTER TABLE `calendars`
  ADD PRIMARY KEY (`calendar_id`),
  ADD UNIQUE KEY `accesstoken` (`accesstoken`);

--
-- Indexes for table `calendar_events`
--
ALTER TABLE `calendar_events`
  ADD PRIMARY KEY (`calendar_id`,`event_id`);

--
-- Indexes for table `canvassdata_can`
--
ALTER TABLE `canvassdata_can`
  ADD PRIMARY KEY (`can_ID`);

--
-- Indexes for table `church_location_person`
--
ALTER TABLE `church_location_person`
  ADD PRIMARY KEY (`location_id`,`person_id`);

--
-- Indexes for table `church_location_role`
--
ALTER TABLE `church_location_role`
  ADD PRIMARY KEY (`location_id`,`role_id`);

--
-- Indexes for table `config_cfg`
--
ALTER TABLE `config_cfg`
  ADD PRIMARY KEY (`cfg_id`),
  ADD UNIQUE KEY `cfg_name` (`cfg_name`);

--
-- Indexes for table `deposit_dep`
--
ALTER TABLE `deposit_dep`
  ADD PRIMARY KEY (`dep_ID`);

--
-- Indexes for table `donateditem_di`
--
ALTER TABLE `donateditem_di`
  ADD PRIMARY KEY (`di_ID`);

--
-- Indexes for table `donationfund_fun`
--
ALTER TABLE `donationfund_fun`
  ADD PRIMARY KEY (`fun_ID`);

--
-- Indexes for table `eventcountnames_evctnm`
--
ALTER TABLE `eventcountnames_evctnm`
  ADD UNIQUE KEY `evctnm_countid` (`evctnm_countid`),
  ADD UNIQUE KEY `evctnm_eventtypeid` (`evctnm_eventtypeid`,`evctnm_countname`);

--
-- Indexes for table `eventcounts_evtcnt`
--
ALTER TABLE `eventcounts_evtcnt`
  ADD PRIMARY KEY (`evtcnt_eventid`,`evtcnt_countid`);

--
-- Indexes for table `events_event`
--
ALTER TABLE `events_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `event_attend`
--
ALTER TABLE `event_attend`
  ADD PRIMARY KEY (`attend_id`),
  ADD UNIQUE KEY `event_id` (`event_id`,`person_id`);

--
-- Indexes for table `event_audience`
--
ALTER TABLE `event_audience`
  ADD PRIMARY KEY (`event_id`,`group_id`);

--
-- Indexes for table `event_types`
--
ALTER TABLE `event_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `family_custom`
--
ALTER TABLE `family_custom`
  ADD PRIMARY KEY (`fam_ID`);

--
-- Indexes for table `family_fam`
--
ALTER TABLE `family_fam`
  ADD PRIMARY KEY (`fam_ID`);

--
-- Indexes for table `fundraiser_fr`
--
ALTER TABLE `fundraiser_fr`
  ADD PRIMARY KEY (`fr_ID`),
  ADD UNIQUE KEY `fr_ID` (`fr_ID`);

--
-- Indexes for table `groupprop_master`
--
ALTER TABLE `groupprop_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_grp`
--
ALTER TABLE `group_grp`
  ADD PRIMARY KEY (`grp_ID`);

--
-- Indexes for table `istlookup_lu`
--
ALTER TABLE `istlookup_lu`
  ADD PRIMARY KEY (`lu_fam_ID`);

--
-- Indexes for table `kioskassginment_kasm`
--
ALTER TABLE `kioskassginment_kasm`
  ADD PRIMARY KEY (`kasm_ID`),
  ADD UNIQUE KEY `kasm_ID` (`kasm_ID`);

--
-- Indexes for table `kioskdevice_kdev`
--
ALTER TABLE `kioskdevice_kdev`
  ADD PRIMARY KEY (`kdev_ID`);

--
-- Indexes for table `list_lst`
--
ALTER TABLE `list_lst`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `menu_links`
--
ALTER TABLE `menu_links`
  ADD PRIMARY KEY (`linkId`);

--
-- Indexes for table `multibuy_mb`
--
ALTER TABLE `multibuy_mb`
  ADD PRIMARY KEY (`mb_ID`);

--
-- Indexes for table `nc_logs`
--
ALTER TABLE `nc_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `note_nte`
--
ALTER TABLE `note_nte`
  ADD PRIMARY KEY (`nte_ID`);

--
-- Indexes for table `paddlenum_pn`
--
ALTER TABLE `paddlenum_pn`
  ADD PRIMARY KEY (`pn_ID`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD UNIQUE KEY `permission_name` (`permission_name`);

--
-- Indexes for table `person2group2role_p2g2r`
--
ALTER TABLE `person2group2role_p2g2r`
  ADD PRIMARY KEY (`p2g2r_per_ID`,`p2g2r_grp_ID`),
  ADD KEY `p2g2r_per_ID` (`p2g2r_per_ID`,`p2g2r_grp_ID`,`p2g2r_rle_ID`);

--
-- Indexes for table `person2volunteeropp_p2vo`
--
ALTER TABLE `person2volunteeropp_p2vo`
  ADD PRIMARY KEY (`p2vo_ID`);

--
-- Indexes for table `person_custom`
--
ALTER TABLE `person_custom`
  ADD PRIMARY KEY (`per_ID`);

--
-- Indexes for table `person_custom_master`
--
ALTER TABLE `person_custom_master`
  ADD PRIMARY KEY (`custom_Field`);

--
-- Indexes for table `person_per`
--
ALTER TABLE `person_per`
  ADD PRIMARY KEY (`per_ID`);

--
-- Indexes for table `person_permission`
--
ALTER TABLE `person_permission`
  ADD PRIMARY KEY (`per_id`,`permission_id`);

--
-- Indexes for table `person_per_no_teacher`
--
ALTER TABLE `person_per_no_teacher`
  ADD PRIMARY KEY (`COL 1`);

--
-- Indexes for table `person_roles`
--
ALTER TABLE `person_roles`
  ADD PRIMARY KEY (`per_id`,`role_id`);

--
-- Indexes for table `pledge_plg`
--
ALTER TABLE `pledge_plg`
  ADD PRIMARY KEY (`plg_plgID`);

--
-- Indexes for table `propertytype_prt`
--
ALTER TABLE `propertytype_prt`
  ADD PRIMARY KEY (`prt_ID`);

--
-- Indexes for table `property_pro`
--
ALTER TABLE `property_pro`
  ADD PRIMARY KEY (`pro_ID`);

--
-- Indexes for table `queryparameteroptions_qpo`
--
ALTER TABLE `queryparameteroptions_qpo`
  ADD PRIMARY KEY (`qpo_ID`);

--
-- Indexes for table `queryparameters_qrp`
--
ALTER TABLE `queryparameters_qrp`
  ADD PRIMARY KEY (`qrp_ID`),
  ADD KEY `qrp_qry_ID` (`qrp_qry_ID`);

--
-- Indexes for table `query_qry`
--
ALTER TABLE `query_qry`
  ADD PRIMARY KEY (`qry_ID`);

--
-- Indexes for table `result_res`
--
ALTER TABLE `result_res`
  ADD PRIMARY KEY (`res_ID`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`token`);

--
-- Indexes for table `userconfig_ucfg`
--
ALTER TABLE `userconfig_ucfg`
  ADD PRIMARY KEY (`ucfg_per_id`,`ucfg_id`);

--
-- Indexes for table `user_settings`
--
ALTER TABLE `user_settings`
  ADD PRIMARY KEY (`user_id`,`setting_name`);

--
-- Indexes for table `user_usr`
--
ALTER TABLE `user_usr`
  ADD PRIMARY KEY (`usr_per_ID`),
  ADD UNIQUE KEY `usr_UserName` (`usr_UserName`),
  ADD UNIQUE KEY `usr_apiKey` (`usr_apiKey`);

--
-- Indexes for table `version_ver`
--
ALTER TABLE `version_ver`
  ADD PRIMARY KEY (`ver_ID`),
  ADD UNIQUE KEY `ver_version` (`ver_version`);

--
-- Indexes for table `volunteeropportunity_vol`
--
ALTER TABLE `volunteeropportunity_vol`
  ADD PRIMARY KEY (`vol_ID`);

--
-- Indexes for table `whycame_why`
--
ALTER TABLE `whycame_why`
  ADD PRIMARY KEY (`why_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `calendars`
--
ALTER TABLE `calendars`
  MODIFY `calendar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `canvassdata_can`
--
ALTER TABLE `canvassdata_can`
  MODIFY `can_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deposit_dep`
--
ALTER TABLE `deposit_dep`
  MODIFY `dep_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `donateditem_di`
--
ALTER TABLE `donateditem_di`
  MODIFY `di_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `donationfund_fun`
--
ALTER TABLE `donationfund_fun`
  MODIFY `fun_ID` tinyint(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `eventcountnames_evctnm`
--
ALTER TABLE `eventcountnames_evctnm`
  MODIFY `evctnm_countid` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `events_event`
--
ALTER TABLE `events_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `event_attend`
--
ALTER TABLE `event_attend`
  MODIFY `attend_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `event_types`
--
ALTER TABLE `event_types`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `family_fam`
--
ALTER TABLE `family_fam`
  MODIFY `fam_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fundraiser_fr`
--
ALTER TABLE `fundraiser_fr`
  MODIFY `fr_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groupprop_master`
--
ALTER TABLE `groupprop_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `group_grp`
--
ALTER TABLE `group_grp`
  MODIFY `grp_ID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `kioskassginment_kasm`
--
ALTER TABLE `kioskassginment_kasm`
  MODIFY `kasm_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kioskdevice_kdev`
--
ALTER TABLE `kioskdevice_kdev`
  MODIFY `kdev_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `list_lst`
--
ALTER TABLE `list_lst`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu_links`
--
ALTER TABLE `menu_links`
  MODIFY `linkId` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `multibuy_mb`
--
ALTER TABLE `multibuy_mb`
  MODIFY `mb_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nc_logs`
--
ALTER TABLE `nc_logs`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `note_nte`
--
ALTER TABLE `note_nte`
  MODIFY `nte_ID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `paddlenum_pn`
--
ALTER TABLE `paddlenum_pn`
  MODIFY `pn_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `person2volunteeropp_p2vo`
--
ALTER TABLE `person2volunteeropp_p2vo`
  MODIFY `p2vo_ID` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `person_per`
--
ALTER TABLE `person_per`
  MODIFY `per_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=369;

--
-- AUTO_INCREMENT for table `pledge_plg`
--
ALTER TABLE `pledge_plg`
  MODIFY `plg_plgID` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `propertytype_prt`
--
ALTER TABLE `propertytype_prt`
  MODIFY `prt_ID` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `property_pro`
--
ALTER TABLE `property_pro`
  MODIFY `pro_ID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `queryparameteroptions_qpo`
--
ALTER TABLE `queryparameteroptions_qpo`
  MODIFY `qpo_ID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `queryparameters_qrp`
--
ALTER TABLE `queryparameters_qrp`
  MODIFY `qrp_ID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT for table `query_qry`
--
ALTER TABLE `query_qry`
  MODIFY `qry_ID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT for table `result_res`
--
ALTER TABLE `result_res`
  MODIFY `res_ID` mediumint(9) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `version_ver`
--
ALTER TABLE `version_ver`
  MODIFY `ver_ID` mediumint(9) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `volunteeropportunity_vol`
--
ALTER TABLE `volunteeropportunity_vol`
  MODIFY `vol_ID` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `whycame_why`
--
ALTER TABLE `whycame_why`
  MODIFY `why_ID` mediumint(9) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
