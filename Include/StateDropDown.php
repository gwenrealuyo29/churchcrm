<select name="State" class="form-control select2" id="state-input">
	<option value="">No Selection</option>
	<option value="" disabled>--------------------</option>
	<option value="R1"<?php if ($sState == 'R1') {
    echo ' selected';
} ?>>Ilocos Region (Region I)</option>
	<option value="R2"<?php if ($sState == 'R2') {
    echo ' selected';
} ?>>Cagayan Valley (Region II)</option>
	<option value="R3"<?php if ($sState == 'R3') {
    echo ' selected';
} ?>>Central Luzon (Region III)</option>
	<option value="4A"<?php if ($sState == '4A') {
    echo ' selected';
} ?>>CALABARZON (Region IV-A)</option>
	<option value="R5"<?php if ($sState == 'R5') {
    echo ' selected';
} ?>>Bicol Region (Region V)</option>
	<option value="R6"<?php if ($sState == 'R6') {
    echo ' selected';
} ?>>Western Visayas (Region VI)</option>
	<option value="R7"<?php if ($sState == 'R7') {
    echo ' selected';
} ?>>Central Visayas (Region VII)</option>
	<option value="R8"<?php if ($sState == 'R8') {
    echo ' selected';
} ?>>Eastern Visayas (Region VIII)</option>
	<option value="R9"<?php if ($sState == 'R9') {
    echo ' selected';
} ?>>Zamboanga Peninsula (Region IX)</option>
	<option value="RX"<?php if ($sState == 'RX') {
    echo ' selected';
} ?>>Northern Mindanao (Region X)</option>
	<option value="11"<?php if ($sState == '11') {
    echo ' selected';
} ?>>Davao Region (Region XI)</option>
	<option value="12"<?php if ($sState == '12') {
    echo ' selected';
} ?>>SOCCSKSARGEN (Region XII)</option>
	<option value="NC"<?php if ($sState == 'NC') {
    echo ' selected';
} ?>>National Capital Region (NCR)</option>
	<option value="CR"<?php if ($sState == 'CR') {
    echo ' selected';
} ?>>Cordillera Administrative Region (CAR)</option>
	<option value="MM"<?php if ($sState == 'MM') {
    echo ' selected';
} ?>>Bangsamoro Autonomous Region in Muslim Mindanao (BARMM)</option>
	<option value="13"<?php if ($sState == '13') {
    echo ' selected';
} ?>>Caraga (Region XIII)</option>
	<option value="4B"<?php if ($sState == '4B') {
    echo ' selected';
} ?>>MIMAROPA Region</option>
</select>
